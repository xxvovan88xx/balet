<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Вопросы и ответы о Баббалет");
//$APPLICATION->SetPageProperty("title", "Вопросы и ответы о Баббалет");
//$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Вопросы и ответы о Баббалет");
?>


<main class="main" role="main">
  <div class="wrap">
    <div class="wrap__block wrap__block--gray">
	
	<div class="wrap__heading">
		<h1 class="wrap__title">Вопросы и ответы</h1>
	</div>
	
	<?$APPLICATION->IncludeComponent("bitrix:news.list", "faq", Array(
        //"COMPONENT_TEMPLATE" => "main_reviews", // Шаблон
        "IBLOCK_TYPE" => "content", // Тип информационного блока (используется только для проверки)
        "IBLOCK_ID" => KRIBlock::$ID_FAQ, // Код информационного блока
        "NEWS_COUNT" => "1000", // Количество новостей на странице
        "SORT_BY1" => "ID", // Поле для первой сортировки новостей
        "SORT_ORDER1" => "ASC", // Направление для первой сортировки новостей
        "SORT_BY2" => "SORT", // Поле для второй сортировки новостей
        "SORT_ORDER2" => "ASC", // Направление для второй сортировки новостей
        "FILTER_NAME" => "", // Фильтр
        /*"FIELD_CODE" => array(// Поля
            0 => "NAME",
            1 => "PREVIEW_TEXT",
            2 => "",
        ),*/
        "PROPERTY_CODE" => array(// Выводимые поля из свойств
            0 => "CITY",
            1 => "VIDEO",
        ),
        "CHECK_DATES" => "Y", // Показывать только активные на данный момент элементы
        "DETAIL_URL" => "", // URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "AJAX_MODE" => "N", // Включить AJAX режим
        "AJAX_OPTION_JUMP" => "N", // Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "N", // Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N", // Включить эмуляцию навигации браузера
        "AJAX_OPTION_ADDITIONAL" => "", // Дополнительный идентификатор
        "CACHE_TYPE" => "A", // Тип кеширования
        "CACHE_TIME" => "3600", // Время кеширования (сек.)
        "CACHE_FILTER" => "N", // Кешировать при установленном фильтре
        "CACHE_GROUPS" => "N", // Учитывать права доступа
        "PREVIEW_TRUNCATE_LEN" => "200", // Максимальная длина анонса для вывода (только для типа текст)
        "ACTIVE_DATE_FORMAT" => "d.m.Y", // Формат показа даты
        "SET_TITLE" => "N", // Устанавливать заголовок страницы
        "SET_BROWSER_TITLE" => "N", // Устанавливать заголовок окна браузера
        "SET_META_KEYWORDS" => "N", // Устанавливать ключевые слова страницы
        "SET_META_DESCRIPTION" => "N", // Устанавливать описание страницы
        "SET_STATUS_404" => "N", // Устанавливать статус 404, если не найдены элемент или раздел
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N", // Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "N", // Включать раздел в цепочку навигации
        "HIDE_LINK_WHEN_NO_DETAIL" => "N", // Скрывать ссылку, если нет детального описания
        "PARENT_SECTION" => "", // ID раздела
        "PARENT_SECTION_CODE" => "", // Код раздела
        "INCLUDE_SUBSECTIONS" => "Y", // Показывать элементы подразделов раздела
        "DISPLAY_DATE" => "N", // Выводить дату элемента
        "DISPLAY_NAME" => "N", // Выводить название элемента
        "DISPLAY_PICTURE" => "N", // Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "Y", // Выводить текст анонса
        "PAGER_TEMPLATE" => ".default", // Шаблон постраничной навигации
        "DISPLAY_TOP_PAGER" => "N", // Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "N", // Выводить под списком
        "PAGER_TITLE" => "Новости", // Название категорий
        "PAGER_SHOW_ALWAYS" => "N", // Выводить всегда
        "PAGER_DESC_NUMBERING" => "N", // Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000", // Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "N", // Показывать ссылку "Все"
            ), false
    );
	?>
        
    </div>
  </div>
</main>
	

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>