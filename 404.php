<?
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');

CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

$APPLICATION->SetTitle("404 Not Found");

?>

<main class="main" role="main">
	<div class="not-found">
		<div class="not-found__inner">
			<p class="not-found__point">404</p>
			<div class="not-found__info">
				<h2 class="not-found__title">Страница не найдена</h2>
				<p class="not-found__text">Извините, такой страницы не существует</p>
			</div>
		</div>
	</div>
</main>
<?

/*
$APPLICATION->IncludeComponent("bitrix:main.map", ".default", Array(
	"LEVEL"	=>	"3",
	"COL_NUM"	=>	"2",
	"SHOW_DESCRIPTION"	=>	"Y",
	"SET_TITLE"	=>	"Y",
	"CACHE_TIME"	=>	"3600"
	)
);
*/

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>