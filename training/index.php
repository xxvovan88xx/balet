<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Тренировки в зале с индивидуальным подходом и контролем техники");
$APPLICATION->SetPageProperty("description", "Профессиональный контроль техники выполнения упражнений. Индивидуальный подход и учет особенностей здоровья и уровеня физической подготовки каждого. Вы достигате больших результатов занимаясь в группе единомышленников.");
$APPLICATION->SetTitle("Тренировки в зале с индивидуальным подходом и контролем техники");
?><main class="main" role="main">
  <div class="wrap">
    <div class="wrap__block wrap__block--gray">
      <div class="wrap__heading">
        <h1 class="wrap__title">Тренировки в зале</h1>
      </div>
      <div class="wrap__item container">
        <div class="">
          <div class="info__content content_fixed">
            <div class="content">
              <p><b>Раз, два, три. Раз, два, три... С некоторыми моими участницами я занимаюсь в зале. Сейчас у меня 3 группы. Я провожу с ними по 2 урока в неделю.</b></p>
              <p>Конечно, такие тренировки отличаются от занятий онлайн. Здесь нельзя нажать кнопку «пауза», как на телефоне, и долго отдыхать. Здесь приходится выкладываться на полную — ведь у тебя всего 1,5 часа на урок.</p>
            </div>
          </div>
          <div class="info__aside">
            <div class="info__preview">
              <div class="info__image" style="background-image: url(/local/templates/main2019/assets/images/training_preview.jpg);" data-new-modal="#Training" data-url="https://www.youtube.com/embed/NuYet9twRYw?enablejsapi=1"></div>
            </div>
          </div>
          <div class="info__content content_fixed">
            <div class="content">
              <h4>Вот еще несколько фактов о занятиях в зале:</h4>
              <ul>
                <li>профессиональный контроль техники выполнения упражнений</li>
                <li>индивидуальный подход и учет особенностей здоровья и уровеня физической подготовки каждого</li>
                <li>вы достигате больших результатов занимаясь в группе единомышленников</li>
              </ul>
              <br />
              <p>Сейчас у меня 5 больших групп.</p>
            </div>
          </div>
        <h4>Расписание занятий Barre + Ballet г. Уфа</h4>
        <br />
        <p><img src="/local/templates/main2019/assets/images/training_table.jpg" width="100%" style="max-width: 1042px;" /></p>
        </div>
      </div>
      <div class="wrap__item container">
        <div class="gallery" data-photoswipe="">
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_01_min.jpg);" data-image="/local/templates/main2019/assets/images/training_01_big.jpg" data-size="1000x1249"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_02_min.jpg);" data-image="/local/templates/main2019/assets/images/training_02_big.jpg" data-size="1116x742"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_03_min.jpg);" data-image="/local/templates/main2019/assets/images/training_03_big.jpg" data-size="1000x668"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_04_min.jpg);" data-image="/local/templates/main2019/assets/images/training_04_big.jpg" data-size="1000x668"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_05_min.jpg);" data-image="/local/templates/main2019/assets/images/training_05_big.jpg" data-size="1000x659"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_06_min.jpg);" data-image="/local/templates/main2019/assets/images/training_06_big.jpg" data-size="1000x667"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_07_min.jpg);" data-image="/local/templates/main2019/assets/images/training_07_big.jpg" data-size="1000x750"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_08_min.jpg);" data-image="/local/templates/main2019/assets/images/training_08_big.jpg" data-size="1188x792"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_09_min.jpg);" data-image="/local/templates/main2019/assets/images/training_09_big.jpg" data-size="1188x792"></div>
          </div>
          <div class="gallery__item">
            <div class="gallery__image" style="background-image: url(/local/templates/main2019/assets/images/training_10_min.jpg);" data-image="/local/templates/main2019/assets/images/training_10_big.jpg" data-size="1188x792"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>