<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

function generateRandomCode($length = 6) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$result = array(
	'status' => 'error',
	'text' => 'Ничего не выполнилось',
);

$code = generateRandomCode();


$ch = curl_init("https://sms.ru/sms/send");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array(
    "api_id" => "609353E2-EB5E-C545-5212-B158F685E81E",
    "to" => $_POST['phone'], // До 100 штук до раз
    "msg" => "Код подтверждения ". $code, // Если приходят крякозябры, то уберите iconv и оставьте только "Привет!",
    /*
    // Если вы хотите отправлять разные тексты на разные номера, воспользуйтесь этим кодом. В этом случае to и msg нужно убрать.
    "multi" => array( // до 100 штук за раз
        "79378499084"=> iconv("windows-1251", "utf-8", "Привет 1"), // Если приходят крякозябры, то уберите iconv и оставьте только "Привет!",
        "74993221627"=> iconv("windows-1251", "utf-8", "Привет 2") 
    ),
    */
    "json" => 1 // Для получения более развернутого ответа от сервера
)));
$body = curl_exec($ch);
curl_close($ch);

$json = json_decode($body);
if ($json) { // Получен ответ от сервера
    // debug($json); // Для дебага
    if ($json->status == "OK") { // Запрос выполнился
        // foreach ($json->sms as $phone => $data) { // Перебираем массив СМС сообщений
        //     if ($data->status == "OK") { // Сообщение отправлено
        //         echo "Сообщение на номер $phone успешно отправлено. ";
        //         echo "ID сообщения: $data->sms_id. ";
        //         echo "";
        //     } else { // Ошибка в отправке
        //         echo "Сообщение на номер $phone не отправлено. ";
        //         echo "Код ошибки: $data->status_code. ";
        //         echo "Текст ошибки: $data->status_text. ";
        //         echo "";
        //     }
		// }
		$result['status'] = 'success';
		$result['code'] = $code;
		$result['text'] = "ok";
		// $result['text'] = "Баланс после отправки: $json->balance руб.";
		
    } else { // Запрос не выполнился (возможно ошибка авторизации, параметрах, итд...)
        $result['text'] = "Запрос не выполнился. ";      
        $result['text'] .= "Код ошибки: $json->status_code. ";
        $result['text'] .= "Текст ошибки: $json->status_text. ";
    }
} else { 

    $result['text'] = "Запрос не выполнился. Не удалось установить связь с сервером. ";
}


echo json_encode($result);

?>