<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$result = array(
	'status' => false,
	'text' => 'Ничего не выполнилось',
);


if(isset($_POST['coupon']) && strlen($_POST['coupon']) > 0 && CModule::IncludeModule("catalog")) {
	CCatalogDiscount::ClearCoupon();
	$res = CCatalogDiscountCoupon::SetCoupon($_POST['coupon']);

	if($res !== false) {
		$result['status'] = true;
		$result['text'] = setCoupon($_POST['coupon'], $USER);
	}
}

echo json_encode($result);

?>