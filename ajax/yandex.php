<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use YandexCheckout\Client;

$client = new Client();
$client->setAuth('685527', 'test_k3rEfamxPUQUVHse5fRRhN3PgzClIkcKWYZQdMHHJTY');
$payment = $client->createPayment(
	array(
		'amount' => array(
			'value' => 100.0,
			'currency' => 'RUB',
		),
		'confirmation' => array(
			'type' => 'redirect',
			'return_url' => 'https://babballet.ru/return_url',
		),
		'capture' => true,
		'description' => 'Заказ №1',
	),
	uniqid('', true)
);
?>