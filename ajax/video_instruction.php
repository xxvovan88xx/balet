<?php require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

$result = array(
	'status' => 'error',
	'text' => 'Ничего не выполнилось',
);

if(isset($_POST['time']) && $_POST['time'] == 'forever') {
	$user = new CUser;
	$fields = Array( 
		"UF_VIDEO_INST" => 1, 
	); 
	$result['text'] = $user->Update(CUser::GetID(), $fields);
} else {
	session_start();
	$_SESSION['UF_VIDEO_INST'] = 1;
	$result['text'] = $_SESSION['UF_VIDEO_INST'];
}
$result['status'] = 'success';

echo json_encode($result);

?>