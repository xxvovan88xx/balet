<?

use \Bitrix\Main\Sendpulse\ApiClient;
use \Bitrix\Main\Sendpulse\Storage\FileStorage;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отписка");
?>
<div class="unsubscribe_block">
<?php 

if(isset($_GET['email'])) {

	$users = CUser::GetList(
		($by = "NAME"),
		($order = "desc"),
		array(
			"EMAIL" => $_GET['email']
		)
	);
	
	while ($user = $users->Fetch()) {

		/* Sendpulse start */
		define('API_USER_ID', '16869f140c16714ce057285f4132a2e1');
		define('API_SECRET', '538bbe1264649b40b7e3773bf5e6382f');
		define('PATH_TO_ATTACH_FILE', __FILE__);

		$SPApiClient = new ApiClient(API_USER_ID, API_SECRET, new FileStorage());

		$event_email = array(
			'email' => $user['EMAIL'],
			'name' => $user['NAME']
		);

		$book_id = 498969;
		$book_emails = array(
			array(
				'email' => $arResult["USER"]['EMAIL'],
				'variables' => array(
					'name' => $arResult["USER"]['NAME'],
				)
			)
		);

		$response = $SPApiClient->startEventAutomation360('otpiska', $event_email);

		$response = json_decode(json_encode($response), true);

		if($response['result']) {
			$SPApiClient->addEmails($book_id, $book_emails);
			echo 'Вы успешно отписались от рассылки';

		} elseif($response['message'] == 'Duplicate data') {
			echo 'Вы уже отписывались ранее от рассылки';
		}

		/* Sendpulse end */

	}
}

?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>