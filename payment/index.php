<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Оформление подписки");

global $USER;
if ( ! $USER->IsAuthorized() || KROrder::isBasketEmpty()) {
	LocalRedirect("/subscription/");
	exit();
}
?>

<?$APPLICATION->IncludeComponent(
	'kr:subscribe',
	'payment',
	array(
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>