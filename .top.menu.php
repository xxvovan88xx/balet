<?
$aMenuLinks = Array(
	Array(
		"Онлайн-тренировки", 
		"/personal/training/", 
		Array(
			[
				'TEXT' => 'Все тренировки',
				'LINK' => '/personal/training/',
			],
			[
				'TEXT' => 'Мои тренировки',
				'LINK' => '/personal/my-training/',
			],
			[
				'TEXT' => 'Мой плейлист',
				'LINK' => '/personal/playlist/',
			],
		), 
		Array("TYPE"=>"dropdown"), 
		"" 
	),
	Array(
		"Тренировки в зале", 
		"/training/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Вопросы и ответы", 
		"/faq/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Стоимость", 
		"/subscription/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Отзывы", 
		"/reviews/", 
		Array(), 
		Array(), 
		"" 
	),
	Array(
		"Блог", 
		"/blog/", 
		Array(), 
		Array(), 
		"" 
	)
);
?>