<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мои тренировки");
?>


<main class="main gray" role="main">
	<div class="wrap">
		<div class="wrap__block wrap__block--white" data-space="bottom-small">
			<div class="wrap__heading">
				<h1 class="wrap__title">Онлайн-тренировки</h1>
			</div>
		</div>
		<div class="wrap__nav">
			<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH.'/include/personal/menu/index.php',
				Array(
					"CUR_PAGE" => '/personal/my-training/',
					"MENU" => 'training'
				), 
				Array(
					"MODE" => "php",
				)
			);?>
		</div>
		
		<?php
		$APPLICATION->IncludeComponent(
			'kr:training',
			'.default',
			array(
				'FILTER_TYPE' 	=> 'DAYS_FILTER',
				'PAGE' 			=> 'my-training',
				'SORT_BY' 		=> 'SORT',
				'SORT_ORDER' 	=> 'ASC',
			),
			false
		);
		?>
	</div>
</main>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>