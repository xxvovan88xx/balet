<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Персональный раздел");
?>


<main class="main gray" role="main">
	<div class="wrap">
		<div class="wrap__block wrap__block--white" data-space="bottom-small">
			<div class="wrap__heading">
				<h1 class="wrap__title">Личный кабинет</h1>
			</div>
		</div>
		<div class="wrap__nav">
			<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH.'/include/personal/menu/index.php',
				Array(
					"CUR_PAGE" => '/personal/account-history/',
					"MENU" => 'personal'
				), 
				Array(
					"MODE" => "php",
				)
			);?>
		</div>
	
	<?
	$APPLICATION->IncludeComponent(
		'kr:subscribe',
		'account_history',
		array(
			"PAGE" => "account_history"
		),
		false
	);
	?>
		
	<?
	/*$APPLICATION->IncludeComponent("bitrix:sale.personal.order.list","",Array(
			"STATUS_COLOR_N" => "green",
			"STATUS_COLOR_P" => "yellow",
			"STATUS_COLOR_F" => "gray",
			"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
			"PATH_TO_DETAIL" => "order_detail.php?ID=#ID#",
			"PATH_TO_COPY" => "basket.php",
			"PATH_TO_CANCEL" => "order_cancel.php?ID=#ID#",
			"PATH_TO_BASKET" => "basket.php",
			"PATH_TO_PAYMENT" => "payment.php",
			"ORDERS_PER_PAGE" => 100,
			//"ID" => $ID,
			"SET_TITLE" => "Y",
			"SAVE_IN_SESSION" => "Y",
			"NAV_TEMPLATE" => "",
			"CACHE_TYPE" => "A",
			"CACHE_TIME" => "3600",
			"CACHE_GROUPS" => "Y",
			"HISTORIC_STATUSES" => "F",
			"ACTIVE_DATE_FORMAT" => "d.m.Y"
		)
	);
	*/?>

	</div>
</main>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>