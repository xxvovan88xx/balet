<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
//$APPLICATION->SetTitle("Персональный раздел");
?>


<main class="main gray" role="main">
	<div class="wrap">
		<div class="wrap__block wrap__block--white" data-space="bottom-small">
			<div class="wrap__heading">
				<h1 class="wrap__title">Личный кабинет</h1>
			</div>
		</div>
		<div class="wrap__nav">
			<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH.'/include/personal/menu/index.php',
				Array(
					"CUR_PAGE" => '/personal/profile/',
					"MENU" => 'personal'
				), 
				Array(
					"MODE" => "php",
				)
			);?>
		</div>
		
	<?$APPLICATION->IncludeComponent(
		"bitrix:main.profile",
		"",
		Array(
			"SET_TITLE" => "Y", 
		)
	);?>

	</div>
</main>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>