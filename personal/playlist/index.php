<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Мой плейлист");
?>


<main class="main gray" role="main">
	<div class="wrap">
		<div class="wrap__block wrap__block--white" data-space="bottom-small">
			<div class="wrap__heading">
				<h1 class="wrap__title">Онлайн-тренировки</h1>
			</div>
		</div>
		<div class="wrap__nav">
			<?$APPLICATION->IncludeFile(SITE_TEMPLATE_PATH.'/include/personal/menu/index.php',
				Array(
					"CUR_PAGE" => '/personal/playlist/',
					"MENU" => 'training'
				), 
				Array(
					"MODE" => "php",
				)
			);?>
		</div>
		
		<?php
		//плейлист
		if(!empty($_GET['ID'])){
			$APPLICATION->IncludeComponent(
				'kr:playlist',
				'player',
				array(
					
				),
				false
			);
		
		//все плейлисты
		}else{
			$APPLICATION->IncludeComponent(
				'kr:playlist',
				'.default',
				array(
					
				),
				false
			);
		}
		?>
	</div>
</main>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>