<?php 
// global $_SERVER;
$_SERVER["DOCUMENT_ROOT"] = '/var/www/u0805077/data/www/babballet.ru';

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Sendpulse\ApiClient;
use \Bitrix\Main\Sendpulse\Storage\FileStorage;

define('API_USER_ID', '16869f140c16714ce057285f4132a2e1');
define('API_SECRET', '538bbe1264649b40b7e3773bf5e6382f');
define('PATH_TO_ATTACH_FILE', __FILE__);

$SPApiClient = new ApiClient(API_USER_ID, API_SECRET, new FileStorage());
$book_id = 498962;
$emails = array();


$now = strtotime("now");
$date_30_days = strtotime('-30 days', $now);
$date_32_days = strtotime('-32 days', $now);


$users = CUser::GetList(
	($by="id"), 
	($order="asc"), 
	array(
		"ACTIVE"              => "Y",
		// "LOGIN"               => "nessy | admin",
		"GROUPS_ID"           => Array(KRUser::$GROUP_FREE_ACCESS, KRUser::$GROUP_SUBSCRIBE_USERS)
	)
);

while($user = $users->Fetch()) {

	$last_login = strtotime($user['LAST_LOGIN']);

	if($date_30_days > $last_login && $last_login > $date_32_days) {
		// debug($user, true);

		$emails[] = array(
			'email' => $user['EMAIL'],
			'variables' => array(
				'name' => $user['NAME'],
			)
		);

		echo $user['EMAIL'] .' '. $user['LAST_LOGIN'] .'<br>';
	}
}


if(!empty($emails)) {
	// $SPApiClient->addEmails($book_id, $emails);
}