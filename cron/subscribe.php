<?php 
// global $_SERVER;
// $_SERVER["DOCUMENT_ROOT"] = '/var/www/u0805077/data/www/babballet.ru';

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main\Sendpulse\ApiClient;
use \Bitrix\Main\Sendpulse\Storage\FileStorage;

$users = CUser::GetList(
	($by="id"), 
	($order="asc"), 
	array(
		"ACTIVE"              => "Y",
		// "LOGIN"               => "nessy | admin",
		"GROUPS_ID"           => Array(7, 8)
	)
);

$now = strtotime("now");
$new_date = strtotime('+3 day', $now);
$date_72 = strtotime('+72 hours', $now);
$date_49 = strtotime('+49 hours', $now);

// debug($date_72 .' '. $date_49, true);

while($user = $users->Fetch()) {
	// debug($rsUser);
	$user['GROUPS'] = array();
	$user_groups_db = CUser::GetUserGroupList($user['ID']);
	while ($user_group = $user_groups_db->Fetch()){
		if(
			($user_group['GROUP_ID'] == 7 || 
			$user_group['GROUP_ID'] == 8) &&
			$user_group['DATE_ACTIVE_TO'] !== null
		) {
			$user['GROUPS'][] = $user_group;
		}
	}

	if(count($user['GROUPS']) > 0) {
		foreach($user['GROUPS'] as $group) {
			$active_to = strtotime($group['DATE_ACTIVE_TO']);
			$diff = $active_to - $new_date;
	
			if($date_72 > $active_to && $active_to > $date_49) {
				if($group['GROUP_ID'] == 7 && in_array(8, CUser::GetUserGroup($user['ID']))) {
					continue;
				}
				$str = $user['EMAIL'] .' '. ($group['GROUP_ID'] == 8 ? 'Платная' : 'Бесплатная') . ' подписка заканчивается через 3 дня';
				echo  $str .'<br>';

				/* Sendpulse start */
				define('API_USER_ID', '16869f140c16714ce057285f4132a2e1');
				define('API_SECRET', '538bbe1264649b40b7e3773bf5e6382f');
				define('PATH_TO_ATTACH_FILE', __FILE__);
				
				$SPApiClient = new ApiClient(API_USER_ID, API_SECRET, new FileStorage());
		
				$book_id = 497125;
				$emails = array(
					array(
						'email' => $user['EMAIL'],
						'variables' => array(
							'name' => $user['NAME'],
						)
					)
				);

				$SPApiClient->addEmails($book_id, $emails);
				/* Sendpulse end */
			}
		}
	}
}