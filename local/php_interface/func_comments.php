<?


//сортировка по parent_id
function sortComments($items, $newitems, $parent_id)
{
	if($items){
		foreach($items as $key=>$item){
			if((int)($item['PROPERTY_PARENT_ID_VALUE']) == $parent_id){
				$newitems[] = $item;
				//unset($items[$key]);
				$newitems = sortComments($items, $newitems, $item['ID']);
			}
		}
	}
	return $newitems;
}

//валидация имени для комментария
//имени не разрешать ничего кроме a-zа-я0-9
function validateCommentName($name)
{
	if(preg_match("/^[[А-Яа-яЁёa-zA-Z ]+$/u", $name)){
		return true;
		//print "<br>{$name} - прошел валидацию";
	}else{
		//print "<br>{$name} - ne прошел валидацию";
	}
	
	return false;
}


//получение количества элементов 
function getCountComments($user_id = false, $blog_id = false)
{
	$res_count = 0;
	
	if( ! $blog_id) return $res_count;
	
	CModule::IncludeModule("iblock"); 
	
	$arFilter = Array("IBLOCK_ID"=>KRIBlock::$ID_COMMENTS, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_BLOG"=>$blog_id);
	if($user_id){
		$arFilter["PROPERTY_USER"] = $user_id;
	}
	$res_count = CIBlockElement::GetList(Array(), $arFilter, Array(), false, Array());
	
	return $res_count;
}
