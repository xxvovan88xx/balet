<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
}

abstract class KRPayments
{
	static $PAYSYSTEM_SBERBANK_ID = PAYSYSTEM_SBERBANK_ID; /* оплата Сбербанком */
	static $PAYSYSTEM_SBERBANK_API_USER = PAYSYSTEM_SBERBANK_API_USER;
	static $PAYSYSTEM_SBERBANK_API_PASS = PAYSYSTEM_SBERBANK_API_PASS;
	static $PAYSYSTEM_SBERBANK_API_KEY = PAYSYSTEM_SBERBANK_API_KEY;
	static $PAYSYSTEM_SBERBANK_API_URL = PAYSYSTEM_SBERBANK_API_URL;
	static $PAYSYSTEM_SBERBANK_URL_SUCCESS = '#SITE#/payment/success.php?ORDER_ID=#ORDER_ID#&HASH=#HASH#';
	static $PAYSYSTEM_SBERBANK_URL_FAIL = '#SITE#/payment/fail.php?ORDER_ID=#ORDER_ID#&HASH=#HASH#';
	
	static $PAYSYSTEM_LOG = "/upload/payments/pay.log";
	

	static function init() {
		
	}
	
	
	/* по заказа определяем какую форму для оплаты нужно вывести */
	static function payform($order_id)
	{
		$result = false;
		$order = \Bitrix\Sale\Order::load((int)($order_id));
		if($order)
		{
			$paymentCollection = $order->getPaymentCollection();
			$payment = $paymentCollection[0];
			switch($payment->getPaymentSystemId()){
				case self::$PAYSYSTEM_SBERBANK_ID : $result = self::sberbankUrlGenerate($order); break;
			}
		}
		
		return $result;
	}

	/* генерация url для сбербанка */
	static function sberbankUrlGenerate($order = false)
	{
		global $USER;
		
		if($order){
			$propertyCollection = $order->getPropertyCollection();
			$property_sberbank_id = $propertyCollection->getItemByOrderPropertyId(KROrder::$PROPERTY_SBERBANK_ID);
			$property_sberbank_form = $propertyCollection->getItemByOrderPropertyId(KROrder::$PROPERTY_SBERBANK_FORM);
			
			$order_id = $order->getField('ID');
			
			$payment_params = array(
				'userName' => self::$PAYSYSTEM_SBERBANK_API_USER,
				'password' => self::$PAYSYSTEM_SBERBANK_API_PASS,
				'orderNumber' => $order_id . '_' . time(),
				'amount' => round($order->getField('PRICE') * 100), /* в копейках */
				'currency' => 643, /* цифровой формат согласно ISO 4217 */
				'returnUrl' => str_replace('#SITE#', "http://".$_SERVER['HTTP_HOST'], str_replace('#ORDER_ID#', $order_id, str_replace('#HASH#', $order->getHash(), self::$PAYSYSTEM_SBERBANK_URL_SUCCESS))),
				'failUrl' => str_replace('#SITE#', "http://".$_SERVER['HTTP_HOST'], str_replace('#ORDER_ID#', $order_id, str_replace('#HASH#', $order->getHash(), self::$PAYSYSTEM_SBERBANK_URL_FAIL))),
				'clientId' => $USER->GetID()
			);
			
			
			/////////////////////////////////////////////////////
			//TEST
			//$property_sberbank_id->setValue('55555');
			//$property_sberbank_form->setValue('http://balet.my/sberbank');
			//$order->save();
			//return $payment_params['returnUrl'];
			////////////////////////////////////////////////////
			
			$result = array();
			
			$session = curl_init();
			curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($session, CURLOPT_POST, 1);
			curl_setopt($session, CURLOPT_TIMEOUT, 10);
			curl_setopt($session, CURLOPT_POSTFIELDS, $payment_params);
			curl_setopt($session, CURLOPT_URL, self::$PAYSYSTEM_SBERBANK_API_URL);
			$answer = curl_exec($session);
			if($answer !== false) {
				$result = json_decode($answer, true);
				
				if(isset($result['orderId'])) {
					$order_sberbank_id = $result['orderId'];
					$order_sberbank_form = $result['formUrl'];
					
					$property_sberbank_id->setValue($order_sberbank_id);
					$property_sberbank_form->setValue($order_sberbank_form);
					$order->save();
				}
			}
			curl_close($session);
			return $order_sberbank_form;
		}
	}
}

KRPayments::init();
	