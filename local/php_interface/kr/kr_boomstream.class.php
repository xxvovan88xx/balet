<?
class KRBoomstream
{
	private $apikey;			//ключ
	private $pass_rule_code; 	//Код платного доступа
	
	public function __construct($code = false)
	{
		if(defined("BOOMSTREAM_API_KEY") && defined("BOOMSTREAM_PASS_RULE_CODE")){
			$this->apikey = BOOMSTREAM_API_KEY;
			$this->pass_rule_code = BOOMSTREAM_PASS_RULE_CODE;
		}
		if($code){
			$this->pass_rule_code = $code;
		}
	}
	
	public function query($params)
	{	
		if(empty($params['ppv'])) return false;
		
		$result = array();
		
		$params['apikey'] = $this->apikey;
		
		$url = "https://boomstream.com/api/ppv/{$params['ppv']}?" . http_build_query($params);
		
		$session = curl_init();
		curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($session, CURLOPT_TIMEOUT, 10);
		curl_setopt($session, CURLOPT_URL, $url);
		$answer = curl_exec($session);
		if($answer !== false) {
			$xml = simplexml_load_string($answer);
			$json = json_encode($xml);
			$result = json_decode($json,TRUE);
		}
		curl_close($session);
		
		return $result;
		
	}
	
	
	//получить список медиа-файлов платного доступа. 
	public function listmedia()
	{
		$result = array();
		
		$params = array();
		$params['ppv'] = 'listmedia';
		$params['code'] = $this->pass_rule_code;
		$medias = $this->query($params);
		if($medias['Status'] == 'Success')
		{	
			//один item
			if(!empty($medias['Medias']['Item']['Code'])){
				$result = array($medias['Medias']['Item']);
			}
			//множество items
			else{
				$result = $medias['Medias']['Item'];
			}
		}
		
		return $result;
	}
	
	
	//Добавить покупателя в платный доступ.
	public function addbuyer($media, $email, $access_expire)
	{
		if(empty($media) || empty($email) || empty($access_expire)) return false;
		
		$result = array();
		
		$params = array();
		$params['ppv'] = 'addbuyer';
		$params['code'] = $this->pass_rule_code;
		$params['media'] = $media;
		$params['email'] = $email;
		//Нотифицировать на электронную почту
		$params['notification'] = 0;
		//дата окончания доступа, YYYY-MM-DD, например 2018-12-31
		$params['access_expire'] = $access_expire;
		$addbuyer = $this->query($params);
		if($addbuyer['Status'] == 'Success'){
			$result = $addbuyer;
		}
		
		return $result;
	}

	public function listbuyers($media) {
		echo 'hello';
		if(empty($media)) return false;
		
		$result = array();
		
		$params = array();
		$params['ppv'] = 'addbuyer';
		$params['code'] = $this->pass_rule_code;
		$params['media'] = $media;
		//Нотифицировать на электронную почту
		$params['notification'] = 0;
		$addbuyer = $this->query($params);
		if($addbuyer['Status'] == 'Success'){
			$result = $addbuyer;
		}
		
		return $result;
	}
	
	
}