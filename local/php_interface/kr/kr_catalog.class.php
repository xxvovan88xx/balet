<?
class KRSubsribeCatalog
{
    static private $instance = NULL;
    private $priceTypes = array();
    private $prices = array();

    static function getInstance(){
        CModule::IncludeModule('catalog');
        CModule::IncludeModule('sale');
        CModule::IncludeModule('currency');

        if (self::$instance == NULL)
            self::$instance = new KRSubsribeCatalog();
        return self::$instance;
    }

    public function getPriceTypes(){
        if(empty($this->priceTypes)){
            $rs = CCatalogGroup::GetList(
                array('SORT' => 'ASC'),
                array('CAN_ACCESS' => 'Y', 'CAN_BUY' => 'Y')
            );
            while($ob = $rs->GetNext()){
                $this->priceTypes[$ob['ID']] = $ob;
            }
        }

        return $this->priceTypes;
    }


    public function getProductSetPrice($productId)
	{
		$price = 0;
		
		$arSets = CCatalogProductSet::getAllSetsByProduct($productId, CCatalogProductSet::TYPE_GROUP); // массив комплектов данного товара
		$arItems = array_shift($arSets); // комплект данного товара
		if(isset($arItems['ITEMS'])){
			foreach($arItems['ITEMS'] as $item){
				$price += (KRSubsribeCatalog::getInstance()->getProductPrice($item['ITEM_ID']) * $item['QUANTITY']);
			}
		}
		
		return $price;
	}
	
    public function getProductSetOriginalPrice($productId)
	{
		$price = 0;
		
		$arSets = CCatalogProductSet::getAllSetsByProduct($productId, CCatalogProductSet::TYPE_GROUP); // массив комплектов данного товара
		$arItems = array_shift($arSets); // комплект данного товара
		if(isset($arItems['ITEMS'])){
			
			$arID = array();
			foreach($arItems['ITEMS'] as $item){
				$arID[] = $item['ITEM_ID'];
			}
			
			//Получить все данные по повару
			$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'], 'ACTIVE' => 'Y', 'ID' => $arID);
			$arSelect = array('ID', 'NAME');
			$rsElement = CIBlockElement::GetList(false, $arFilter);
			while($obElement = $rsElement->GetNextElement()){ 
				$arItem = $obElement->GetFields();
				$arItem['PROP'] = $obElement->GetProperties();

				$ORIGINAL_PRICE = $arItem['PROP']['SECOND_PRICE']['VALUE'];
				$BASE_PRICE = KRSubsribeCatalog::getInstance()->getProductPrice($arItem['ID']);
				$origPrice = KRSubsribeCatalog::getInstance()->getProductOriginalPrice($arItem['ID']);
				if(!empty($origPrice))
					$ORIGINAL_PRICE = $origPrice;
				
				//внести данные по товару в общий массив
				foreach($arItems['ITEMS'] as $key=>$item){
					if($item['ITEM_ID'] == $arItem['ID']){
						$price += ($ORIGINAL_PRICE > 0) ? ($ORIGINAL_PRICE * $item['QUANTITY']) : ($BASE_PRICE * $item['QUANTITY']);
						break;
					}
				}
			}
		}
		
		return $price;
	}
	
    public function getProductPrice($productId, $arPrices = array(), $is_set_calc = true){
        $price = 0;
        $price_set = 0;

        if($is_set_calc && CCatalogProductSet::isProductHaveSet($productId, CCatalogProductSet::TYPE_GROUP)){
            //$arSets = CCatalogProductSet::getAllSetsByProduct($productId, CCatalogProductSet::TYPE_GROUP);
            //return $price; // TODO: считать цену
			$price_set = $this->getProductSetPrice($productId);

        }

        $maybeOffers = CCatalogSKU::getOffersList($productId);
        if(isset($maybeOffers[$productId])){
            foreach($maybeOffers[$productId] as &$offer){
                $offer['PRICE'] = $this->getOptimalRubPrice($offer['ID'], $arPrices);
                if($offer['PRICE'] < $price || $price == 0){
                    $price = $offer['PRICE'];
                }
            }
        } else {
            $price = $this->getOptimalRubPrice($productId, $arPrices);
        }


        return ($price + $price_set);
    }

    public function getProductOriginalPrice($productId, $is_set_calc = true){
        $price = 0;

		$price_set = 0;

        if($is_set_calc && CCatalogProductSet::isProductHaveSet($productId, CCatalogProductSet::TYPE_GROUP)){
            // CCatalogProductSet::getAllSetsByProduct($productId, CCatalogProductSet::TYPE_GROUP)
            //return $price; // TODO: считать цену
			$price_set = $this->getProductSetOriginalPrice($productId);
        }

        $maybeOffers = CCatalogSKU::getOffersList($productId);
        if(isset($maybeOffers[$productId])){
            foreach($maybeOffers[$productId] as &$offer){
                if(!isset($this->prices[$offer['ID']])){
                    $offer['PRICE'] = $this->getOptimalRubPrice($offer['ID']);
                }

                if(isset($this->prices[$offer['ID']]) && $this->prices[$offer['ID']]['RESULT_PRICE']['DISCOUNT_PRICE'] != $this->prices[$offer['ID']]['RESULT_PRICE']['BASE_PRICE']){

                    return ($this->prices[$offer['ID']]['RESULT_PRICE']['BASE_PRICE'] + $price_set);

                }
            }
        } else {
            if(!isset($this->prices[$productId])){
                $price = $this->getOptimalRubPrice($productId);
            }

            if(isset($this->prices[$productId]) && $this->prices[$productId]['RESULT_PRICE']['DISCOUNT_PRICE'] != $this->prices[$productId]['RESULT_PRICE']['BASE_PRICE']){

                return ($this->prices[$productId]['RESULT_PRICE']['BASE_PRICE'] + $price_set);
            }
        }

        return ($price + $price_set);
    }

    private function getOptimalRubPrice($productId, $arPrices = array()){
        global $USER;
        $price = 0;

        if(is_object($USER) && $USER->IsAuthorized()) $groups = $USER->GetUserGroupArray();
        else $groups = array();

        //if(!isset($this->prices[$productId])){
            $tmpPrice = CCatalogProduct::GetOptimalPrice($productId, 1, $groups, 'N', $arPrices, CUSTOM_SITE_ID, false);
            if(isset($tmpPrice['RESULT_PRICE']['DISCOUNT_PRICE'])){
                if($tmpPrice['RESULT_PRICE']['CURRENCY'] != 'RUB') {
                    $tmpPrice['RESULT_PRICE']['DISCOUNT_PRICE'] = CCurrencyRates::ConvertCurrency($tmpPrice['RESULT_PRICE']['DISCOUNT_PRICE'], $tmpPrice['RESULT_PRICE']['CURRENCY'], 'RUB');
                    $tmpPrice['RESULT_PRICE']['CURRENCY'] = 'RUB';
                }

                $price = $tmpPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
            } elseif(isset($tmpPrice['RESULT_PRICE']['BASE_PRICE'])){
                if($tmpPrice['RESULT_PRICE']['CURRENCY'] != 'RUB') {
                    $tmpPrice['RESULT_PRICE']['BASE_PRICE'] = CCurrencyRates::ConvertCurrency($tmpPrice['RESULT_PRICE']['BASE_PRICE'], $tmpPrice['RESULT_PRICE']['CURRENCY'], 'RUB');
                    $tmpPrice['RESULT_PRICE']['CURRENCY'] = 'RUB';
                }

                $price = $tmpPrice['RESULT_PRICE']['BASE_PRICE'];
            } elseif(isset($tmpPrice['PRICE']['PRICE'])){
                if($tmpPrice['PRICE']['CURRENCY'] != 'RUB') {
                    $tmpPrice['PRICE']['PRICE'] = CCurrencyRates::ConvertCurrency($tmpPrice['PRICE']['PRICE'], $tmpPrice['PRICE']['CURRENCY'], 'RUB');
                    $tmpPrice['PRICE']['CURRENCY'] = 'RUB';
                }

                $price = $tmpPrice['PRICE']['PRICE'];
            }

            $tmpPrice['RES_PRICE'] = $price;

            $this->prices[$productId] = $tmpPrice;
        /*} else {
            $price = $this->prices[$productId]['RES_PRICE'];
        }*/

        return $price;
    }

    public function formatPrice($price){
        if(empty($price) || $price == '0.0') return '&mdash;';

        return MyString::priceFormat($price).' <span class="rouble">р</span>';
    }

    public function formatPriceNoUnits($price){
        if(empty($price) || $price == '0.0') return '&mdash;';

        return MyString::priceFormat($price);
    }

    public function getProductInfo($productId){
        if(!isset($this->prices[$productId])){
            $this->getOptimalRubPrice($productId);
        }

        return $this->prices[$productId];
    }

    public static function addOrderProperty($code, $value, $order) {
        if(empty($code)) return false;

        if(CModule::IncludeModule('sale')){
            if($arProp = CSaleOrderProps::GetList(array('ID' => 'DESC'), array('CODE' => $code))->Fetch()){
                $rsOrderPropsValues = CSaleOrderPropsValue::GetList(array('ID' => 'DESC'), array('ORDER_ID' => $order, 'ORDER_PROPS_ID' => $arProp['ID']));
                if($obOrderPropsValues = $rsOrderPropsValues->GetNext()){
                    return CSaleOrderPropsValue::Update($obOrderPropsValues['ID'], array(
                        'NAME' => $arProp['NAME'],
                        'CODE' => $arProp['CODE'],
                        'ORDER_PROPS_ID' => $arProp['ID'],
                        'ORDER_ID' => $order,
                        'VALUE' => $value,
                    ));
                } else {
                    return CSaleOrderPropsValue::Add(array(
                        'NAME' => $arProp['NAME'],
                        'CODE' => $arProp['CODE'],
                        'ORDER_PROPS_ID' => $arProp['ID'],
                        'ORDER_ID' => $order,
                        'VALUE' => $value,
                    ));
                }
            }
        }
    }

    /**
     * Возвращает ID пользователя
     * - если авторизован, вызываем $USER->GetID()
     * - если не передан $email и юзер не авторизован, ищем дефолтного юзера с логином ONECLICK
     * - если передан $email, ищем и возвращаем его ID
     * - если передан $email и такой пользователь не найден, то регистрируем его
     * @param string $email
     * @param string $name
     * @return bool|null
     */
    public function getUserId($email = '', $name = ''){
        global $USER;
        if($USER->IsAuthorized())
            return $USER->GetID();

        if(empty($email)){
            $rsUser = CUser::GetList(($by = 'ID'), ($dir = 'ASC'), array('LOGIN' => 'ONECLICK'), array('SELECT' => array('ID')));
            if($obUser = $rsUser->Fetch()){
                return $obUser['ID'];
            } else {
                return false;
            }
        }

        if(!check_email($email))
            return false;

        $rsUser = CUser::GetList(($by = 'ID'), ($dir = 'ASC'), array('EMAIL' => $email), array('SELECT' => array('ID')));
        if($obUser = $rsUser->Fetch()){
            return $obUser['ID'];
        } else {
            $arErrors = array();
            $newUser = CSaleUser::DoAutoRegisterUser($email, $name, SITE_ID, $arErrors, array('ACTIVE' => 'N'));
            if(empty($arErrors))
                return $newUser;
            else
                return false;
        }
    }
}