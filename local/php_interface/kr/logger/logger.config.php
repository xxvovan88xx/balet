<?php
	/*
	*		Файл настроек
	*		
	*		ВНИМАНИЕ: Файл настроек должен лежать рядом с классом!
	*		
	*		Представлен полный набор данных
	*		Структура массива настроек:
	*
	*
	*		Для файлов:
	*		Если передан sessions = true, то будут создаваться файлы <date>_<session>.log
	*		Если передан sessions = false и name, то будут создан файлы <name>.log
	*		Если передан только sessions = false, то будут создан файлы <date>.log
	*
	*/
	
	$krasivoereshenie_logger_config = array(
		'destinations' => array(
			'file' => realpath(dirname(__FILE__) . '/../../../../') . '/upload/logs',
			'database_sql' => array(
				'host' => '',
				'base' => '',
				'user' => '',
				'pass' => ''
			)
		),
		'logs' => array(
			'fatal' => array(
				'log' => true,
				'file' => array(
					'name' => '',
					'sessions' => true
				)
			),
			'error' => array(
				'log' => true,
				'file' => array(
					'name' => '',
					'sessions' => true
				)
			),
			'note' => array(
				'log' => true,
				'file' => array(
					'name' => '',
					'sessions' => true
				)
			),
			'debug' => array(
				'log' => true,
				'file' => array(
					'name' => '',
					'sessions' => true
				)
			)
		)
	);
	