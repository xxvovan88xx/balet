<?php
	/*
	*		name:			Логгер данных
	*		date:			2019.09.06
	*		vaersion:	1.0
	*/
	namespace KrasivoeReshenie;
	
	abstract class Logger {
		/* уровени логов */
		const LEVEL_FATAL		= 1;		/* фатальные ошибки */
		const LEVEL_ERROR		= 2;		/* ошибки */ 
		const LEVEL_NOTE		= 3;		/* заметки */
		const LEVEL_DEBUG		= 4;		/* отладка */
		const LEVEL_UNKNOWN	= 100;	/* что-то пошло совсем не так */
		
		/*
		*		Настройки по умолчанию
		*		Переопредляются из файла logger.config.php
		*		Так же можно передать на прямую в init()
		*/
		/* Путь до папки логов */
		static private $_config_file = '';
		/* настройки подключения к БД */
		static private $_config_database_sql = array(
			'host' => 'localhost',
			'base' => '',
			'user' => '',
			'pass' => ''
		);
		/* настройки логирования, все закрыто */
		static private $_config_logs = array(
			'fatal' => array(
				'log' => false
			),
			'error' => array(
				'log' => false
			),
			'note' => array(
				'log' => false
			),
			'debug' => array(
				'log' => false
			)
		);
		
		/* параметры для сессии логов */
		static private $_param_session = '';
		static private $_param_datetime = '';
		
		
		/* 
		*		Инициализация параметров
		*		Входные параметры:
		*			$params	- array, 	массив для переопределения настроек. 
		*
		*		Приоритет: базовые настройки > файл настроек > параметры $params
		*/
		static function init($params = array()) {
			/* параметры для сессии логов */
			self::$_param_datetime = date('Ymd_His');
			self::$_param_session = session_id();
			if(empty(self::$_param_session)) {
				/* если нет сессии, то генерим свой код */
				self::$_param_session = 'R' . substr(md5(rand(0, 100) . rand(0, 100) . rand(0, 100)), 1);
			}
			
			/* проверяем конфигурационны файл */
			$config_file = realpath(dirname(__FILE__)) . '/logger.config.php';
			if(file_exists($config_file)) {
				require_once($config_file);
				/* настройки хранилищ */
				if(!empty($krasivoereshenie_logger_config['destinations'])) {
					/* папка логов */
					if(!empty($krasivoereshenie_logger_config['destinations']['file'])) {
						self::$_config_file = $krasivoereshenie_logger_config['destinations']['file'];
					}
					/* настройки БД */
					if(!empty($krasivoereshenie_logger_config['destinations']['database_sql'])) {
						foreach(self::$_config_database_sql as $field => $value) {
							if(!empty($krasivoereshenie_logger_config['destinations']['database_sql'][$field])) {
								self::$_config_database_sql[$field] = $krasivoereshenie_logger_config['destinations']['database_sql'][$field];
							}
						}
					}		
				}
				
				/* настройки уровней логирования */
				if(!empty($krasivoereshenie_logger_config['logs'])) {
					foreach(self::$_config_logs as $level => $value) {
						if(!empty($krasivoereshenie_logger_config['logs'][$level])) {
							$config = $krasivoereshenie_logger_config['logs'][$level];
							if(!empty($config['log'])) {
								/* логируем уровень */
								self::$_config_logs[$level] = array('log' => true);
								/* логирование в файл */
								if(!empty($config['file'])) {
									self::$_config_logs[$level]['file']['name'] = trim($config['file']['name'], '/');
									self::$_config_logs[$level]['file']['sessions'] = !empty($config['file']['sessions']);
								}
								/* логирование в БД SQL */
								if(!empty($config['database_sql'])) {
									self::$_config_logs[$level]['database_sql'] = $config['database_sql'];
								}
							}
							else {
								/* не логируем уровень */
								self::$_config_logs[$level] = array('log' => false);
							}
						}
						
					}
				}
			}
			else {
				self::_addSelf(self::LEVEL_NOTE, 'init()', 'Нет файла конфигурации: logger.config.php');
			}

			/* проверяем входящие параметры */
			if(!empty($params)) {
				/* настройки хранилищ */
				if(!empty($params['destinations'])) {
					/* папка логов */
					if(!empty($params['destinations']['file'])) {
						self::$_config_file = $params['destinations']['file'];
					}
					/* настройки БД */
					if(!empty($params['destinations']['database_sql'])) {
						foreach(self::$_config_database_sql as $field => $value) {
							if(!empty($params['destinations']['database_sql'][$field])) {
								self::$_config_database_sql[$field] = $params['destinations']['database_sql'][$field];
							}
						}
					}		
				}

				/* настройки уровней логирования */
				if(!empty($params['logs'])) {
					foreach(self::$_config_logs as $level => $value) {
						if(!empty($params['logs'][$level])) {
							$config = $params['logs'][$level];
							if(!empty($config['log'])) {
								/* логируем уровень */
								self::$_config_logs[$level] = array('log' => true);
								/* логирование в файл */
								if(!empty($config['file'])) {
									self::$_config_logs[$level]['file']['name'] = trim($config['file']['name'], '/');
									self::$_config_logs[$level]['file']['sessions'] = !empty($config['file']['sessions']);
								}
								/* логирование в БД SQL */
								if(!empty($config['database_sql'])) {
									self::$_config_logs[$level]['database_sql'] = $config['database_sql'];
								}
							}
							else {
								/* не логируем уровень */
								self::$_config_logs[$level] = array('log' => false);
							}
						}
						
					}
				}
			}
			return true;
		}
		
		/*
		*		Добавление логов
		*		Входные параметры:
		*			$level	- int, 		код уровня данных, см. константы LEVEL_*
		*			$tag 		- string,	метка для группировки логов
		*			$data		- mix,		данные для логирования
		*		Возвращает boolean:
		*			- true 				при успешной записи лога
		*			- false				при возниконовении ошибки записи лога 
		*/
		static function add($level, $tag, $data) {
			$result = true;
			/* получаем настройки уровня */
			$config = array('log' => false);
			switch($level) {
				case self::LEVEL_FATAL: $config = !empty(self::$_config_logs['fatal']) ? self::$_config_logs['fatal'] : $config; break;
				case self::LEVEL_ERROR: $config = !empty(self::$_config_logs['error']) ? self::$_config_logs['error'] : $config; break;
				case self::LEVEL_NOTE: $config = !empty(self::$_config_logs['note']) ? self::$_config_logs['note'] : $config; break;
				case self::LEVEL_DEBUG: $config = !empty(self::$_config_logs['debug']) ? self::$_config_logs['debug'] : $config; break;
			}
			/* логируем */
			if(!empty($config['log'])) {
				/* логируем в файл */
				if(!empty($config['file'])) {
					$dir = self::$_config_file . '/' . $tag;
					if(!file_exists($dir)) {
						mkdir($dir);
					}
					
					/* путь до файла */
					if(!empty($config['file']['sessions'])) {
						/* логирование по сессиям */
						$path = $dir . '/' . self::$_param_datetime . '_' . self::$_param_session . '.log';
					}
					else {
						/* логирование без в один файл */	
						if(!empty($config['file']['name'])) {
							$path = $dir . '/' . $config['file']['name'] . '.log';
						}
						else {
							$path = $dir . '/' . self::$_param_datetime . '.log';
						}
					}
					/* логируем в файл */
					self::_addToFile($path, $level, $tag, $data);
				}
				
				/* логируем в SQL-Like БД*/
				if(!empty($config['database_sql'])) {
					
				}
				
			}
			
			return $result;
		}
		
		/* 
		*		Запись лога в файл
		*		Входные параметры:
		*			$path		- string, путь до файла лога
		*			$level	- int, 		код уровня данных, см. константы LEVEL_*
		*			$tag 		- string,	метка для группировки логов
		*			$data		- mix,		данные для логирования
		*		Возвращает boolean:
		*			- true 				при успешной записи лога
		*			- false				при возниконовении ошибки записи лога 
		*/
		static private function _addToFile($path, $level, $tag, $data) {
			$result = false;
			$file = fopen($path, 'a');
			if(!empty($file)) {
				$result = true;
				fwrite($file, date('Y.m.d H:i:s') . ': ' . self::_formatLevel($level) . ' - ' . $tag . "\r\n");
				fwrite($file, self::_formatData($data) . "\r\n");
				fwrite($file, "\r\n");
				fclose($file);
			}
			else {
				self::_addSelf(self::LEVEL_ERROR, '_addToFile()', 'Ошибка открытия файла: ' . $path);
			}
			return $result;
		}
		
		/* Запись данных в БД SQL */
		static private function _addToDBSQL($table, $level, $tag, $data) {
			
		}
		
		/* 
		*		Текстовое представление уровня логирования по его коду
		* 	Входные параметры:
		*			$level	- int,		код уровня данных
		*		Возвращает string:
		*			- 'fatal'			для кода LEVEL_FATAL
		*			- 'error'			для кода LEVEL_ERROR
		*			- 'note'			для кода LEVEL_NOTE
		*			- 'debug'			для кода LEVEL_DEBUG
		*			- 'unknown'		для остального
		*/
		static private function _formatLevel($level) {
			$result = 'unknown';
			switch($level) {
				case self::LEVEL_FATAL: $result = 'fatal'; break;
				case self::LEVEL_ERROR: $result = 'error'; break;
				case self::LEVEL_NOTE: $result = 'note'; break;
				case self::LEVEL_DEBUG: $result = 'debug'; break;
			}
			return $result;
		}
		
		/* 
		*		Текстовое представление данных
		*		Входные параметры:
		*			$data		- mix,		данные для логирования
		*		Возвращает string:
		*			- 'true' или 'false'							для логического типа
		*			- json_encode($data)							для массивов
		*			- var_dump($data)									для объектов и классов
		*			- get_resource_type($data)				для объектов и классов
		*			- неявное преобразование в строку для остальных
		*/
		static private function _formatData($data) {
			$result = $data;
			if(is_bool($data)) {
				/* логика */
				$result = !empty($data) ? 'true' : 'false';
			}
			else if(is_array($data)) {
				/* массив */
				$result = json_encode($data);
			}
			else if(is_object($data)) {
				/* объект */
				ob_start();
				var_dump($data);
				$result = ob_get_clean();
			}
			else if(is_resource($data)) {
				/* ресурс */
				$result = get_resource_type($data);
			}
			return $result;
		}
		
		/* 
		*		Самологирование в файл
		*		Входные параметры:
		*			$level	- int, 		код уровня данных, см. константы LEVEL_*
		*			$tag 		- string,	метка для группировки логов
		*			$data		- mix,		данные для логирования
		*		Возвращает boolean:
		*			- true 				при успешной записи лога
		*			- false				при возниконовении ошибки записи лога 
		*/
		static private function _addSelf($level, $tag, $data) {
			$result = false;
			/* файл для самологов всегда рядом с классом */
			$path = realpath(dirname(__FILE__)) . '/logger.errors.txt';
			$file = fopen($path, 'a');
			if(!empty($file)) {
				$result = true;
				fwrite($file, date('Y.m.d H:i:s') . ': ' . self::_formatLevel($level) . ' - ' . $tag . "\r\n");
				fwrite($file, self::_formatData($data) . "\r\n");
				fwrite($file, "\r\n");
				fclose($file);
			}
			return $result;
		}
		
	}
	
	/* запускаем инициализацию */
	Logger::init();
	