<?php
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
		die();
	}
	
	abstract class KROrder {
		
		static $PERSON_TYPE_FIZ_ID = 1; /* физ. лицо */
		
		static $PROPERTY_SBERBANK_ID; /* ID в сбербанке */
		static $PROPERTY_SBERBANK_FORM; /* URL формы оплаты */
		
		static $_cache_time = 3600;
		static $_cache_id = 'kr_class_order';
		static $_cache_dir = '/kr_classes';
		
		static function init() {
			if(isset($_REQUEST['clear_cache'])) {
				self::clearCache();
			}
			
			$cache = new CPHPCache;
			if($cache->StartDataCache(self::$_cache_time, self::$_cache_id, self::$_cache_dir)) {
				if(CModule::IncludeModule('sale')){
					
					$res = CSaleOrderProps::GetList(array(), array('PERSON_TYPE_ID' => self::$PERSON_TYPE_FIZ_ID));
					while($lot = $res->Fetch()) {
						switch($lot['CODE']) {
							case 'SBERBANK_ID': self::$PROPERTY_SBERBANK_ID = $lot['ID']; break;
							case 'SBERBANK_FORM': self::$PROPERTY_SBERBANK_FORM = $lot['ID']; break;
						}
					}
				}
				
				$classData = array(
					'PROPERTY_SBERBANK_ID' => self::$PROPERTY_SBERBANK_ID,
					'PROPERTY_SBERBANK_FORM' => self::$PROPERTY_SBERBANK_FORM,
				);
				$cache->EndDataCache($classData);
			}
			else {
				$classData = $cache->GetVars();
				self::$PROPERTY_SBERBANK_ID = $classData['PROPERTY_SBERBANK_ID'];
				self::$PROPERTY_SBERBANK_FORM = $classData['PROPERTY_SBERBANK_FORM'];
			}
		}
		
		
		/* Проверка, пуста ли корзина */
		public static function isBasketEmpty()
		{
			$return = true;
			if(CModule::IncludeModule('sale')){
				$rs_Basket = CSaleBasket::GetList(
					array(), 
					array('FUSER_ID' => CSaleBasket::GetBasketUserID(), 'LID' => SITE_ID, 'ORDER_ID' => 'NULL'), 
					false, 
					false, 
					array("ID")
				);
				if($rs_Basket->SelectedRowsCount() > 0)
					$return = false;
			}
			return $return;
		}
		
		
		/* Проверка, верный ли ордер */
		public static function isOrderVerify($ORDER_ID, $HASH)
		{
			$result = false;
			if(CModule::IncludeModule('sale')){
				$order = \Bitrix\Sale\Order::load((int)($ORDER_ID));
				if($order && $order->getHash() == $HASH){
					$result = true;
				}
			}
			return $result;
		}
		
		
		
		/* выставление статуса заказа, оплачено */
		public static function setOrderPaid($ORDER_ID, $HASH)
		{
			$result = false;
			if(CModule::IncludeModule('sale') && self::isOrderVerify($ORDER_ID, $HASH)){
				if (CSaleOrder::PayOrder($ORDER_ID, "Y", True, True, 0, array())){
					if (CSaleOrder::StatusOrder($ORDER_ID, "F")){
						$result = true;
					}
				}
			}
			return $result;
		}
		
		
		/* заказ в сессию */
		public static function setOrder($ORDER_ID){
			$_SESSION['ORDER_ID'] = $ORDER_ID;
			return true;
		}
		public static function getOrder(){
			return (!empty($_SESSION['ORDER_ID'])) ? $_SESSION['ORDER_ID'] : false ;
		}
		public static function removeOrder(){
			unset($_SESSION['ORDER_ID']);
			return true;
		}
		
		
		static function clearCache() {
			$cache = new CPHPCache();
			$cache->CleanDir(self::$_cache_dir);
		}
	}
	
	KROrder::init();
	