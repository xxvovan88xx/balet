<?php
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
		die();
	}
	
	abstract class KRUser {
		static $GROUP_ALL = 2; /* Все пользователи */
		static $GROUP_ADMIN = 1; /* Администратор */
		static $GROUP_FREE_ACCESS = 7; /* Тестовый доступ */
		static $GROUP_SUBSCRIBE_USERS = 8; /* Подписанные пользователи */
		
		static $_cache_time = 3600;
		static $_cache_id = 'kr_class_user';
		static $_cache_dir = '/kr_classes';

		static function init() {
			if(isset($_REQUEST['clear_cache'])) {
				self::clearCache();
			}
			
			$cache = new CPHPCache;
			if($cache->StartDataCache(self::$_cache_time, self::$_cache_id, self::$_cache_dir)) {
				$res = CGroup::GetList( ($by='id'), ($order='asc'), false );
				while($lot = $res->Fetch()) {
					switch($lot['STRING_ID']) {
						case 'ALL': self::$GROUP_ALL = $lot['ID']; break;
						case 'ADMIN': self::$GROUP_ADMIN = $lot['ID']; break;
						case 'test_access': self::$GROUP_FREE_ACCESS = $lot['ID']; break;
						case 'subscribe_users': self::$GROUP_SUBSCRIBE_USERS = $lot['ID']; break;
						default: {
							if(preg_match('/^SAVE_DISCOUNT_/', $lot['STRING_ID'])) {
								self::$GROUP_SAVE_DISCOUNT[$lot['STRING_ID']] = $lot['ID'];
							}
						}
					}
				}
				$classData = array(
					'group_all' => self::$GROUP_ALL,
					'group_admin' => self::$GROUP_ADMIN,
					'GROUP_FREE_ACCESS' => self::$GROUP_FREE_ACCESS,
					'GROUP_SUBSCRIBE_USERS' => self::$GROUP_SUBSCRIBE_USERS,
				);
				$cache->EndDataCache($classData);
			}
			else {
				$classData = $cache->GetVars();
				self::$GROUP_ALL = $classData['group_all'];
				self::$GROUP_ADMIN = $classData['group_admin'];
				self::$GROUP_FREE_ACCESS = $classData['GROUP_FREE_ACCESS'];
				self::$GROUP_SUBSCRIBE_USERS = $classData['GROUP_SUBSCRIBE_USERS'];
			}
		}
	
		static function checkUserGroup($group_id) {
			global $USER;
			$arGroups = array();
			$rows = CUser::GetUserGroupList( $USER->GetID() );
			while ($row = $rows->Fetch()){
				$arGroups[] = $row['GROUP_ID'];
			}
			
			return (in_array($group_id, $arGroups)) ? true : false;
		}
		
		static function phoneFormat($string, $short = false, $plus = false) {
			/* пытаемся привести к формату +7 (999) 999-99-99 или 79999999999 (short) */
			$phone = preg_replace('/[^0-9]/', '', $string);
			if(!empty($phone)) {
				$phone = '7' . preg_replace('/^[7|8]/', '', $phone);
				if(preg_match('/^(7)([0-9]{3})([0-9]{3})([0-9]{2})([0-9]{2})$/', $phone, $match)) {
					if($short) {
						$string = $phone;
						if($plus) {
							$string = '+' . $string;
						}
					}
					else {
						$string = '+' . $match[1] . ' (' . $match[2] . ') ' . $match[3] . '-' . $match[4] . '-' . $match[5];
					}
				}
				
			}
			return $string;
		}

		
		static function clearCache() {
			$cache = new CPHPCache();
			$cache->CleanDir(self::$_cache_dir);
		}
	}
	
	KRUser::init();
	