<?php
	if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
		die();
	}
	
	abstract class KRIBlock {
		static $ID_REVIEWS;
		static $ID_WORKOUTS;
		static $ID_PLAYLISTS;
		static $ID_BLOGS;
		static $ID_COMMENTS;
		static $ID_CATALOG;
		static $ID_FAQ;
		
		static $_cache_time = 3600;
		static $_cache_id = 'kr_class_iblock';
		static $_cache_dir = '/kr_classes';

		static function init() {
			if(isset($_REQUEST['clear_cache'])) {
				self::clearCache();
			}

			$cache = new CPHPCache;
			if($cache->StartDataCache(self::$_cache_time, self::$_cache_id, self::$_cache_dir)) {
				if(CModule::IncludeModule('iblock')) {
					$res = CIBlock::GetList(array(), array('CODE' => array('reviews', 'workouts', 'playlists', 'blogs', 'comments', 'catalog', 'faq'), 'CHECK_PERMISSIONS' => 'N'));
					while($lot = $res->Fetch()) {
						switch($lot['CODE']) {
							case 'reviews': self::$ID_REVIEWS = $lot['ID']; break;
							case 'workouts': self::$ID_WORKOUTS = $lot['ID']; break;
							case 'playlists': self::$ID_PLAYLISTS = $lot['ID']; break;
							case 'blogs': self::$ID_BLOGS = $lot['ID']; break;
							case 'comments': self::$ID_COMMENTS = $lot['ID']; break;
							case 'catalog': self::$ID_CATALOG = $lot['ID']; break;
							case 'faq': self::$ID_FAQ = $lot['ID']; break;
						}
					}
				}
				$classData = array(
					'ID_REVIEWS' => self::$ID_REVIEWS,
					'ID_WORKOUTS' => self::$ID_WORKOUTS,
					'ID_PLAYLISTS' => self::$ID_PLAYLISTS,
					'ID_BLOGS' => self::$ID_BLOGS,
					'ID_COMMENTS' => self::$ID_COMMENTS,
					'ID_CATALOG' => self::$ID_CATALOG,
					'ID_FAQ' => self::$ID_FAQ,
				);
				$cache->EndDataCache($classData);
			}
			else {
				$classData = $cache->GetVars();
				self::$ID_REVIEWS = $classData['ID_REVIEWS'];
				self::$ID_WORKOUTS = $classData['ID_WORKOUTS'];
				self::$ID_PLAYLISTS = $classData['ID_PLAYLISTS'];
				self::$ID_BLOGS = $classData['ID_BLOGS'];
				self::$ID_COMMENTS = $classData['ID_COMMENTS'];
				self::$ID_CATALOG = $classData['ID_CATALOG'];
				self::$ID_FAQ = $classData['ID_FAQ'];
			}
			
		}
		
		static function clearCache() {
			$cache = new CPHPCache();
			$cache->CleanDir(self::$_cache_dir);
		}
	}
	
	KRIBlock::init();

	