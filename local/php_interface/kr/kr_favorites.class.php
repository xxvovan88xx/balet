<?

class KRFavorites
{
	
	//добавление/удаления в избранное, в случа удаления возвращается -1, добавление +1
	public static function set($id)
	{
		global $DB, $USER;
		
		if($id = (int)($_POST["id"]))
		{
			$is_add = true;
			
			//Пользователь авторизован, пишем в базу
			if($user_id = $USER->GetID())
			{
				
				$arFilter = array("ID" => $user_id);
				$arParams["SELECT"] = array("UF_FAVORITES");
				$arRes = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, $arParams);
				if ($arUser = $arRes->Fetch()) {
				
					$UF_FAVORITES = json_decode($arUser['UF_FAVORITES'], true);
					
					//Если товар уже есть в базе, то удаям его
					if($UF_FAVORITES){
						foreach($UF_FAVORITES as $key=>$val){
							if($val==$id){
								unset($UF_FAVORITES[$key]);
								$is_add = false;
							}
						}
					}
					
					//Добавляем товар 
					if($is_add){
						$UF_FAVORITES[] = $id;
					}
					
					$user = new CUser;
					$fields = Array( 
						"UF_FAVORITES" => json_encode($UF_FAVORITES, JSON_HEX_APOS | JSON_HEX_QUOT ), 
					); 
					$user->Update($user_id, $fields);
				}
			}
			//не авторизованный пользователь, пишем в сессию
			else
			{
				if(isset($_SESSION["FAVORITES"][$id])){
					unset($_SESSION["FAVORITES"][$id]);
                    $is_add = false;
				}
				else{
					$_SESSION["FAVORITES"][$id] = $id;
				}
			}
			
			if($is_add)
				return 1;
			else
				return -1;
		}
		
		return 0;
	}
	
	//Получение избранного пользователя 
	public static function get()
	{
		global $DB, $USER;
		$FAVORITES = array();
		
		//Пользователь авторизован, пишем в базу
		if($user_id = $USER->GetID())
		{
			$arFilter = array("ID" => $user_id);
			$arParams["SELECT"] = array("UF_FAVORITES");
			$arRes = CUser::GetList(($by = "NAME"), ($order = "desc"), $arFilter, $arParams);
			if ($arUser = $arRes->Fetch()) {
				$FAVORITES = json_decode($arUser['UF_FAVORITES'], true);
			}
		}			
		else if(isset($_SESSION["FAVORITES"])){
			$FAVORITES = $_SESSION["FAVORITES"];
		}

		return $FAVORITES;
	}
	
	//Проверка по ID, входит ли товар в избранное
	public static function isExist($id)
	{
		$FAVORITES = self::get();
		if(in_array($id, $FAVORITES))
			return true;
		
		return false;
	}
	
	//Получение ID товара из избранного
	public static function getID()
	{

        $arRes = array();
        $FAVORITES = self::get();

        if(isset($FAVORITES) && is_array($FAVORITES) && count($FAVORITES) > 0)
        {
            foreach($FAVORITES as $id=>$val)
                $arRes[] = $val;

        }
        return ($arRes) ? json_encode($arRes) : '[]' ;
	}

	
	//Количество элементов в избранном
	public static function getCount()
	{
		return count(self::get());
	}

	//Очистка избранного
    public static function clearAll(){

	    global $DB, $USER;

        if($user_id = $USER->GetID())
        {
            $UF_FAVORITES = array();
			
			$user = new CUser;
			$fields = Array( 
				"UF_FAVORITES" => json_encode($UF_FAVORITES, JSON_HEX_APOS | JSON_HEX_QUOT ), 
			); 
			$user->Update($user_id, $fields);
        }
    }
}