<?
//период помещения в тестовую группу
use Bitrix\Main\Config\Option;
Option::set("main", "free_access_period", "7");



include_once dirname(__FILE__) . '/kr/logger/logger.class.php';
include_once dirname(__FILE__) . '/kr/kr_catalog.class.php';
include_once dirname(__FILE__) . '/kr/kr_iblock.class.php';
include_once dirname(__FILE__) . '/kr/kr_order.class.php';
include_once dirname(__FILE__) . '/kr/kr_user.class.php';
include_once dirname(__FILE__) . '/kr/kr_payments.class.php';
include_once dirname(__FILE__) . '/kr/kr_boomstream.class.php';
include_once dirname(__FILE__) . '/wptt/sendpulse.class.php';

//Избранное
require_once(dirname(__FILE__)."/kr/kr_favorites.class.php");
//Функции для комментариев
require_once(dirname(__FILE__)."/func_comments.php");

//Оплата Яндекс Кассой
require dirname(__FILE__) .'/wptt/yandex-checkout-sdk-php-master/autoload.php'; 

//Склонение
function num2word($num, $words)
{
	$num = $num % 100;
	if ($num > 19) {
		$num = $num % 10;
	}
	switch ($num) {
		case 1: {
			return($words[0]);
		}
		case 2: case 3: case 4: {
			return($words[1]);
		}
		default: {
			return($words[2]);
		}
	}
}



//РЕГИСТРАЦИЯ ПОЛЬЗОВАТЕЛЯ
//Действия перед регистрацией пользователя
AddEventHandler("main", "OnBeforeUserRegister", "OnBeforeUserRegisterHandler");
function OnBeforeUserRegisterHandler(&$arFields)
{
	/*
	//логин такой же как email
	$arFields["LOGIN"] = $arFields["EMAIL"];
	
	//пароль
	$pass = randString(7);
	$arFields["PASSWORD"] = $pass;
	$arFields["CONFIRM_PASSWORD"] = $pass;
	*/
	
	//тестовая группа
	$arFields["GROUP_ID"] = array(KRUser::$GROUP_FREE_ACCESS);
}

//После регистрации пользователя, прописываем ему тестовый период, а также отсылаем письмо
//AddEventHandler("main", "OnAfterUserAdd", "OnAfterUserRegisterHandler"); 
AddEventHandler("main", "OnAfterUserRegister", "OnAfterUserRegisterHandler"); 
function OnAfterUserRegisterHandler(&$arFields) 
{
	if(!empty($arFields['USER_ID'])){
		$free_access_period = Option::get("main", "free_access_period");
		
		//Прописываем тестовый период для пользователя
		if($free_access_period && KRUser::$GROUP_FREE_ACCESS && $arFields['USER_ID'])
		{
			$arGroups_db = CUser::GetUserGroupList( $arFields['USER_ID'] );
			while ($arGroup = $arGroups_db->Fetch()){

				if ($arGroup['GROUP_ID'] == KRUser::$GROUP_FREE_ACCESS) {
					$arGroup['DATE_ACTIVE_TO'] = date("d.m.Y H:i:s", strtotime("+".$free_access_period."days"));
				}
				$arGroups[] = $arGroup;
			}
			CUser::SetUserGroup($arFields['USER_ID'], $arGroups);
		}
		
		//*** Копия кода находится также в kr/subscribe/component.php, просьба дублировать правки и туда
		//регистрациея пользователя в бумстрим, с демо доступом
		//получаем список медиа-файлов демо доступа, берем первый попавшийся и прописываем для него доступ пользователю, эта операция позволит пользователю просматривать все видео файлы из этого демо доступа
		if(defined("BOOMSTREAM_PASS_RULE_CODE_DEMO")){
			$KRBoomstream = new KRBoomstream(BOOMSTREAM_PASS_RULE_CODE_DEMO);
			if($listmedia = $KRBoomstream->listmedia())
			{
				//Добавить покупателя в платный доступ.
				$media = $listmedia[0]['Code'];
				$email = $arFields["EMAIL"];
				$access_expire = date("Y-m-d", strtotime("+{$free_access_period}days"));
				$access = $KRBoomstream->addbuyer($media, $email, $access_expire);
				
				//добавление пароля пользователю
				if(!empty($access['Hash'])){
					$user = new CUser;
					$fields = Array( 
						"UF_BOOMSTREAM_HASH_D" => $access['Hash'], 
					); 
					$user->Update($arFields['USER_ID'], $fields);
				}
			}
		}
		
		//шлем пиcьмо
		$arEventFields = array();
		$arEventFields['USER_ID'] 	= $arFields['USER_ID'];
		$arEventFields['EMAIL'] 	= $arFields['EMAIL'];
		$arEventFields['LOGIN'] 	= $arFields['LOGIN'];
		$arEventFields['PASSWORD'] 	= $arFields['PASSWORD'];
		$arEventFields['NAME'] 		= $arFields['NAME'];
		$arEventFields['USER_IP'] 	= $arFields['USER_IP'];
		$arEventFields['USER_HOST'] = $arFields['USER_HOST'];
		if (!CEvent::Send('REG_NEW_USER', SITE_ID, $arEventFields)){
			throw new Exception('Произошла ошибка во время отправки заказа. Повторите попытку позже.');
			
		}
	}
}

function debug($data, $die = false) {
	echo '<pre>';
	print_r($data);
	echo '</pre>';

	if($die) {
		die();
	}
}

function is_wptt_su() {
	return strpos($_SERVER['HTTP_HOST'], 'wptt.su') !== false;
}

function getBasket() {
	$arBasketItems = array();

	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC",
			"ID" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array('PRODUCT_ID', 'NAME', 'PRICE', 'CURRENCY', 'QUANTITY')
	);

	while ($arItems = $dbBasketItems->Fetch()) {
		if (strlen($arItems["CALLBACK_FUNC"]) > 0) {
			CSaleBasket::UpdatePrice(
				$arItems["ID"], 
				$arItems["CALLBACK_FUNC"], 
				$arItems["MODULE"], 
				$arItems["PRODUCT_ID"], 
				$arItems["QUANTITY"]
			);
			$arItems = CSaleBasket::GetByID($arItems["ID"]);
		}

		$arBasketItems[] = $arItems;
	}

	return $arBasketItems;
}

function setCoupon($coupon, $USER, $update_prices = false) {
	$res = CCatalogDiscountCoupon::SetCoupon($coupon);
	$arOrder = array();

	if($res !== false) {
		$dbBasketItems = CSaleBasket::GetList(array(), array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => "s1",
			"ORDER_ID" => "NULL"
		), false, false, array());

		while ($arItems = $dbBasketItems->Fetch()) {
			$arOrder["BASKET_ITEMS"][] = $arItems;
		}

		$arOrder['SITE_ID'] = SITE_ID;
		$arOrder['USER_ID'] = $USER->GetID();
		CSaleBasket::UpdateBasketPrices(CSaleBasket::GetBasketUserID(), SITE_ID);
		CSaleDiscount::DoProcessOrder($arOrder, array(), $arErrors);

		if($update_prices) {
			foreach($arOrder["BASKET_ITEMS"] as $product) {
				CSaleBasket::Update($product['ID'], array("PRICE" => $product['PRICE']));
			}
		}
	}

	return $arOrder;
}

?>