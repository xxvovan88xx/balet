<div class="footer__side">
    <p class="footer__text">2019 © Баббалет</p>
<br />
    <p class="footer__text"><a href="/upload/privacy.pdf" target="_blank">Политика конфиденциальности</a> | <a href="/upload/agreement.pdf" target="_blank">Пользовательское соглашение</a> | <a href="/upload/rules.pdf" target="_blank">Правила оказания услуг</a> | <a href="/upload/safety.pdf" target="_blank">Техника безопасности</a></p>
<br />
	<p class="footer__text">Мы принимаем к оплате:<br />
		<img src="/local/templates/main2019/assets/images/payment.png" alt="payment"></p>
<br />
    <p class="footer__text">Баббалет&nbsp;&mdash; зарегистрированный товарный знак. Незаконное использование товарного знака влечет за&nbsp;собой гражданскую, административную и&nbsp;уголовную ответственность (ст.&nbsp;1515 ГК&nbsp;РФ, ст.&nbsp;14.10. КоАП&nbsp;РФ, ст.&nbsp;180 УК&nbsp;РФ)</p>
</div>