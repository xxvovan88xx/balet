<div class="footer__top">
  <a href="" class="footer__logo"></a>
  <div class="footer__contacts">
	<a href="mailto:support@babballet.ru" class="footer__link">support@babballet.ru
</a>
	<div class="footer__social social">
	  <a href="https://teleg.run/Babballet_bot" target="_blank" class="social__item telegram">
	  </a>
	  <a href="https://instagram.com/nastya_barre" target="_blank" class="social__item">
		<svg xmlns="http://www.w3.org/2000/svg">
		  <use xlink:href="#ins"></use>
		</svg>
	  </a>
	</div>
  </div>
</div>