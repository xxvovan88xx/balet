<?
$arMenu = array();
if($arParams['MENU'] == 'training'){
	$arMenu = array(
		array(
			"NAME" => "Все тренировки",
			"URL" => "/personal/training/",
		),
		array(
			"NAME" => "Мои тренировки",
			"URL" => "/personal/my-training/",
		),
		array(
			"NAME" => "Мой плейлист",
			"URL" => "/personal/playlist/",
		),
	);
}
else if($arParams['MENU'] == 'personal'){
	$arMenu = array(
		array(
			"NAME" => "Профиль",
			"URL" => "/personal/profile/",
		),
		array(
			"NAME" => "История покупок",
			"URL" => "/personal/account-history/",
		),
	);
}

?>

<? if($arMenu){ ?>
<nav class="nav" role="navigation">
	<div class="nav__bar">
		<ul class="nav__list dragscroll" role="menu">
			<? foreach($arMenu as $menu){ ?>
			<li class="nav__item" role="menuitem">
				<a href="<?=$menu['URL']?>" class="nav__link <?if($arParams['CUR_PAGE']==$menu['URL']) print"active"?>"><?=$menu['NAME']?></a>
			</li>
			<?}?>
		</ul>
	</div>
</nav>
<? } ?>