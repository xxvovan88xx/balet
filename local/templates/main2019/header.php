<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>




<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <?$APPLICATION->ShowHead()?>
    <title><?$APPLICATION->ShowTitle()?></title>

    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/assets/css/main.css'); ?>
    <? $APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/assets/css/common.css'); ?>

    <?use Bitrix\Main\Page\Asset;?>
    <?Asset::getInstance()->addJs(SITE_TEMPLATE_PATH.'/assets/libs/jquery-3.2.1.min.js')?>

    <script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/assets/js/popups.js?deploy=1"></script>

</head>

<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>

<? global $USER;?>


<header class="header">
    <div class="header__inner">
        <a href="/" class="header__logo"></a>
        <div class="header__content">
            <?$APPLICATION->IncludeComponent(
                "bitrix:menu",
                "top",
                array(
                    "COMPONENT_TEMPLATE" => "top",
                    "ROOT_MENU_TYPE" => "top",
                    "MENU_CACHE_TYPE" => "N",
                    "MENU_CACHE_TIME" => "3600",
                    "MENU_CACHE_USE_GROUPS" => "Y",
                    "MENU_CACHE_GET_VARS" => array(
                    ),
                    "MAX_LEVEL" => "2",
                    "CHILD_MENU_TYPE" => "left",
                    "USE_EXT" => "N",
                    "DELAY" => "N",
                    "ALLOW_MULTI_SELECT" => "N"
                ),
                false
            );?>

            <div class="header__entities">
                <div class="header__entity">
                    <div class="entity">
                        <a href="/faq/" class="entity__button white">
                            <svg xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="#entity-1"></use>
                            </svg>
                        </a>
                        <a href="https://www.instagram.com/nastya_barre/" target="_blank" class="entity__button">
                            <svg xmlns="http://www.w3.org/2000/svg">
                            <use xlink:href="#ins"></use>
                            </svg>
                        </a>
                    </div>
                </div>
                <div class="header__entity">
                    <div class="entity entity--dropdown" data-header-dropdown>
                        <button type="button" class="entity__button entity__button_login" data-dropdown>
                            <svg class="login" xmlns="http://www.w3.org/2000/svg"><use xlink:href="#entity-2"></use></svg>
                            <svg class="logout" xmlns="http://www.w3.org/2000/svg"><use xlink:href="#exit"></use></svg>
                        </button>
                        <div class="header__dropdown">
                            <ul class="header__list">
                            <? if($USER->IsAuthorized()){?>
                                <li class="header__item">
                                    <a href="/personal/profile/" class="header__link">Личный кабинет</a>
                                </li>
                                <li class="header__item">
                                    <a href="/personal/account-history/" class="header__link">История покупок</a>
                                </li>
                                <li class="header__item">
                                    <a href="?logout=yes" class="header__link">Выход</a>
                                </li>
                            <?}else{?>
                                <li class="header__item">
                                    <div type="button" class="header__link js-sign_in-popup" data-new-popup="#modalEnter">Вход</div>
                                </li>
                                <li class="header__item">
                                    <div type="button" class="header__link js-registration-popup" data-new-popup="#modalReg">Регистрация</div>
                                </li>
                                <!--                <li class="header__item">-->
                                <!--                  <a href="#!" class="header__link">Личный кабинет</a>-->
                                <!--                </li>-->
                                <!--                <li class="header__item">-->
                                <!--                  <a href="#!" class="header__link">История покупок</a>-->
                                <!--                </li>-->
                                <!--                <li class="header__item">-->
                                <!--                  <a href="#!" class="header__link">Все тренировки</a>-->
                                <!--                </li>-->
                                <!--                <li class="header__item">-->
                                <!--                  <a href="#!" class="header__link">Мои тренировки</a>-->
                                <!--                </li>-->
                                <!--                <li class="header__item">-->
                                <!--                  <a href="#!" class="header__link">Мой плейлист</a>-->
                                <!--                </li>-->
                                <!--                <li class="header__item">-->
                                <!--                  <a href="#!" class="header__link">Выход</a>-->
                                <!--                </li>-->
                                <?}?>
                            </ul>
                        </div>
                    </div>

                    <div class="header-welcome">
                        <div class="header-welcome__title">Добро пожаловать в&nbsp;личный кабинет!</div>
                        <div class="header-welcome__info">Чтобы начать заниматься перейдите в раздел <a href="/personal/training/" class="header-welcome__emphasis">"Онлайн-тренировки"</a>. Со всеми возможностями сайта вы можете ознакомиться в <a href="/personal/training/" class="header-welcome__emphasis">обучающем видео</a>.</div>
                        <span class="close"><svg xmlns="http://www.w3.org/2000/svg"><use xlink:href="#close"></use></svg></span>
                    </div>
                </div>
            </div>
            <a href='<?php echo ($APPLICATION->GetCurPage() != '/') ? '/#trial_subscription' : '#trial_subscription'?>' class="header__button">Попробовать бесплатно</a>
            <div class="header__hamburger">
                <button type="button" class="hamburger" data-menu aria-label="Открыть меню">
                    <span class="hamburger__line"></span>
                    <span class="hamburger__line"></span>
                    <span class="hamburger__line"></span>
                </button>
            </div>

            <div class="header-welcome header-welcome_mobile">
                <div class="header-welcome__title">Добро пожаловать в&nbsp;личный кабинет!</div>
                <div class="header-welcome__info">Чтобы начать заниматься перейдите в раздел <a href="/personal/training/" class="header-welcome__emphasis">"Онлайн-тренировки"</a>. Со всеми возможностями сайта вы можете ознакомиться в <a href="/personal/training/" class="header-welcome__emphasis">обучающем видео</a>.</div>
                <span class="close"><svg xmlns="http://www.w3.org/2000/svg"><use xlink:href="#close"></use></svg></span>
            </div>
        </div>
    </div>
</header>

<div class="menu">
    <div class="menu__inner">
        <div class="menu__item">
            <div class="menu-roll">
                <button type="button" class="menu-roll__button">Личный кабинет</button>
                <div class="menu-roll__collapse">
                    <div class="menu-roll__content">
                        <ul class="menu-roll__list">
                        <? if($USER->IsAuthorized()){?>
                            <li class="menu-roll__item">
                                <a href="/personal/profile/" class="menu-roll__link">Личный кабинет</a>
                            </li>
                            <li class="menu-roll__item">
                                <a href="/personal/account-history/" class="menu-roll__link">История покупок</a>
                            </li>
                            <li class="menu-roll__item">
                                <a href="?logout=yes" class="menu-roll__link">Выход</a>
                            </li>
                        <?}else{?>
                            <li class="menu-roll__item">
                                <a href="#" class="menu-roll__link js-sign_in-popup" data-modal="#modalEnter">Вход</a>
                            </li>
                            <li class="menu-roll__item">
                                <a href="#" class="menu-roll__link js-registration-popup" data-modal="#modalReg">Регистрация</a>
                            </li>
                        <?}?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <?$APPLICATION->IncludeComponent("bitrix:menu", "top_mobile", Array(
            "COMPONENT_TEMPLATE" => "top",
            "ROOT_MENU_TYPE" => "top",  // Тип меню для первого уровня
            "MENU_CACHE_TYPE" => "Y",   // Тип кеширования
            "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
            "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
            "MENU_CACHE_GET_VARS" => "",    // Значимые переменные запроса
            "MAX_LEVEL" => "1", // Уровень вложенности меню
            "CHILD_MENU_TYPE" => "top", // Тип меню для остальных уровней
            "USE_EXT" => "N",   // Подключать файлы с именами вида .тип_меню.menu_ext.php
            "DELAY" => "N", // Откладывать выполнение шаблона меню
            "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
            ),
            false
        );?>



    </div>
</div>
