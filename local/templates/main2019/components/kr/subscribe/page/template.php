<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>


<main class="main" role="main">
  <div class="wrap">
    <div class="wrap__block wrap__block--gray">
      <div class="wrap__heading">
        <h1 class="wrap__title">Подписка на курс</h1>
      </div>

        <? if($arResult['ITEMS']):?>
            <?php //debug($arResult['ITEMS']); ?>
            <!-- <h3 class="wrap__subtitle">Выберите план подписки</h3> -->
            <div class="wrap__item container">

                <div class="plan">

                    <div class="plan__item">
                        <div class="card-plan">
                        <div class="card-plan__content">
                            <div class="card-plan__top">
                              <p class="card-plan__price">Бесплатно</p>
                              <p class="card-plan__month"></p>
                              <div class="content">
                                  <ul>
                                    <li>Доступ к 7 бесплатным тренировкам на 7 дней</li>
                                  </ul>
                              </div>
                            </div>
                            <a href="/#trial_subscription" class="button button__goal" type="button">
                                <span class="button__text">Оформить</span>
                            </a>
                        </div>
                        </div>
                    </div>

                    <?php $item_key = 0; ?>
                    <?php foreach($arResult['ITEMS'] as $item): ?>
                        <?php if($item["PRICE"] == 1 && $USER->GetID() !== '1') {
                          continue;
                        } ?>
                        <?$props = $item['PROPS'];?>
                        <div class="plan__item">
                            <div class="card-plan <?if($props["IS_PRIORITY"]["VALUE_XML_ID"]=="Y")print"card-plan--mark"?> "> 
                                    <div class="card-plan__content">
                                    <?php if($props["MONTH"]["VALUE"] == 1): ?>
                                     <div class="card-plan__top flex_start"> 
                                    <?php else:?>
                                      <div class="card-plan__top"> 
                                    <?php endif;?>
                                          <p class="card-plan__period"><?php echo $item['NAME']; ?><?//$props["MONTH"]["VALUE"] . num2word($props["MONTH"]["VALUE"], array("месяц", "месяца", "месяцев"))?></p>

                                          <?php if($props["MONTH"]["VALUE"] > 1): ?>
                                            <p class="card-plan__total"><?=number_format($item["PRICE"], 0, ',', ' ')?><span class="ruble_desktop"> ₽</span><span class="ruble_mobile"> P</span></p>
                                          <?elseif($props["MONTH"]["VALUE"] == 1):?>
                                            <p class="card-plan__total big_margin"> </p>
                                          <?php endif; ?>

                                          <?php if($props["BENEFIT"]["VALUE"] > 1):?>
                                            <p class="card-plan__benefit">— <?php echo $props["BENEFIT"]["VALUE"]?></p>
                                          <?php endif?>

                                          <p class="card-plan__price"><?php echo $props["MONTH"]["VALUE"] > 1 ? number_format(round($item["PRICE"]/$props["MONTH"]["VALUE"], 0), 0, ',', ' ') : number_format(round($item["PRICE"], 0), 0, ',', ' '); ?> <span class="ruble_desktop">₽</span><span class="ruble_mobile">P</span></p>

                                          <?php if($props["MONTH"]["VALUE"] >= 1): ?>
                                            <p class="card-plan__month">в месяц</p>
                                          <?php endif; ?>
                                        </div>
                                          <?php if ($USER->IsAuthorized()): ?>
                                              <form method="post" action="" >
                                                  <input type="hidden" name="METHOD" value="add_to_cart">
                                                  <input type="hidden" name="ID" value="<?=$item["ID"]?>">
                                                  <div class="content">

                                                  <?php if(isset($item['PROPS']['FEATURES']) && !empty($item['PROPS']['FEATURES']['VALUE'])): ?>
                                                    <ul>
                                                        <?php foreach($item['PROPS']['FEATURES']['VALUE'] as $feature): ?>
                                                          <li><?php echo $feature; ?></li>
                                                        <?php endforeach; ?>
                                                    </ul>
                                                  <?php endif; ?>

                                                  </div>
                                                  <button class="button button__goal" type="submit">
                                                  <span class="button__text">Оформить</span>
                                                  </button>
                                              </form>
                                          <?php else: ?>
                                              <div class="content">

                                                <?php if(isset($item['PROPS']['FEATURES']) && !empty($item['PROPS']['FEATURES']['VALUE'])): ?>
                                                  <ul>
                                                      <?php foreach($item['PROPS']['FEATURES']['VALUE'] as $feature): ?>
                                                        <li><?php echo $feature; ?></li>
                                                      <?php endforeach; ?>
                                                  </ul>
                                                <?php endif; ?>
                                                
                                              </div>
                                              <button type="button" class="button js-registration-popup button__goal" data-modal="#modalReg">
                                              <span class="button__text">Оформить</span>
                                              </button>
                                          <?php endif; ?>
                                      </div>
                            </div>
                        </div>

                        <?php if($item_key == 0): ?>
                            </div>
                            <div class="plan" id="plan">
                        <?php endif; ?>

                        <?php $item_key++; ?>

                    <?php endforeach; ?>

                </div>
            </div>
        <?php endif; ?>

      <div class="wrap__item container subscribtion">
      <div class="info__content">
      <!-- <div class="content">
        <p><b>Раз, два, три. Раз, два, три... С некоторыми моими участницами я занимаюсь в зале. Сейчас у меня 3
        группы.
        Я провожу с ними по 2 урока в неделю.</b></p>
        <p>Конечно, такие тренировки отличаются от занятий онлайн. Здесь нельзя нажать кнопку “пауза”, как на
        телефоне, и долго отдыхать. Здесь приходится выкладываться на полную — ведь у тебя всего 1,5 часа на
        урок.</p>
      </div> -->
      </div>
      <div class="info__aside subscribtion">
      <div class="info__preview">
        <div class="info__image"
        style="background-image: url(/local/templates/main2019/assets/images/instruction_preview.png);"
        data-new-modal="#Instruction" data-url="https://www.youtube.com/embed/kNTbXJ_XV4k?enablejsapi=1"></div>
      </div>
      <div class="content">
        <blockquote>
        <p>Регулярные тренировки&nbsp;&mdash; действенный способ достижения и&nbsp;поддержания баланса. Один из&nbsp;способов сохранить мотивацию&nbsp;&mdash; это прислушаться к&nbsp;своему телу, как оно чувствует себя после занятий.</p>
        <p>Систематические тренировки дают важный эффект домино: чем больше вы&nbsp;тренируетесь, тем больше ваше тело привыкает к&nbsp;нагрузкам, тем больше сил придают вам упражнения, ускоряя метаболизм.</p>
        </blockquote>
      </div>
      </div>
      <div class="info__content subscribtion">
      <div class="content">
        <h4>Что вы получите:</h4>
        <ul>
        <li>Полный доступ к&nbsp;эксклюзивным видео на&nbsp;все группы мышц</li>
        <li>Полноценные тренировки от&nbsp;15 до&nbsp;60&nbsp;минут</li>
        <li>Вы&nbsp;сами сможете сконструировать тренировку по&nbsp;интенсивности, времени и&nbsp;типу занятия (кардио, на&nbsp;коврике, у&nbsp;станка или микс)</li>
        <li>Собственный плейлист. Вы&nbsp;сможете сохранить любимые тренировки в&nbsp;отдельную папку</li>
        <li>Ежемесячное пополнение новых видеоуроков</li>
        <li>Доступ 24&nbsp;часа в&nbsp;сутки (Wi-Fi)</li>
        <li>Сайт адаптирован под любой смартфон</li>
        </ul>
      </div>
      </div>
      </div>
      
   
      
      <div class="wrap__item container">
    <div class="content">
      <p>Для оплаты (ввода реквизитов вашей карты) вы&nbsp;будете перенаправлены на&nbsp;платежный шлюз ПАО СБЕРБАНК. Соединение с&nbsp;платежным шлюзом и&nbsp;передача информации осуществляется в&nbsp;защищенном режиме с&nbsp;использованием протокола шифрования SSL. В&nbsp;случае, если ваш банк поддерживает технологию безопасного проведения интернет-платежей Verified By&nbsp;Visa, MasterCard SecureCode, MIR Accept, J-Secure, для проведения платежа также может потребоваться ввод специального пароля.</p>
      <p>Настоящий сайт поддерживает 256-битное шифрование. Конфиденциальность сообщаемой персональной информации обеспечивается ПАО СБЕРБАНК. Введенная информация не&nbsp;будет предоставлена третьим лицам за&nbsp;исключением случаев, предусмотренных законодательством&nbsp;РФ. Проведение платежей по&nbsp;банковским картам осуществляется в&nbsp;строгом соответствии с&nbsp;требованиями платежных систем МИР, Visa Int., MasterCard Europe Sprl, JCB.</p>
    </div>
      </div>

    </div>
  </div>
  <div class="scroll_bottom">
    <a href="#plan" class="intro__down" data-smooth="">
      <svg xmlns="http://www.w3.org/2000/svg">
        <use xlink:href="#arrow"></use>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg">
        <use xlink:href="#arrow"></use>
      </svg>
    </a>
  </div>
</main>

