<?php

$basket = getBasket();

if(!empty($basket)) {
	$arResult['ITEM'] = $basket[0];
} else {
	LocalRedirect("/subscription/");
	exit();
}
?>