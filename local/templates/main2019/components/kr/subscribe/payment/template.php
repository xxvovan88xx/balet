<main class="main" role="main">
	<div class="wrap">
		<div class="wrap__block wrap__block--gray">
			<div class="wrap__heading">
				<h1 class="wrap__title">Оформление подписки</h1>
			</div>

			<div class="wrap__item container">
				<div class="info">
					<div class="info__content">
						<div class="content">
							<h4>Категория подписки: <span class="color"><?php echo $arResult['ITEM']['NAME']; ?></span></h4>
							<h4>Стоимость: <?php echo number_format($arResult['ITEM']['PRICE'], 0, '.', ' '); ?> ₽</h4>
							<h4>Промокод</h4>

							<form class="form_coupon">
					        	<input type="text" name="coupon" placeholder="Промокод">
								<input type="submit" value="Применить">
							</form>

							<div class="form_coupon__response">Промокод применен</div>

							<br>

							<h4>Итого: <span class="color"><span class="price_target"><?php echo number_format($arResult['ITEM']['PRICE'], 0, '.', ' '); ?></span> ₽</span></h4>

							<br/>

							<form method="post" action="/subscription/" id="payment_form">
								<input type="hidden" name="METHOD" value="order_paylink">
								<input type="hidden" name="coupon" class="payment_form_coupon" value="">
								<button class="button" type="submit">
									<span class="button__text">Оплатить</span>
								</button>
							</form>
							<script type="text/javascript">
								// $('document').ready(function() {
								// 	setTimeout(function() {
								// 		$("#payment_form button").trigger("click");
								// 	}, 3000);
								// });
							</script>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>


<script>
	$('.form_coupon').submit(function() {
		let data = $(this).serialize(),
			coupon = $('.form_coupon input[type="text"]').val();

		$.ajax({
			url: '/ajax/coupon.php',
			data: {
				coupon: coupon
			},
			type: "POST",
			dataType: 'json',
			success: function(result) {
				console.log(result);
				if(result.status) {
					$('.payment_form_coupon').val(coupon);
					$('.price_target').text(result.text.ORDER_PRICE);
					$('.form_coupon__response').removeClass('error').text('Промокод применен');
				} else {
					$('.form_coupon__response').addClass('error').text('Данный промокод не действителен');
				}
				$('.form_coupon__response').fadeIn();
			}
		});

		return false;
	});
</script>