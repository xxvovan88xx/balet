
<div class="wrap__item container">
    <div class="info">
      <div class="info__content">
        <div class="content">
			<p>Вы будете автоматически переадресованы на страницу оплаты Сбербанка. Если этого не происходит, нажмите кнопку «Оплатить» ниже.</p>

			<br/>

			<form method="post" action="/subscription/" id="payment_form">
				<input type="hidden" name="METHOD" value="order_paylink">
				<button class="button" type="submit">
					<span class="button__text">Оплатить</span>
				</button>
			</form>
			<script type="text/javascript">
				$('document').ready(function() {
					setTimeout(function() {
						$("#payment_form button").trigger("click");
					}, 3000);
				});
			</script>
		</div>
	</div>
	</div>
</div>