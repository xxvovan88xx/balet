<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>







<div class="wrap__block" id="trial_subscription">
    <div class="wrap__heading">
        <h1 class="wrap__title">Потренируйтесь бесплатно</h1>
        <p class="wrap__text js-subscribe-text_reg">Регулярные тренировки дают результаты и повышают самодисциплину. Хотите достичь большего? Введите свои данные, и&nbsp;я&nbsp;открою вам бесплатный доступ к&nbsp;7&nbsp;полноценным тренировкам на&nbsp;1&nbsp;неделю.</p>
        <p class="wrap__text js-subscribe-text_success hidden">Вы успешно зарегистрировались!</p>
    </div>
    <div class="wrap__item container">
        <form class="form js-subscribe-form" method="post">
            <input type="hidden" name="METHOD" value="subscribe" />
            <div class="form__row">
                <div class="form__item">
                    <input type="text" class="input" placeholder="Имя" name="NAME" required>
                </div>
                <div class="form__item">
                    <input type="email" class="input" placeholder="E-mail" name="EMAIL" required>
                </div>
            </div>
            <div class="form__row">
                <div class="form__item">
                    <input type="text" class="input" placeholder="Номер телефона" name="PHONE_NUMBER" required>
                </div>
                <div class="form__item">
                    <input type="text" class="input form_code" placeholder="Код из СМС" style="display:none" required>
                    <button type="button" class="button subscribe_send_sms">
                        <span class="button__text">Отправить смс-код</span>
                    </button>
                </div>
            </div>
            <div class="form__row">
                <div class="form__item">
                    <div class="checkbox">
                        <label class="checkbox__label">
                            <input type="checkbox" class="checkbox__input" required>
                            <span class="checkbox__mark"></span>
							<span class="checkbox__text">Я соглашаюсь с <a href="/upload/privacy.pdf" target="_blank">политикой конфидециальности</a>, <a href="/upload/agreement.pdf" target="_blank">пользовательским соглашением</a> и <a href="/upload/rules.pdf" target="_blank">правилами оказания информационных услуг</a></span>
                        </label>
                    </div>
                </div>
                <div class="form__item">
                    <div class="form_sms__error" style="display:none">Код из смс не подходит. Проверьте данные и попробуйте еще раз.</div>
                    <div class="form_sms__request_info" style="display:none">Запрос кода повторно через: 0:<span></span></div>
                    <button type="submit" class="button" id="subscribe_send" style="display:none">
                        <span class="button__text">Подтвердить</span>
                    </button>
                </div>
            </div>
            <div class="errors hidden js-subscribe-errors">
                Пользователь с таким E-mail уже существует! 
            </div>
        </form>
    </div>
</div>


<script>
let time = 0,
    interval_timer;


function start_timer() {
    time = 180;
    $('.form_sms__request_info').fadeIn();
    interval_timer = setInterval(function(){
        if(time != 0) {
            time--;
            $('.form_sms__request_info span').text((time < 10 ? '0' : '') + time);
        } else {
            clearInterval(interval_timer);
            $('.form_code').hide();
            $('#subscribe_send').hide();
            $('.subscribe_send_sms').fadeIn();
            $('.form_sms__request_info').fadeOut();
        }
    }, 1000);
}


function retry() {
    $('.form_code').hide();
    $('.subscribe_send_sms').fadeIn();
}


$('.subscribe_send_sms').click(function() {
    let phone_val = $('input[name="PHONE_NUMBER"]').val();

    if(phone_val != '' && time == 0) {
        $.ajax({
            url: '/ajax/subscribe_free.php',
            type: 'POST',
            data: {
                phone: phone_val,
            },
            dataType: 'json',
            success: function(result) {
                console.log(result);
                if(result.status == 'success') {
                    window['code'] = result.code;

                    console.log(code);

                    start_timer();
                    $('.subscribe_send_sms').hide();
                    $('.form_code').fadeIn();
                    $('#subscribe_send').fadeIn();
                }
            }
        });
    }
});
	

$(".js-subscribe-form").submit(function(){
    var type = $(this).attr("method"),
        url = $(this).attr("action"),
        formData = new FormData($(this)[0]),
        code_user = $('.form_code').val();

    if(code_user == code) {
        $.ajax({
            url: url,
            data: formData,
            dataType: "json",
            type: type,
            contentType: false,
            processData: false,
            success: function (data){
                if(data.status !== undefined && data.status == 'ok'){
                    window.location.href = '/personal/training/?register=yes';
                }else if(data.status !== undefined && data.status == 'error'){
                    $(".js-subscribe-errors").removeClass("hidden");
                }
            }
        });

    } else {
        $('.form_sms__error').fadeIn();
    }

    return false;
});
</script>