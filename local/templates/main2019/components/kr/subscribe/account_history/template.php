<div class="wrap__block wrap__block--gray" data-space="top-small">
	<div class="wrap__item container">
		<div class="wrap__item-heading">
			<h3 class="wrap__title">История покупок</h3>
		</div>
		
		<?if($arResult['ITEMS']){?>
		<div class="data-history">

			<div class="data-history__row data-history__row--heading">
				<div class="data-history__cell">
					Номер заказа
				</div>
				<div class="data-history__cell">
					Дата
				</div>
				<div class="data-history__cell">
					Тариф
				</div>
				<div class="data-history__cell">
					Стоимость
				</div>
			</div>

			<div class="data-history__body">
			<?php //debug($arResult['ITEMS']); ?>
				<?foreach($arResult['ITEMS'] as $item){?>
				<div class="data-history__row">
					<div class="data-history__cell" data-name="Номер заказа">
						№<?=str_pad($item["ID"], 5, "0", STR_PAD_LEFT);?>
					</div>
					<div class="data-history__cell" data-name="Дата">
						<?=date("d.m.Y", strtotime($item["DATE_INSERT"]))?>
					</div>
					<div class="data-history__cell" data-name="Тариф">
						<?=$item['NAME'];?>
					</div>
					<div class="data-history__cell" data-name="Стоимость">
						<?=number_format($item["PRICE"], 0, ',', ' ')?> ₽
					</div>
				</div>
				<?}?>
			</div>
		</div>
		<?}?>
	</div>
</div>