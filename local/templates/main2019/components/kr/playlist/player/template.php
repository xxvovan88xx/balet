<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$allowed_days = 5;
if(array_key_exists(KRUser::$GROUP_SUBSCRIBE_USERS, $arResult['USER_GROUPS']) && $arResult['USER_GROUPS'][KRUser::$GROUP_SUBSCRIBE_USERS]['DATE_ACTIVE_FROM'] !== null) {
  $dStart = new DateTime($arResult['USER_GROUPS'][KRUser::$GROUP_SUBSCRIBE_USERS]['DATE_ACTIVE_FROM']);
  $dEnd  = new DateTime($arResult['USER_GROUPS'][KRUser::$GROUP_SUBSCRIBE_USERS]['DATE_ACTIVE_TO']);
  $dDiff = $dStart->diff($dEnd);
  if($dDiff->format('%r%a') < 8) {
    $allowed_days = 7;
  }
}
?>


<div class="wrap__block wrap__block--gray <?php echo $allowed_days == 7 ? 'disabled' : ''; ?>" data-space="top-extra-small">
	<div class="container">
		<a href="/personal/playlist/" class="wrap__go-back">Назад</a>
	</div>
	<div class="wrap__item container">
		<div class="playlist-wrapper">
			<? if($arResult['PLAYLISTS']){?>
			
			<div class="playlist-wrapper__player">
				<div class="playlist-video">
					<div class="playlist-video__frame js-playlist-video-container">
						
					</div>
					<h4 class="playlist-video__title"><?=$arResult['PLAYLISTS'][0]["NAME"]?></h4>
				</div>
			</div>
			
			<div class="playlist-wrapper__aside">
				<div class="playlist-panel">
					<h4 class="playlist-panel__title">Избранное</h4>
					
					<div class="playlist-panel__controls">
						<button type="button" class="playlist-panel__button">
							<svg xmlns="http://www.w3.org/2000/svg">
							<use xlink:href="#prev-play"></use>
							</svg>
						</button>
						<button type="button" class="playlist-panel__button">
							<svg xmlns="http://www.w3.org/2000/svg">
							<use xlink:href="#next-play"></use>
							</svg>
						</button>
					</div>

					<div class="playlist-panel__inner">
					<?$is_active = true;?>
					<?foreach($arResult['PLAYLISTS'][0]['PROPS']['VIDEO']['VALUE'] as $key=>$video_id){?>
						<?$video = $arResult['VIDEOS'][$video_id]?>
						
						<?
						//Если видео не доступно пользователю, то добавляем класс unavailable
						$is_unavailable = true;
						if(
							! $video['PROPS']['SUBSCRIBE_ONLY']['VALUE_XML_ID'] ||
							in_array("A", $video['PROPS']['SUBSCRIBE_ONLY']['VALUE_XML_ID']) ||
							(
								in_array("Y", $video['PROPS']['SUBSCRIBE_ONLY']['VALUE_XML_ID']) &&
								$arParams['IS_SUBSCRIBE'] &&
								$arParams['BOOMSTREAM_HASH']
							)
						){
							$is_unavailable = false;
						}
						?>
						
						<?
						//Ролик с BOOMSTREAM
						$video_src = false;
						if( ! $is_unavailable)
						{
							$mediaCode = $video['PROPS']['CODE']['VALUE'];
							//доспуп для подписчикаов
							if($arParams['BOOMSTREAM_HASH'] && in_array("Y", $video['PROPS']['SUBSCRIBE_ONLY']['VALUE_XML_ID'])){
								$hash = $arParams['BOOMSTREAM_HASH'];
								$video_src = "https://play.boomstream.com/{$mediaCode}?autostart=1&id_recovery={$hash}";
							}
							//демо доступ
							elseif($arParams['BOOMSTREAM_HASH_D']){
								$hash = $arParams['BOOMSTREAM_HASH_D'];
								$video_src = "https://play.boomstream.com/{$mediaCode}?autostart=1&id_recovery={$hash}";
							}
							else{
								$video_src = "https://play.boomstream.com/{$mediaCode}?autostart=1";
							}
						}
						/*
						if(defined("BOOMSTREAM_API_KEY") && ! $is_unavailable){
							$mediaCode = $video['PROPS']['CODE']['VALUE'];
							$apiKey = BOOMSTREAM_API_KEY;
							$time = time();
							$hash = md5($apiKey . '|' . $time . '|' . $mediaCode);
							$playerCode = '<iframe class="hidden js-playlist-video-content" data-id="'.$video['ID'].'" width="640" height="356" src="https://play.boomstream.com/player.html?autostart=1&code='
							 . $mediaCode . '&hash=' . $hash
							 . '&time=' . $time
							 . '" frameborder="0" scrolling="no"></iframe>';
							//print $playerCode;
							$video_src = 'https://play.boomstream.com/player.html?autostart=1&code='.$mediaCode.'&hash='.$hash.'&time='.$time;
							//$video_src = 'https://play.boomstream.com/player.html?autostart=1&code='.$mediaCode.'&time=0&autostart=1';
						}
						*/
						?>
						
						<div class="playlist-panel__item js-playlist-video-player js-playlist-autoplay <?=($is_unavailable ? "unavailable" : "")?> <?=($is_active && ! $is_unavailable ? "active" : "")?>" data-id="<?=$video['ID']?>"  data-video_src="<?=($video_src ? $video_src : "")?>" >
							<div class="playlist-panel__thumb"
							style="background-image: url('<?=CFile::GetPath($video['PREVIEW_PICTURE']);?>');"></div>
							<div class="playlist-panel__info">
								<p class="playlist-panel__name"><?=$video["~PREVIEW_TEXT"]?></p>
								<span class="playlist-panel__duration"><?=$video['PROPS']['DURATION']['VALUE']?></span>
								<?if($is_unavailable){?><span style="color:red">недоступно, только по подписке</span><?}?>
							</div>
						</div>
						<?if($is_active && ! $is_unavailable){$is_active = false;}?>
					<?}?>
					</div>
				</div>
			</div>
			<?}?>
		</div>
	</div>
</div>