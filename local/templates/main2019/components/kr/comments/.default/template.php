<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
global $USER;

//print_r($arResult);die;

validateCommentName('Alex F');
validateCommentName('Alex "F');
validateCommentName('Алекс F');
validateCommentName('Алекс Кок');
validateCommentName('@Алекс Кок');
?>


<div class="comments">
	<div class="comments__block" id="comment_block">
		<p class="comments__heading">Комментарии</p>
		
		<? if($arResult['ITEMS']){?>
			<? foreach($arResult['ITEMS'] as $key=>$item){?>
			<? $comment = strip_tags($item['~PREVIEW_TEXT']);?>
			<? $comment = str_replace("[b]", "<strong>", $comment);?>
			<? $comment = str_replace("[/b]", "</strong>", $comment);?>
			
			<div class="comments__item js-comments-container">
				<div class="comment" id="comment<?=($key+1)?>">
					<p class="comment__name js-comments-name"><?=$item['NAME']?></p>
					<?if($item['PROPERTY_AUTHOR_VALUE']){?>
						<span class="comment__state">Автор</span>
					<?}?>
					<p class="comment__text js-comments-comment"><?=$comment?></p>
					<time class="comment__date" datetime="<? echo ConvertDateTime($item["DATE_CREATE"], "DD.MM.YYYY", "ru");?>"><? echo ConvertDateTime($item["DATE_CREATE"], "DD/MM/YYYY", "ru");?></time>
					<? if ($USER->IsAuthorized()){?>	
						<a href="#comment_form_container" class="comment__link js-comments-answer" data-id="<?=$item['ID']?>">Ответить</a>
					<?}?>
				</div>
			</div>
			<?}?>
		<?}?>
		
	</div>

	<? if ($USER->IsAuthorized()){?>
	<div class="comments__block" id="comment_form_container">
		<p class="comments__heading js-comments-title-new">Оставить комментарий</p>
		<p class="comments__heading js-comments-title-answer hidden">Ответить на комментарий</p>
		
		<?if($arParams["validate_error"]){?>
		<div class="error">
			<?foreach($arParams["validate_error"] as $error){?>
				<?if($error == "name"){?>
				Имя должно содержать буквы кириллицы или латиницы и цифры
				<?}?>
			<?}?>
		</div>
		<br>
		<?}?>
		
		<form class="form" id="commentForm" action="#comment_form_container" method="post">
			<input type="hidden" name="method" value="comment_add">
			<input type="hidden" name="parent_id" value="0">
			<div class="form__item">
				<label for="commentName" class="form__label">Имя*</label>
				<input id="commentName" type="text" class="input" placeholder="Имя" name="name" value="<?=!empty($arParams['form']["name"])?$arParams['form']["name"]:$USER->GetFirstName()?>" required>
			</div>
			<div class="form__item">
				<label for="commentComment" class="form__label">Комментарий*</label>
				<textarea id="commentComment" class="textarea" placeholder="Комментарий" name="comment"><?=!empty($arParams['form']["comment"])?$arParams['form']["comment"]:''?></textarea>
			</div>
			<div class="form__buttons">
				<button type="submit" class="button">
					<span class="button__text">Отправить</span>
				</button>
			</div>
		</form>
	</div>
	<?}?>
</div>