<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
//print_r($arResult);die;
?>

<?if(!empty($arResult)){ ?>

	<?foreach($arResult as $item){?>
		<?if($item['PARAMS']['TYPE'] == 'dropdown'){?>
		<div class="menu__item">
			<div class="menu-roll">
				<button type="button" class="menu-roll__button <?if($item['SELECTED']){print "active";}?>"><?=$item['TEXT']?></button>
				<?if(!empty($item['ADDITIONAL_LINKS'])){?>
				<div class="menu-roll__collapse">
					<div class="menu-roll__content">
						<ul class="menu-roll__list">
						<?foreach($item['ADDITIONAL_LINKS'] as $subitem){?>
							<li class="menu-roll__item">
								<a href="<?=$subitem['LINK']?>" class="menu-roll__link"><?=$subitem['TEXT']?></a>
							</li>
						<?}?>
						</ul>
					</div>
				</div>
				<?}?>
			</div>
		</div>
		<?}else{?>
		<div class="menu__item">
			<a href="<?=$item['LINK']?>" class="menu__link <?if($item['SELECTED']){print "active";}?>"><?=$item['TEXT']?></a>
		</div>


		<?}?>
	<?}?>
<?}?>
