<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
// debug($arResult);
?>

<?if(!empty($arResult)){ ?>
<nav class="header__nav" role="navigation">
	<ul class="header__list" role="menu">
	<?foreach($arResult as $item){?>
		<?if($item['PARAMS']['TYPE'] == 'dropdown'){?>
		<li class="header__item header__item--dropdown" role="menuitem" data-header-dropdown>
			<button type="button" class="header__link <?if($item['SELECTED']){print "active";}?>" data-dropdown><?=$item['TEXT']?>
			<svg xmlns="http://www.w3.org/2000/svg">
			<use xlink:href="#arrow"></use>
			</svg>
			</button>
			<?if(!empty($item['ADDITIONAL_LINKS'])){?>
			<div class="header__dropdown">
			<ul class="header__list">
				<?foreach($item['ADDITIONAL_LINKS'] as $subitem){?>
				<li class="header__item">
					<a href="<?=$subitem['LINK']?>" class="header__link <?//active?>"><?=$subitem['TEXT']?></a>
				</li>
				<?}?>
			</ul>
			</div>
			<?}?>
		</li>
		<?}else{?>
		<li class="header__item" role="menuitem">
			<a href="<?=$item['LINK']?>" class="header__link <?if($item['SELECTED']){print "active";}?>"><?=$item['TEXT']?></a>
		</li>
		<?}?>
	<?}?>
	</ul>
</nav>
<?}?>
