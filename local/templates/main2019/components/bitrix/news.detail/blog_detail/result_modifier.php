<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}


//Добавление просмотра в статью
$view_count = (int)($arResult['PROPERTIES']['VIEWS']['VALUE']) + 1;
$arResult['PROPERTIES']['VIEWS']['VALUE'] = $view_count;
CIBlockElement::SetPropertyValuesEx($arResult['ID'], KRIBlock::$ID_BLOGS, array("VIEWS" => $view_count));

//print_r($arResult);die;