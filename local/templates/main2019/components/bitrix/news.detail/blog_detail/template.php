<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//print_r($arResult);die;
?>


<div class="wrap__block wrap__block--gray" data-space="bottom-small">
	<div class="wrap__item container-frame">
		<div class="content">
			<time datetime="<? echo ConvertDateTime($arResult["DATE_CREATE"], "DD.MM.YYYY", "ru");?>"><? echo ConvertDateTime($arResult["DATE_CREATE"], "DD/MM/YYYY", "ru");?></time>
			<h1><?echo $arResult["NAME"]?></h1>
			
			<div class="content__stat">
				<div class="stat">
					<span class="stat__item">
						<svg xmlns="http://www.w3.org/2000/svg">
						<use xlink:href="#eye"></use>
						</svg>
						<?=$arResult['PROPERTIES']['VIEWS']['VALUE']?>
					</span>
					<span class="stat__item">
						<svg xmlns="http://www.w3.org/2000/svg">
						<use xlink:href="#comment"></use>
						</svg>
						<?=getCountComments($user_id = false, $arResult["ID"]);?>
					</span>
				</div>
			</div>
			<figure>
			<img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="">
			</figure>
			<p><?echo $arResult["DETAIL_TEXT"]?></p>
			
		</div>
	</div>
</div>

<div class="wrap__block" data-space="top-small">
	<div class="wrap__item container-frame">
<?
$APPLICATION->IncludeComponent(
	'kr:comments',
	'.default',
	array(
		"BLOG_ID" => $arResult["ID"]
	),
	$component
);
?>
	</div>
</div>
