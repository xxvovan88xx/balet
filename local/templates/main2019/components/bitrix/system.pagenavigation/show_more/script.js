$(document).ready(function(){
	

    $(document).on('click', '.js-load_more', function(){

        var targetContainer_news = $('.js-news-list'),	//  Контейнер, в котором хранятся элементы
			targetContainer_button = $('.js-load_more-container'),	//  Контейнер, в котором хранятся элементы
			__this = $(this),
            url =  __this.attr('data-url');	//  URL, из которого будем брать элементы
			

        if (url !== undefined) {
            $.ajax({
                type: 'GET',
                url: url,
                dataType: 'html',
				cache: false,
                success: function(data){

                    //  Удаляем старую навигацию
                    targetContainer_button.html('');

                    var elements = $(data).find('.js-news-item'),  //  Ищем элементы
                        pagination = $(data).find('.js-load_more');//  Ищем навигацию

                    targetContainer_news.append(elements);   //  Добавляем посты в конец контейнера
                    targetContainer_button.append(pagination); //  добавляем навигацию следом

                }
            })
        }

    });

});