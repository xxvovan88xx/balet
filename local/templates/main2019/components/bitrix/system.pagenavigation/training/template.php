<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");

//var_dump($arResult);
?>


<?if ($arResult["NavPageCount"] > 1){?>
<nav class="pagination" role="navigation" aria-label="pagination">
	<ul class="pagination__list">
	
		<li class="pagination__item pagination__item--prev <?if($arResult["NavPageNomer"]<=1){?>disabled<?}?>">
			<a href="<?=(($arResult["NavPageNomer"]>1)?$arResult["sUrlPath"].$strNavQueryStringFull:"#")?>" class="pagination__link">
				<svg viewBox="0 0 50 12" xmlns="http://www.w3.org/2000/svg">
				<path d="M12 4.99986L50 4.99987V6.99987L12 6.99986L12 4.99986Z"/>
				<path d="M0 5.99986L12 0.803711L12 11.196L0 5.99986Z"/>
				</svg>
			</a>
		</li>
		
		<?/*
		<li class="pagination__item <?if($arResult["NavPageNomer"]==1){?>active<?}?>">
			<a href="<?=($arResult["sUrlPath"].$strNavQueryStringFull)?>" class="pagination__link ">1</a>
		</li>
		*/?>
		
		<?//$arResult["nStartPage"]++?>
		
		<?while($arResult["nStartPage"] <= $arResult["nEndPage"]):?>
			
			<li class="pagination__item <?if($arResult["NavPageNomer"]==$arResult["nStartPage"]){?>active<?}?>">
				<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>" class="pagination__link"><?=$arResult["nStartPage"]?></a>
			</li>
			
			<?$arResult["nStartPage"]++?>
			
		<?endwhile?>
		
		
		<?/*
		<li class="pagination__item">
			<a href="#!" class="pagination__link">...</a>
		</li>
		
		<li class="pagination__item <?if($arResult["NavPageNomer"]==$arResult["nEndPage"]){?>active<?}?>">
			<a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>" class="pagination__link "><?=$arResult["nEndPage"]?></a>
		</li>
		*/?>
		
		<li class="pagination__item pagination__item--next <?if($arResult["NavPageNomer"] >= $arResult["NavPageCount"]){?>disabled<?}?>"">
			<a href="<?=(($arResult["NavPageNomer"] < $arResult["NavPageCount"])?$arResult["sUrlPath"]."?".$strNavQueryString."PAGEN_".$arResult["NavNum"]."=".($arResult["NavPageNomer"]+1):"#")?>" class="pagination__link">
				<svg viewBox="0 0 50 12" xmlns="http://www.w3.org/2000/svg">
				<path d="M38 6.99985L0 6.99984V4.99984L38 4.99985V6.99985Z"/>
				<path d="M50 5.99985L38 11.196V0.803711L50 5.99985Z"/>
				</svg>
			</a>
		</li>
	</ul>
</nav>
<?}?>

