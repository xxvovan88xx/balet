<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["PHONE_REGISTRATION"])
{
	CJSCore::Init('phone_auth');
}
?>

<main class="main" role="main">
	<div class="wrap">
		<div class="wrap__block wrap__block--gray">
			<div class="wrap__heading">
				<h2 class="wrap__title"><?=GetMessage("AUTH_CHANGE_PASSWORD")?></h2>
			</div>
			<div class="wrap__item container">
			
				<form class="form" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_FORM"]?>">
				
					<?ShowMessage($arParams["~AUTH_RESULT"]);?>
				
					<?if($arResult["SHOW_FORM"]){?>
				
						<?if (strlen($arResult["BACKURL"]) > 0): ?>
						<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
						<? endif ?>
						<input type="hidden" name="AUTH_FORM" value="Y">
						<input type="hidden" name="TYPE" value="CHANGE_PWD">
						
					
						
						<div class="form__item">
							<label for="fogName" class="form__label"><?=GetMessage("AUTH_LOGIN")?></label>
							<input id="fogName" type="text" class="input" placeholder="<?=GetMessage("AUTH_LOGIN")?>" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>" />
						</div>
						
						<div class="form__item">
							<label for="fogEMAIL" class="form__label"><?=GetMessage("AUTH_CHECKWORD")?></label>
							<input id="fogEMAIL" type="text" class="input" placeholder="<?=GetMessage("AUTH_CHECKWORD")?>" name="USER_CHECKWORD" value="<?=$arResult["USER_CHECKWORD"]?>" autocomplete="off" />
						</div>
						
						<div class="form__item">
							<label for="fogName" class="form__label">Новый пароль(не менее 6 символов)</label>
							<input id="fogName" type="password" class="input" placeholder="Новый пароль(не менее 6 символов)" name="USER_PASSWORD" value="<?=$arResult["USER_PASSWORD"]?>" autocomplete="off" />
						</div>
						
						<div class="form__item">
							<label for="fogName" class="form__label"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?></label>
							<input id="fogName" type="password" class="input" placeholder="<?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?>" name="USER_CONFIRM_PASSWORD" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" autocomplete="off" />
						</div>
						
						<div class="form__buttons">
							<input type="hidden" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>">
							<button type="submit" class="button">
								<span class="button__text">Восстановить</span>
							</button>
						</div>
						
					<?}?>
				</form>

			</div>
		</div>
	</div>
</main>
