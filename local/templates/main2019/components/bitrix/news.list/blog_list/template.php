<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//	print_r($arResult);die;
?>

<div class="wrap__item container">
	<div class="blog js-news-list">
	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<div class="blog__item js-news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>" class="card-blog">
				<div class="card-blog__background">
					<div class="card-blog__image" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');"></div>
				</div>
				<div class="card-blog__wrap">
					<div class="card-blog__info">
						<time class="card-blog__date" datetime="<? echo ConvertDateTime($arItem["DATE_CREATE"], "DD.MM.YYYY", "ru");?>"><? echo ConvertDateTime($arItem["DATE_CREATE"], "DD/MM/YYYY", "ru");?></time>
						<p class="card-blog__title"><?echo $arItem["NAME"]?></p>
						<p class="card-blog__text"><?echo $arItem["PREVIEW_TEXT"];?></p>
					</div>
					<div class="stat">
						<span class="stat__item">
							<svg xmlns="http://www.w3.org/2000/svg">
							<use xlink:href="#eye"></use>
							</svg>
							<?=(int)$arItem['PROPERTIES']['VIEWS']['VALUE']?>
						</span>
						<span class="stat__item">
							<svg xmlns="http://www.w3.org/2000/svg">
							<use xlink:href="#comment"></use>
							</svg>
							<?=getCountComments($user_id = false, $arItem["ID"]);?>
						</span>
					</div>
				</div>
			</a>
		</div>
	<?endforeach;?>
	</div>
</div>


<?=$arResult["NAV_STRING"]?>



