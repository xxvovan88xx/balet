<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


//print_r($arResult);die;
?>


<? if($arResult['ITEMS']){?>
	<div class="wrap__item container">
		<div class="faq">
			<h4>Видеоинструкция</h4>
			<div class="info__preview">
				<div class="info__image" style="background-image: url(/local/templates/main2019/assets/images/instruction_preview.png);" data-new-modal="#Instruction" data-url="https://www.youtube.com/embed/kNTbXJ_XV4k?enablejsapi=1"></div>
			</div>
			<? foreach($arResult['ITEMS'] as $item){?>
			<?
			$this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>

			<div class="accordion" id="<?=$this->GetEditAreaId($item['ID']);?>">
				<button type="button" class="accordion__button"><?=$item['NAME']?></button>
				<div class="accordion__roll">
					<div class="accordion__content">
						<div class="content">
							<?=$item['~PREVIEW_TEXT']?>
						</div>
					</div>
				</div>
			</div>
			<?}?>
		</div>
	</div>
<?}?>