<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


//print_r($arResult);die;
?>


<? if($arResult['ITEMS']){?>
	<div class="wrap__item container">
		<div class="reviews">

			<? foreach($arResult['ITEMS'] as $item){?>
			<?
			$this->AddEditAction($item['ID'], $item['EDIT_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($item['ID'], $item['DELETE_LINK'], CIBlock::GetArrayByID($item["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

			$video = (!empty($item['PROPERTIES']['VIDEO']['~VALUE'])) 
				? array("video" => str_replace("'", '"', $item['PROPERTIES']['VIDEO']['~VALUE'])) 
				: false ;
			?>
			<div class="reviews__item js-card-video-frame" id="<?=$this->GetEditAreaId($item['ID']);?>" <?if($video){?>data-video='<?=json_encode($video)?>'<?}?>>
				<div class="card-review <?if($video) print"card-review--video"?>" <?if($video){?>data-modal="#modalPreview"<?}?>>
					<div class="card-review__inner">
						<div class="card-review__avatar">
							<?if($item['PREVIEW_PICTURE']){?>
							<img src="<?=$item['PREVIEW_PICTURE']['SRC']?>" class="card-review__img" alt="">
							<?}?>
						</div>
						<div class="card-review__content">
							<div class="card-review__heading">
								<p class="card-review__name"><?=$item['NAME']?></p>
								<p class="card-review__city"><?=$item['PROPERTIES']['CITY']['~VALUE']?></p>
							</div>
							<p class="card-review__text"><?=$item['~PREVIEW_TEXT']?></p>
						</div>
					</div>
				</div>
			</div>
			<?}?>
		</div>
	</div>
    
<?}?>