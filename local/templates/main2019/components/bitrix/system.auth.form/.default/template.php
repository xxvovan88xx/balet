<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CJSCore::Init();
?>


<div class="modal modal--base <?if($arResult['ERROR'] && $_GET['login']=='yes'){print "js-modal-error";}?>" data-popup="sign_in" id="modalEnter">
	<div class="modal__dialog">
		<div class="modal__inner">
			<div class="modal__pane">
				<button type="button" class="modal__close" data-close></button>
				<h3 class="modal__title">Вход</h3>
				
				<?
				if ($arResult['ERROR'] && $_GET['login']=='yes')
					ShowMessage($arResult['ERROR_MESSAGE']);
				?>
				
				<form class="form"  name="system_auth_form<?=$arResult["RND"]?>" method="post" action="<?=$arResult["AUTH_URL"]?>">
					<?if($arResult["BACKURL"] <> ''):?>
						<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
					<?endif?>
					<?foreach ($arResult["POST"] as $key => $value):?>
						<input type="hidden" name="<?=$key?>" value="<?=$value?>" />
					<?endforeach?>
					<input type="hidden" name="AUTH_FORM" value="Y" />
					<input type="hidden" name="TYPE" value="AUTH" />
						
					<div class="form__item">
						<label for="enterEmail" class="form__label">Логин или E-mail*</label>
						<input name="USER_LOGIN" id="enterEmail" type="text" class="input" placeholder="Логин или E-mail" required>
						<script>
							BX.ready(function() {
								var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
								if (loginCookie)
								{
									var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
									var loginInput = form.elements["USER_LOGIN"];
									loginInput.value = loginCookie;
								}
							});
						</script>
					</div>
					<div class="form__item">
						<label for="enterPass" class="form__label">Пароль*</label>
						<input name="USER_PASSWORD" autocomplete="off" id="enterPass" type="password" class="input" placeholder="Пароль" required>
					</div>
					<div class="form__buttons">
						<button type="submit" class="button">
							<span class="button__text">Войти</span>
						</button>
						<button type="button" class="header__link" onclick="location.href='/passrecovery/?forgot_password=yes'">
							<span class="button__text">Забыли пароль?</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

