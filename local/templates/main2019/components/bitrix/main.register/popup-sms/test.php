<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}

// debug($arResult);

$sms_code = $arResult["SHOW_SMS_FIELD"] == true;
?>



<div class="modal modal--base <?if($arResult['ERRORS'] && $_GET['register']=='yes'){print "js-modal-error";}?>" data-popup="registration" id="modalReg">
	<div class="modal__dialog">
		<div class="modal__inner">
			<div class="modal__pane">
				<button type="button" class="modal__close" data-close></button>
				<h3 class="modal__title"><?php echo $sms_code ? 'Код из СМС' : ($arResult["USE_EMAIL_CONFIRMATION"] === "Y" ? 'Завершение регистрации' : 'Регистрация'); ?></h3>
								
				<?
				if ($arResult['ERRORS'] && $_GET['register']=='yes')
					ShowError(implode("<br />", $arResult["ERRORS"]));
				?>
				
				
				<form class="form" method="POST" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
					<?/*//тк нам нужно, чтобы в форме отсутсвовали поля LOGIN, PASSWORD, CONFIRM_PASSWORD, то необходимо создать их балванки, с произвольными значениями, эти значения будут перезаписаны перед регистрацией пользователя?>
					<input type="hidden" class="input" name="REGISTER[LOGIN]" value="test">
					<input type="hidden" class="input" name="REGISTER[PASSWORD]" value="123456">
					<input type="hidden" class="input" name="REGISTER[CONFIRM_PASSWORD]" value="123456">
					*/?>
				
					<?if($sms_code):?>
						<?php //debug($arResult['VALUES']['PHONE_NUMBER']); ?>

						<div class="form_popup_sms__text">Подтвердите, что этот номер телефона принадлежит вам. Введите код, который мы отправили по SMS на <nobr><?php echo $arResult['VALUES']['PHONE_NUMBER']; ?></nobr></div>
						
						<input type="hidden" name="SIGNED_DATA" value="<?=htmlspecialcharsbx($arResult["SIGNED_DATA"])?>" />

						<div class="form__item">
							<label for="regName" class="form__label">Код из СМС</label>
							<input type="text" class="input" name="SMS_CODE" value="<?=htmlspecialcharsbx($arResult["SMS_CODE"])?>" autocomplete="off" required>
						</div>

						<div id="bx_register_error" style="display:none"><?ShowError("error")?></div>

						<div id="bx_register_resend"></div>

						<div class="form__buttons">
							<button type="submit" class="button">
								<span class="button__text">Подтвердить</span>
							</button>
						</div>

						<script>
							$('body').addClass('modal-opened');
							$('.modal.modal--base[data-popup="registration"]').addClass('show');

							new BX.PhoneAuth({
								containerId: 'bx_register_resend',
								errorContainerId: 'bx_register_error',
								interval: <?=$arResult["PHONE_CODE_RESEND_INTERVAL"]?>,
								data:
									<?=CUtil::PhpToJSObject([
										'signedData' => $arResult["SIGNED_DATA"],
									])?>,
								onError:
									function(response)
									{
										var errorDiv = BX('bx_register_error');
										var errorNode = BX.findChildByClassName(errorDiv, 'errortext');
										errorNode.innerHTML = '';
										for(var i = 0; i < response.errors.length; i++)
										{
											errorNode.innerHTML = errorNode.innerHTML + BX.util.htmlspecialchars(response.errors[i].message) + '<br>';
										}
										errorDiv.style.display = '';
									}
							});
						</script>
					<?php elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y"): ?>
						<div class="form_popup_sms__text">Мы отправили письмо с подтверждением регистрации на электронную почту</div>

						<div class="form__buttons">
							<button type="button" class="button close_form_popup">
								<span class="button__text">Ок</span>
							</button>
						</div>

						<script>
							$('body').addClass('modal-opened');
							$('.modal.modal--base[data-popup="registration"]').addClass('show');

							$('.close_form_popup').click(function() {
								$('.modal.modal--base[data-popup="registration"]').removeClass('show');
								$('body').removeClass('modal-opened');
							});
						</script>
						
					<?php else: ?>
						<div class="form__item">
							<label for="regName" class="form__label">Имя*</label>
							<input id="regName" type="text" class="input" placeholder="Имя" name="REGISTER[NAME]" value="<?=$arResult["VALUES"]["NAME"]?>" required>
						</div>
						
						<div class="form__item">
							<label for="regLogin" class="form__label">Логин*</label>
							<input id="regLogin" type="text" class="input" placeholder="Логин" name="REGISTER[LOGIN]" value="<?=$arResult["VALUES"]["LOGIN"]?>" required>
						</div>
						
						
						
						<div class="form__item">
							<label for="regPhone" class="form__label">Телефон*</label>
							<input id="regPhone" type="text" class="input" placeholder="Телефон" name="REGISTER[PHONE_NUMBER]" value="<?=$arResult["VALUES"]["PHONE_NUMBER"]?>" required>
						</div>
						
						
						<div class="form__item">
							<label for="regEmail" class="form__label">E-mail*</label>
							<input id="regEmail" type="email" class="input" placeholder="E-mail" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]["EMAIL"]?>" required>
						</div>
						
						<div class="form__item" style="position:relative">
							<label for="regPass" class="form__label">Пароль (не менее 6 символов)*</label>
							<input id="regPass" type="password" class="input" placeholder="Пароль (не менее 6 символов)" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]["PASSWORD"]?>" autocomplete="off" required>
							<a class="password_show"></a>
							<input type="hidden" id="regPassRep" class="input" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>" autocomplete="off">

							<script>
								$('#regPass').on('input', function() {
									$('#regPassRep').val($(this).val());
								});
								$('.password_show').click(function() {
									let inp = $('#regPass');

									if(inp.attr('type') == 'password') {
										inp.attr('type', 'text');
									} else {
										inp.attr('type', 'password');
									}
								});
							</script>
						</div>
						<?php /*
						<div class="form__item">
							<label for="regPassRep" class="form__label">Повтор пароля*</label>
							<input id="regPassRep" type="password" class="input" placeholder="Повтор пароля" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]["CONFIRM_PASSWORD"]?>" autocomplete="off" required>
						</div>
						*/ ?>
						
						
						<?// CAPTCHA
						if ($arResult["USE_CAPTCHA"] == "Y"){?>
						<div class="form__item">
							<label for="regCaptcha" class="form__label"><?=GetMessage("CAPTCHA_REGF_TITLE")?></label>
							<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
							<input id="regCaptcha" type="text" class="input" name="captcha_word" value="" autocomplete="off" />
						</div>
						<?}
						?>
						<div class="form__item">
							<div class="checkbox">
								<label class="checkbox__label">
									<input type="checkbox" class="checkbox__input" required>
									<span class="checkbox__mark"></span>
									<span class="checkbox__text">Я соглашаюсь с <a href="/upload/privacy.pdf" target="_blank">политикой конфидециальности</a>, <a href="/upload/agreement.pdf" target="_blank">пользовательским соглашением</a> и <a href="/upload/rules.pdf" target="_blank">правилами оказания информационных услуг</a></span>
								</label>
							</div>
						</div>
						<div class="form__buttons">
							<input type="hidden" name="register_submit_button" value="Регистрация">
							<button type="submit" class="button">
								<span class="button__text">Зарегистрироваться</span>
							</button>
						</div>
					<?php endif; ?>

				</form>
			</div>
		</div>
	</div>
	<?if(!empty($arResult["FORM_NOTE"])):?>
		<p class="text--normal" style="text-align:center;">Спасибо, Ваша заявка отправлена.</p>
	<?endif;?>
</div>


<?php /*
<script>
	$('.regform > button[type="submit"]').click(function() {
		$('#regPassRep').val($('#regPass').val());
	});
</script>
*/ ?>