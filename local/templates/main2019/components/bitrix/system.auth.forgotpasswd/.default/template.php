<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

?>

<main class="main" role="main">
	<div class="wrap">
		<div class="wrap__block wrap__block--gray">
			<div class="wrap__heading">
				<h2 class="wrap__title">Восстановление пароля</h2>
			</div>
			<div class="wrap__item container">
			
				<form class="form" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_URL"]?>">
				
				<?ShowMessage($arParams["~AUTH_RESULT"]);?>
				
				<?
				if (strlen($arResult["BACKURL"]) > 0)
				{
				?>
					<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
				<?
				}
				?>
					<input type="hidden" name="AUTH_FORM" value="Y">
					<input type="hidden" name="TYPE" value="SEND_PWD">
					
					<div class="form__item">
						<label for="fogName" class="form__label"><?=GetMessage("sys_forgot_pass_login1")?></label>
						<input id="fogName" type="text" class="input" placeholder="<?=GetMessage("sys_forgot_pass_login1")?>" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>" />
						<input type="hidden" name="USER_EMAIL" />
					</div>
					
					<div class="form__buttons">
						<input type="hidden" name="send_account_info" value="<?=GetMessage("AUTH_SEND")?>">
						<button type="submit" class="button">
							<span class="button__text">Восстановить</span>
						</button>
					</div>
				</form>

				<script type="text/javascript">
				document.bform.onsubmit = function(){document.bform.USER_EMAIL.value = document.bform.USER_LOGIN.value;};
				document.bform.USER_LOGIN.focus();
				</script>


			</div>
		</div>
	</div>
</main>
