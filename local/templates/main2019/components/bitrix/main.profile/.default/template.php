<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

if($arResult["SHOW_SMS_FIELD"] == true)
{
	CJSCore::Init('phone_auth');
}

global $USER;

//Если пользователь не член тестовой группы, или подписки нужно слать его на страницу subscription
$arGroups = array();
$rows = CUser::GetUserGroupList( $USER->GetID() );
while ($row = $rows->Fetch()){
	$arGroups[] = $row['GROUP_ID'];
}
?>



<div class="wrap__block wrap__block--gray" data-space="top-small">
	<div class="wrap__item container">
		<div class="wrap__item-heading">
			<h3 class="wrap__title">Информация</h3>
			<button type="button" class="wrap__link js-account_edit-popup" data-modal="#modalAccountEdit" >Редактировать</button>
		</div>
		<div class="account-data">
			<div class="account-data__row account-data__row--name">
				<div class="account-data__cell">
					<span class="account-data__title">Имя</span>
				</div>
				<div class="account-data__cell">
					<p class="account-data__text"><?=$arResult["arUser"]["NAME"]?></p>
				</div>
				<? if($arResult["arUser"]["PERSONAL_PHOTO"]){?>
				<img src="<?=CFile::GetPath($arResult["arUser"]["PERSONAL_PHOTO"]);?>" alt="" class="account-data__avatar">
				<?}else{?>
				<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgBAMAAAB54XoeAAAAMFBMVEVHcEz/zdH/zdD/zND/zND/zNH/zdD/y9D/z8//ztH/zND//v7/2t3/8vP/5uj/09bgcG/JAAAACnRSTlMAhuTFIGqkOBBPb67k6gAABMhJREFUaN7FWztsE0EQvZAPhgqFj1AqEL/IFSDxUSpDB1WABlEF6FJF0ABVACEEFQUSiIqcwSEbx9fn/OltK/Q+THo7AdHaIbCBYO43M7u3Y3hFojjn5/nuzO6OLYtC6v6V26fTQthHzu5/ZRnj2QHJ9RfZm/eM6J6MihA64uzLxHQ794pYnJlJxnchLQDY+xLQpa4JBGe0+YZGBYrjGU3zTQgCuQwvnx7jkAKfDmNqTAhWxqtCEcfU+O4KZRxV4RsQGtjD5ZBezmT4DKhoxodCEwc5FVZQ+o7QxgmMb1gkwCRCOJaEcI4pBFWCcSwZISjiiEiIKV4BQREHRGLs4RUQEHFYGCAuFh+YEM7HZHHahDAmox8JI5zndMkWvrG6JM4tFw35nHC9SptKmGXLkvhsuWxOOM+rcUjnYcGASUYf/8K4j3Cag9C35AwJFmTM1/4gTnIGTTBwpnkIsxomdOr1Dv3UjKoJ8+uuxOeqqhEpE25KtmZT/vhKPPhOzYSSb03q66y4bkstEvFEdtzStq55z8UNac+qJHLb7dku75ZV0nkQfabgt1yBUPqkgk/aZfgvwCtjuAVbQXmrtFdQn3RDIrWXUa/82sqiSnghgqUymSvDuMad8AtVqlKhTl4qRZzUoNyMOrlboV/x44MkfIM9UIvIgxvxPZXJXsRihRIVN+juLpq8jkvETQp3sspn+DGLh6ETo5+HEr7A26R8ScWswUAc4SWcwuPa0SY8ZO3mdcoHYnuiGzay2L8ReoGdRwNbLFg38PVaM/Vk7uH7E93FQeYeXpSj8uDLl8jhqSwLp94CKxsm4uTHa1ArbpiQ6P8jRapCbEspwkJQZ4dqb2yLatGCItXK1PMkYaD7oFoRFULRLvWUdjxSQAVC2XJtMzptt8pAKL67pY9bvzc8d1VwEErGbXxSeFhhH7ryh88trdHHIyRhvi37//rGl7X6itwLlDskIZF6sq8uV33kpaowymXJtxZyUIcgxJcvL7w32SAYc/gCW4vudTbdIr7A3kC3PKtxQdRCSwBSpIBMa2NKL2BltBtfgh23gpXR50iVX4UyBxbxrbUDFhBSzfEqSJM9oi3glohIszSguPgHO5QG3M7tUlv78dLl3/mkQI2rGuXa1xJDy81iWa/n+XswAuReDS3Ai0UolS0o9/DyAem8IAmfA+8gWpQWuAMf1NKpZ5FlcPM4oPMG6gMnwQ24R7QIwJZvBjwiIE5ogN7dBg8xiNYc2q7MgccsBbKJabfAY5Y4Ny8VKcJaAzwIijvFWKxQhN1l8KgqlVZ8nPxIG75K6TaSEObgA8na5yaB9SLkk1iv1JougWYxbv3fnruI/mvjR53Aj4/Rd/VOxicEC3I8V3pRE1JnnMqY6t/lgumlXtiExteOv3G0n1dIHIFj890Fh4OmLxeF5jrbzJetkeEg9utg09iOzhE8NiMc7/+lP/tYAv/gBPtoB//wCft4DP8AD/uIEf8QFPuYFv8gGfuoW4KMzmb+7bgg/0CjptKUwv0YCtWqV+f+z2CtNTStmCKzFiujikN0GLNak+00ox6fZLyF8x3WHEGXwIfkZy19MI/xb+2wACFPJfyigcSl2zHWu2707YqnBwKtqH3ztfkXQPxfJ6F98RN/LEdBc73uZQAAAABJRU5ErkJggg==" alt="" class="account-data__avatar">
				<?}?>
			</div>
			<div class="account-data__row">
				<div class="account-data__cell">
					<span class="account-data__title">Дата рождения</span>
				</div>
				<div class="account-data__cell">
					<p class="account-data__text"><?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?></p>
				</div>
			</div>
			<div class="account-data__row">
				<div class="account-data__cell">
					<span class="account-data__title">e-mail</span>
				</div>
				<div class="account-data__cell">
					<p class="account-data__text"><?=$arResult["arUser"]["EMAIL"]?></p>
				</div>
			</div>
			<div class="account-data__row">
				<div class="account-data__cell">
					<span class="account-data__title">Номер телефона</span>
				</div>
				<div class="account-data__cell">
					<p class="account-data__text"><?=$arResult["arUser"]["PHONE_NUMBER"]?></p>
				</div>
			</div>
			<div class="account-data__row">
				<div class="account-data__cell">
					<span class="account-data__title">Подписка</span>
				</div>
				<div class="account-data__cell">
				<?if( ! in_array(KRUser::$GROUP_SUBSCRIBE_USERS, $arGroups)){?>
					<p class="account-data__text">Не активирована</p>
					<a href="/subscription/" class="account-data__link">Оформить</a>
				<?}else{?>
					<p class="account-data__text">Активирована</p>
				<?}?>
				</div>
			</div>
			<div class="account-data__row">
				<div class="account-data__cell">
					<span class="account-data__title">Пароль</span>
				</div>
				<div class="account-data__cell">
					<button type="button" class="account-data__link js-new_pass-popup" data-modal="#modalChangePass">Изменить пароль</button>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal modal--base <?if($arResult['strProfileError'] && !isset($_POST['NEW_PASSWORD'])){print "js-modal-error";}?>" id="modalAccountEdit"  data-popup="account_edit">
	<div class="modal__dialog">
		<div class="modal__inner">
			<div class="modal__pane">
				<button type="button" class="modal__close" data-close></button>
				<h3 class="modal__title">Редактировать</h3>
				
				<?
				if ($arResult['strProfileError'] && !isset($_POST['NEW_PASSWORD'])){
					ShowMessage($arResult['strProfileError']);print "<br>";
				}?>

				<form class="form" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
				<?=$arResult["BX_SESSION_CHECK"]?>
					<input type="hidden" name="lang" value="<?=LANG?>" />
					<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
					<input type="hidden" name="save" value="Сохранить">

					<div class="form__avatar avatar">
						<label class="avatar__inner">
							<?
							$avatar_src_default = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAKAAAACgBAMAAAB54XoeAAAAMFBMVEVHcEz/zdH/zdD/zND/zND/zNH/zdD/y9D/z8//ztH/zND//v7/2t3/8vP/5uj/09bgcG/JAAAACnRSTlMAhuTFIGqkOBBPb67k6gAABMhJREFUaN7FWztsE0EQvZAPhgqFj1AqEL/IFSDxUSpDB1WABlEF6FJF0ABVACEEFQUSiIqcwSEbx9fn/OltK/Q+THo7AdHaIbCBYO43M7u3Y3hFojjn5/nuzO6OLYtC6v6V26fTQthHzu5/ZRnj2QHJ9RfZm/eM6J6MihA64uzLxHQ794pYnJlJxnchLQDY+xLQpa4JBGe0+YZGBYrjGU3zTQgCuQwvnx7jkAKfDmNqTAhWxqtCEcfU+O4KZRxV4RsQGtjD5ZBezmT4DKhoxodCEwc5FVZQ+o7QxgmMb1gkwCRCOJaEcI4pBFWCcSwZISjiiEiIKV4BQREHRGLs4RUQEHFYGCAuFh+YEM7HZHHahDAmox8JI5zndMkWvrG6JM4tFw35nHC9SptKmGXLkvhsuWxOOM+rcUjnYcGASUYf/8K4j3Cag9C35AwJFmTM1/4gTnIGTTBwpnkIsxomdOr1Dv3UjKoJ8+uuxOeqqhEpE25KtmZT/vhKPPhOzYSSb03q66y4bkstEvFEdtzStq55z8UNac+qJHLb7dku75ZV0nkQfabgt1yBUPqkgk/aZfgvwCtjuAVbQXmrtFdQn3RDIrWXUa/82sqiSnghgqUymSvDuMad8AtVqlKhTl4qRZzUoNyMOrlboV/x44MkfIM9UIvIgxvxPZXJXsRihRIVN+juLpq8jkvETQp3sspn+DGLh6ETo5+HEr7A26R8ScWswUAc4SWcwuPa0SY8ZO3mdcoHYnuiGzay2L8ReoGdRwNbLFg38PVaM/Vk7uH7E93FQeYeXpSj8uDLl8jhqSwLp94CKxsm4uTHa1ArbpiQ6P8jRapCbEspwkJQZ4dqb2yLatGCItXK1PMkYaD7oFoRFULRLvWUdjxSQAVC2XJtMzptt8pAKL67pY9bvzc8d1VwEErGbXxSeFhhH7ryh88trdHHIyRhvi37//rGl7X6itwLlDskIZF6sq8uV33kpaowymXJtxZyUIcgxJcvL7w32SAYc/gCW4vudTbdIr7A3kC3PKtxQdRCSwBSpIBMa2NKL2BltBtfgh23gpXR50iVX4UyBxbxrbUDFhBSzfEqSJM9oi3glohIszSguPgHO5QG3M7tUlv78dLl3/mkQI2rGuXa1xJDy81iWa/n+XswAuReDS3Ai0UolS0o9/DyAem8IAmfA+8gWpQWuAMf1NKpZ5FlcPM4oPMG6gMnwQ24R7QIwJZvBjwiIE5ogN7dBg8xiNYc2q7MgccsBbKJabfAY5Y4Ny8VKcJaAzwIijvFWKxQhN1l8KgqlVZ8nPxIG75K6TaSEObgA8na5yaB9SLkk1iv1JougWYxbv3fnruI/mvjR53Aj4/Rd/VOxicEC3I8V3pRE1JnnMqY6t/lgumlXtiExteOv3G0n1dIHIFj890Fh4OmLxeF5jrbzJetkeEg9utg09iOzhE8NiMc7/+lP/tYAv/gBPtoB//wCft4DP8AD/uIEf8QFPuYFv8gGfuoW4KMzmb+7bgg/0CjptKUwv0YCtWqV+f+z2CtNTStmCKzFiujikN0GLNak+00ox6fZLyF8x3WHEGXwIfkZy19MI/xb+2wACFPJfyigcSl2zHWu2707YqnBwKtqH3ztfkXQPxfJ6F98RN/LEdBc73uZQAAAABJRU5ErkJggg==";
							
							$avatar_src = ($arResult["arUser"]["PERSONAL_PHOTO"])
								? $avatar_src = CFile::GetPath($arResult["arUser"]["PERSONAL_PHOTO"])
								: $avatar_src_default;
							?>
							
							<img src="<?=$avatar_src?>" data-src_default="<?=$avatar_src_default?>" alt="" class="avatar__img" id="PERSONAL_PHOTO_VIEW">
							
							<input type="file" name="PERSONAL_PHOTO" id="PERSONAL_PHOTO" class="avatar__input">
						</label>
						<input type="hidden" name="PERSONAL_PHOTO_del" value=""  id="PERSONAL_PHOTO_del_form">
						<a class="avatar__remove js-personal_photo-delete <?if(!$arResult["arUser"]["PERSONAL_PHOTO"])print "hidden"?>" href="#"></a>
					</div>
					<div class="form__item">
						<label for="editName" class="form__label">Имя</label>
						<input id="editName" type="text" class="input" placeholder="Имя" name="NAME" value="<?=$arResult["arUser"]["NAME"]?>" required>
					</div>
					<div class="form__item">
						<label for="editBirthday" class="form__label">Дата рождения</label>
						<input id="editBirthday" type="text" class="input" placeholder="Дата рождения" name="PERSONAL_BIRTHDAY" value="<?=$arResult["arUser"]["PERSONAL_BIRTHDAY"]?>"  data-birth required>
					</div>
					<div class="form__buttons">
						<button type="button" class="button button--stoke button--black-stroke" data-close>
							<span class="button__text">Отмена</span>
						</button>
						<button type="submit" class="button">
							<span class="button__text">Сохранить</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="modal modal--base <?if($arResult['strProfileError'] && isset($_POST['NEW_PASSWORD'])){print "js-modal-error";}?>" id="modalChangePass" data-popup="new_pass">
	<div class="modal__dialog">
		<div class="modal__inner">
			<div class="modal__pane">
				<button type="button" class="modal__close" data-close></button>
				<h3 class="modal__title">Изменить пароль</h3>
				
				<?
				if ($arResult['strProfileError'] && isset($_POST['NEW_PASSWORD'])){
					ShowMessage($arResult['strProfileError']);print "<br>";
				}?>
				
				<form class="form" method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
				<?=$arResult["BX_SESSION_CHECK"]?>
					<input type="hidden" name="lang" value="<?=LANG?>" />
					<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />
					<input type="hidden" name="save" value="Сохранить">
					<div class="form__item">
						<label for="changePassOld" class="form__label">Новый пароль (не менее 6 символов)</label>
						<input id="changePassOld" type="password" name="NEW_PASSWORD" class="input" placeholder="Введите новый пароль" autocomplete="off" required>
					</div>
					<div class="form__item">
						<label for="changePassNew" class="form__label">Подтверждение нового пароля</label>
						<input id="changePassNew" type="password" name="NEW_PASSWORD_CONFIRM" class="input" placeholder="Введите подтверждение нового пароля" autocomplete="off" required>
					</div>
					<div class="form__buttons">
						<button type="submit" class="button">
							<span class="button__text">Сохранить</span>
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>