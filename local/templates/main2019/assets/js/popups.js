$(document).ready(function(){
    $('[data-new-popup]').on('click', function(){
        $(''+$(this).attr('data-new-popup')).addClass('show');
        $('body').addClass('modal-opened');
    })

    $('.modal__close').on('click', function(){
        $('.modal').removeClass('show');
        $('body').removeClass('modal-opened');
    })
})