$(document).ready(function(){
	
	if($('div[data-new-modal]').length){
// 		console.log($('div[data-new-modal]').length)
		$('div[data-new-modal]').on('click', function(){
			const modal_id = $(this).attr('data-new-modal');
			const modal_src = $(this).attr('data-url');
			$(modal_id).find('iframe').attr('src', modal_src)
			$(modal_id).addClass('show');
		});
		$('.modal__close').on('click', function(){
			$(this).parent().find('iframe').attr('src', '');
		})
		$('.modal.modal--video').on('click', function(ev){
			if($(ev.target).hasClass('modal--video')){
				$(this).find('iframe').attr('src', '');
			}
		})
	}

	
// 	console.log($('div[data-new-modal]').length)
	//Комментарии
	$(".js-comments-answer").click(function(){
		
		var parent_id = parseInt($(this).data("id"));
		var name = $(this).parents(".js-comments-container").find(".js-comments-name").html();
		var comment = $(this).parents(".js-comments-container").find(".js-comments-comment").clone();
		comment.children("span").remove().end()
		comment.children("strong").remove().end()
		comment = '[b]'+name+'[/b]';
		
		//данные
		$("#commentForm input[name=parent_id]").val(parent_id);
		$("#commentForm textarea[name=comment]").val(`${comment}, `);
		
		//фокус
		document.getElementById("commentComment").focus();
		
		//внешний вид
		$(".js-comments-title-new").addClass("hidden");
		$(".js-comments-title-answer").removeClass("hidden");
	});
	
	
	//Подмена видео при вызове модального окна
	$(document).delegate(".js-card-video-frame", "click", function(){
		var obj = $(this).data("video");
		if(typeof obj !== 'undefined'){
			$(".js-card-video-frame-container").html(obj.video);
		}
	})
	
	
	//Форма оплаты
	$("#payment_form").submit(function(){
		var type = $(this).attr("method");
		var url = $(this).attr("action");
		var formData = new FormData($(this)[0]);
		
		$(this).find("button").attr("disabled","disabled");
		
		$.ajax({
			url: url,
			data: formData,
			dataType: "json",
			type: type,
			contentType: false,
			processData: false,
			success: function (data){
				if(data.status !== undefined && data.status == 'ok'){
					window.location.href = data.payform_url;
				}else if(data.status !== undefined && data.status == 'error'){
					alert("Произошла ошибка, попробуйте позже");
					window.location.href = "/";
				}
			}
		});
		return false;
	});
	
	
	//для личного кабинета, при редактировании аватарки, подменяем изображение в картинке для просмотра, после его выбора в inut file
	if($("#PERSONAL_PHOTO").length > 0){
		document.getElementById('PERSONAL_PHOTO').onchange = function (evt) {
			var tgt = evt.target || window.event.srcElement,
				files = tgt.files;

			// FileReader support
			if (FileReader && files && files.length) {
				var fr = new FileReader();
				fr.onload = function () {
					document.getElementById('PERSONAL_PHOTO_VIEW').src = fr.result;
					$(".js-personal_photo-delete").removeClass('hidden');
					$('#PERSONAL_PHOTO_del_form').val('');
				}
				fr.readAsDataURL(files[0]);
			}
			// Not supported
			else {
				console.log("FileReader not support");
			}
		}
		
		$(".js-personal_photo-delete").click(function(){
			$("#PERSONAL_PHOTO").val("");
			$('#PERSONAL_PHOTO_del_form').val('Y');
			$('#PERSONAL_PHOTO_VIEW').attr('src', $('#PERSONAL_PHOTO_VIEW').data('src_default'));
			$(this).addClass('hidden');
			return false;
		});
	
	}
	
	//Авто проигрывание видео
	var is_video_autoplay = false;
	//проигрывать все видео
	$('.js-playlist-autoplay_start').click(function(){
		is_video_autoplay = true;
		$(".js-playlist-autoplay:first").trigger("click");
		return false;
	})
	//Страница плейлиста с плеером, тут надо сделать зацикливание видео
	if($(".js-playlist-video-container").length > 0){
		is_video_autoplay = true;
	}
	
	
	//события для плеера
	window.addEventListener('message', receiveMessage, false);
	function receiveMessage(event) {
		if (event.origin !== "https://play.boomstream.com") {
			return;
		}
		// Your code here
		if(event.data.method == 'stop'){
			console.log(event.data);
		}
		
		//Страница плейлиста с плеером, тут надо сделать зацикливание видео
		if(is_video_autoplay && event.data.method == 'stop')
		{
			$(".js-playlist-autoplay.active").nextAll(".js-playlist-autoplay:not(.unavailable)").each(function(i, val){
				if(i==0){
					$(this).trigger("click");
				}
			});
		}
	}
	
	
	
	
	//В случае ошибки при авторизации или регистрации, открываем нужное окно
	$(".js-modal-error").each(function(){
		var name = $(this).data("popup");
		$(`.js-${name}-popup`).trigger("click");
	});
	
	//Когда открываем окно, забыли пароль, то окно входа нужно скрыть
	$(".js-forgotpasswd-popup").click(function(){
		$(this).parents(".modal").find(".modal__close").trigger("click");
	})
	
	
	//ПЛЕЙЛИСТ
	//лейблы при создании плейлиста, 1 окно
	/*
	$("#modalPlaylistCreate .form-creator__item").click(function(e){
		e.preventDefault();
		var input = $(this).find(".form-creator__checkbox");
		if(input.is(":checked")){
			input.removeAttr('checked');
		}else{
			input.attr('checked', 'checked');
		}
	});
	$("#modalPlaylistCreate .radio-row__label").click(function(e){
		e.preventDefault();
		
		$(this).parents(".form-creator__interval").find(".radio-row__input").removeAttr('checked');
		$(this).find(".radio-row__input").attr('checked', 'checked');
		
		//input.attr('checked', 'checked');
	});
	*/
	//Переход к выбору видео
	$(".js-playlist_create-step2").click(function(){
		
		//ФИЛЬТРОВКА ВИДЕО
		//CATEGORY
		if($(".js-playlist_create-category-chooser:checked").length > 0){
			$(`.js-playlist_create-video-chooser`).removeClass("is-category-visible")
			$(".js-playlist_create-category-chooser:checked").each(function(){
				var category = $(this).val();
				if(category=='all'){
					$(`.js-playlist_create-video-chooser`).addClass("is-category-visible");
				}else{
					$(`.js-playlist_create-video-chooser[data-category=${category}]`).addClass("is-category-visible");
				}
			})
		}else{
			$(`.js-playlist_create-video-chooser`).addClass("is-category-visible");
		}
		//TYPE
		if($(".js-playlist_create-type-chooser:checked").length > 0){
			$(`.js-playlist_create-video-chooser`).removeClass("is-type-visible")
			$(".js-playlist_create-type-chooser:checked").each(function(){
				var type = $(this).val();
				if(type=='all'){
					$(`.js-playlist_create-video-chooser`).addClass("is-type-visible");
				}else{
					$(`.js-playlist_create-video-chooser[data-type=${type}]`).addClass("is-type-visible");
				}
			})
		}else{
			$(`.js-playlist_create-video-chooser`).addClass("is-type-visible");
		}
		//DAY
		$(`.js-playlist_create-video-chooser`).removeClass("is-duration-visible")
		var interval_handle = $(`.js-playlist_create-interval-chooser`);
		var min = parseInt(interval_handle.data("min"));
		var max = parseInt(interval_handle.data("max"));
		$(`.js-playlist_create-video-chooser`).each(function(){
			var duration = parseInt($(this).data("duration"));
			if(min <= duration && duration <= max){
				$(this).addClass("is-duration-visible")
			}
		})
		
		$(`.js-playlist_create-video-chooser`).addClass("hidden")
		.filter(".is-category-visible.is-type-visible.is-duration-visible").removeClass("hidden");
		
		
		$("#modalPlaylistVideo").addClass("show");
		$("#modalPlaylistCreate").removeClass("show");
		$("#modalPlaylistInformation").removeClass("show");
		return false;
	});
	$(".js-playlist_create-step1").click(function(){
		$("#modalPlaylistCreate").addClass("show");
		$("#modalPlaylistVideo").removeClass("show");
		return false;
	});
	$(".js-playlist_create-step3").click(function(){
		$("#modalPlaylistInformation").addClass("show");
		$("#modalPlaylistVideo").removeClass("show");
		//Количество видео
		$(".js-playlist_create-video_container").html($("#modalPlaylistVideo .video-chooser__input:checked").length)
		return false;
	});
	
	//Удаление плейлиста
	$(".js-playlist_remove-popup").click(function(){
		$("#playlist_remove-field-id").val(parseInt($(this).data("id")));
	});
	
	//запуск видео в плейлисте по клику
	$("body").delegate(".js-playlist-video-player:not(.unavailable)", "click", function(){
		$(".js-playlist-video-player").removeClass("active");
		$(this).addClass("active");
		video_frame($(this).data("id"), $(this).data("video_src"));
	})
	
	//запуск видео на странице плейлиста при первой загрузке
	if($(".js-playlist-video-container").length > 0){
		video_frame($(".js-playlist-video-player.active").data("id"), $(".js-playlist-video-player.active").data("video_src"));
	}
	
	function video_frame(id, src)
	{
		var id = parseInt(id);
		var container = $(".js-playlist-video-container");
		
		var content = `
		<iframe class="hidden js-playlist-video-content" data-id="${id}" width="640" height="356" src="${src}" frameborder="0" scrolling="no"></iframe>
		 `;
		 
		//вставка видео в попап окно
		container.html(content);
	}
	
	
	
	
	//ФИЛЬТР
	//отправка формы
	/*$(".js-filter-form").submit(function(){
		var type = $(this).attr("method");
		var url = $(this).attr("action");
		var formData = new FormData($(this)[0]);
		$.ajax({
			url: url,
			data: formData,
			//dataType: "json",
			type: type,
			contentType: false,
			processData: false,
			success: function (data){
				$(".js-filter-content").html(data);
			}
		});
		return false;
	});*/
	//при изменении фильтра, отправка формы
	$(".js-filter-field").change(function(){
		$(this).parents(".js-filter-form").submit();
	});
	//кнопка сортировки acs-desc
	$(".js-filter-sort_order").click(function(){
		var sort_order = $("#filter-sort_order-field");
		if(sort_order.val() == 'ASC'){
			sort_order.val('DESC');
			$(this).addClass("filters__button--desc");
		}else{
			sort_order.val('ASC');
			$(this).removeClass("filters__button--desc");
		}
		$(this).parents(".js-filter-form").submit();
	});
	
	//запуск видео после клика
	$("body").delegate(".js-filter-video-popup:not(.unavailable)", "click", function(){
		var id = parseInt($(this).data("id"));
		var src = $(this).data("video_src");
		var container = $("#modalFilterVideo .modal__player");
		
		//active
		$(".js-filter-video-popup").removeClass("active");
		$(this).addClass("active");
		
		var content = `
		<iframe class="hidden js-playlist-video-content" data-id="${id}" width="640" height="356" src="${src}" frameborder="0" scrolling="no"></iframe>
		 `;
		 
		//вставка видео в попап окно
		container.html(content);
		$("#modalFilterVideo").addClass("show");
	})
	
	//кнопка дня в мои тренировки
	$(".js-filter-day").click(function(){
		$("#filter-day-field").val(parseInt($(this).data("day")));
		$(this).parents(".js-filter-form").submit();
		return false;
	});
	
	//кнопка избранного 1-0
	$(".js-filter-favorites").click(function(){
		var favorites = $("#filter-favorites-field");
		if(parseInt(favorites.val()) == 0){
			favorites.val('1');
			$(this).addClass("active");
		}else{
			favorites.val('0');
			$(this).removeClass("active");
		}
		$(this).parents(".js-filter-form").submit();
	});

	//избранное
	$('body').delegate(".js-favorites", "click", function(){
		var this_ = $(this);
		$.ajax({
			url: "",
			type: 'POST',
			dataType: "json",
			data: {
				id: $(this).data("id"),
				METHOD: "favorites"
			},
			success: function(res)
			{
				if(res.status !== undefined && res.status == 'ok')
				{
					//избранное на карточке товара в категории
					if(parseInt(res.type) == -1)
						this_.parents(".card-video").removeClass("card-video--fav");
					else
						this_.parents(".card-video").addClass("card-video--fav");
					
					//Если включен фильтр отбора по избранному, то при снятии с видео статуса удаляю его из выборки
					if(parseInt(res.type) == -1 && parseInt($("#filter-favorites-field").val()) == 1){
						this_.parents(".js-filter-video-popup").remove();
					}
				}
			},
			error: function(res){

			}
		});

		return false;
	});
})