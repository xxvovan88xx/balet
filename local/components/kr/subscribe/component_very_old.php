<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if(
	!CModule::IncludeModule('iblock') || 
	!CModule::IncludeModule('catalog') || 
	!CModule::IncludeModule('sale') || 
	!CModule::IncludeModule('currency')
) {
	return;
}


global $USER;
use Bitrix\Main\Config\Option;

use \Bitrix\Main\Sendpulse\ApiClient;
use \Bitrix\Main\Sendpulse\Storage\FileStorage;


//Вызовы компонента с параметрами
//сбербан шлет колбек запрос, после успешной оплаты. В заказе мы ставим статус оплачен, добавляем пользователя в группу Подписанные пользователи и продлеваем ее на дни, которые указаны в купленном товаре, далее удаляем сессию OREDER_ID, чтобы больше не проходить этот участок
if (
	$arParams['METHOD'] == 'SBER_PAYMENT_CONFIRM'
)
{
	$error_message = '';
	$error_header = false;	//Нужно ли выдавать заголовок ошибки в ответ
	$mail_error = true;	//Нужно ли слать нам сообщение о ошибке на почту
	
	$payment_answer_order_id = !empty($_REQUEST['orderNumber']) ? $_REQUEST['orderNumber'] : 0;
	$payment_answer_order_sberbank = !empty($_REQUEST['mdOrder']) ? mb_strtolower($_REQUEST['mdOrder']) : '';
	$payment_answer_operation = !empty($_REQUEST['operation']) ? mb_strtolower($_REQUEST['operation']) : '';
	$payment_answer_status = !empty($_REQUEST['status']) ? (int)$_REQUEST['status'] : 0;
	$payment_answer_check = !empty($_REQUEST['checksum']) ? mb_strtolower($_REQUEST['checksum']) : '';
	
	$payment_answer_paid = false;
	$payment_answer_updated = false;
	
	$payment_key = KRPayments::$PAYSYSTEM_SBERBANK_API_KEY;
	$payment_data = 'mdOrder;' . $payment_answer_order_sberbank . ';operation;' . $payment_answer_operation . ';orderNumber;' . $payment_answer_order_id . ';status;' . $payment_answer_status . ';';
	$payment_sign = mb_strtolower(hash_hmac('sha256', $payment_data , $payment_key));

	/* verify data checksum */
	if($payment_answer_check == $payment_sign) {
		if($payment_answer_status == 1) {
			/* Успешная операция */
			if($payment_answer_operation == 'deposited') {
				/* Списание средст */
				$order_id = explode('_', $payment_answer_order_id)[0];
				if($arOrder = CSaleOrder::GetByID($order_id)) {
					
					if($arOrder['PAYED'] != 'Y') {
						$arFields = array(
							'PAYED' => 'Y',
							'STATUS_ID' => 'F',
							'DATE_PAYED' => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat('FULL', LANG))),
							'PS_STATUS' => 'Y',
							'PS_STATUS_CODE' => 'Sberbank',
							'PS_STATUS_DESCRIPTION' => 'Sberbank оплата банковской картой',
							'PS_STATUS_MESSAGE' => 'ID транзакции: ' . $payment_answer_order_sberbank,
							'PS_RESPONSE_DATE' => date(CDatabase::DateFormatToPHP(CLang::GetDateFormat('FULL', LANG))),
							'PS_SUM' => $arOrder['PRICE'],
							'PS_CURRENCY' => 'RUB',
						);
						if(CSaleOrder::Update($arOrder['ID'], $arFields)) {
							$payment_answer_updated = true;
							$payment_answer_paid = true;
							
							
							$month_period = 0;
			
							//из заказа достаем товары, и смотрим месяцы
							$dbBasketItems = CSaleBasket::GetList(array(), array("ORDER_ID" => $arOrder['ID']), false, false, array());
							while ($arItems = $dbBasketItems->Fetch()){
								$arSelect = Array("ID", "NAME", "PROPERTY_MONTH");
								$arFilter = Array("IBLOCK_ID"=>KRIBlock::$ID_CATALOG, "ID"=>$arItems["PRODUCT_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
								$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
								while($ob = $res->GetNextElement()){
									$arFields = $ob->GetFields();
									$month_period += (int)($arFields["PROPERTY_MONTH_VALUE"]);
								}
							}
							
							//Прописываем тестовый период для пользователя
							$arGroups = array();
							$is_subscribe = false;
							$arGroups_db = CUser::GetUserGroupList( $arOrder['USER_ID'] );
							while ($arGroup = $arGroups_db->Fetch()){
								if ($arGroup['GROUP_ID'] == KRUser::$GROUP_SUBSCRIBE_USERS) {
									$DATE_ACTIVE_TO = ($arGroup['DATE_ACTIVE_TO']) ? $arGroup['DATE_ACTIVE_TO'] : "";
									$arGroup['DATE_ACTIVE_TO'] = date("d.m.Y H:i:s", strtotime("{$DATE_ACTIVE_TO} +{$month_period}month"));
									$is_subscribe = true;
								}
								$arGroups[] = $arGroup;
							}
							//Если пользователь не состоит в группе Подписанные пользователи, добавляем его
							if( ! $is_subscribe){
								$arGroups[] = array(
									"GROUP_ID" => KRUser::$GROUP_SUBSCRIBE_USERS,
									"DATE_ACTIVE_TO" => date("d.m.Y H:i:s", strtotime("+{$month_period}month"))
								);
							}
							CUser::SetUserGroup($arOrder['USER_ID'], $arGroups);
							
							
							//получаем список медиа-файлов платного доступа, берем первый попавшийся и прописываем для него доступ пользователю, эта операция позволит пользователю просматривать все видео файлы из этого платного доступа
							$KRBoomstream = new KRBoomstream();
							if($listmedia = $KRBoomstream->listmedia())
							{	
								//user info
								$rsUser = CUser::GetByID($arOrder['USER_ID']);
								$arUser = $rsUser->Fetch();
								
								//Добавить покупателя в платный доступ.
								$media = $listmedia[0]['Code'];
								$email = $arUser['EMAIL'];
								$access_expire = date("Y-m-d", strtotime("+{$month_period}month"));
								$access = $KRBoomstream->addbuyer($media, $email, $access_expire);
								
								//добавление пароля пользователю
								if(!empty($access['Hash'])){
									$user = new CUser;
									$fields = Array( 
										"UF_BOOMSTREAM_HASH" => $access['Hash'], 
									); 
									$user->Update($arOrder['USER_ID'], $fields);
								}


								/* Sendpulse start */
								define('API_USER_ID', '16869f140c16714ce057285f4132a2e1');
								define('API_SECRET', '538bbe1264649b40b7e3773bf5e6382f');
								define('PATH_TO_ATTACH_FILE', __FILE__);
								
								$SPApiClient = new ApiClient(API_USER_ID, API_SECRET, new FileStorage());
						
								$book_id = 407250;
								$email = array(
									'email' => $arUser['EMAIL'],
									'name' => $arUser['NAME']
								);
								
								$SPApiClient->startEventAutomation360('purchase', $email);
								/* Sendpulse end */
							}
							
							
							/* отправляем письмо 
							$mail_data = array(
								'ORDER_ID' => $arOrder['ID'],
								'ORDER_DATE' => $arOrder['DATE_INSERT_FORMAT'],
								'ORDER_SUM' => $arOrder['PRICE'],
								'SALE_EMAIL' => COption::GetOptionString('sale', 'order_email', 'imag@proficosmetics.ru'),
								'PAYMENT_SYSTEM' => 'Sberbank Card'
							);
							CEvent::SendImmediate('ADMIN_ORDER_PAID', 's1', $mail_data);
							*/
						}
						else {
							$error_message = 'CSaleOrder::PayOrder() error';
						}
					}
					else {
						$error_message = 'Order paid already';
						$mail_error = false;
					}
				}
				else {
					$error_message = 'Order not found: ' . $payment_answer_order;
				}
			}
			else {
				$error_message = 'Other operation: ' . $payment_answer_operation . ' (should be approved)';
			}
		}
		else {
			$error_message = 'Operation error status';
		}
	}
	else {
		$error_message = 'Wrong checksum';
	}
	
	$log_file = KRPayments::$PAYSYSTEM_LOG;
	if(!empty($log_file)) {
		$file = fopen($_SERVER['DOCUMENT_ROOT'] . $log_file, 'a+');
		fwrite($file, 'Date: ' . date('Y.m.d H:i:s') . "\r\n");
		fwrite($file, 'Sberbank data: ' . json_encode($_REQUEST) . "\r\n");
		fwrite($file, 'Order updated: ' . ($payment_answer_updated ? 'Y' : 'N') . "\r\n");
		fwrite($file, 'Order paid: ' . ($payment_answer_paid ? 'Y' : 'N') . "\r\n");
		fwrite($file, 'Order error: ' . $error_message . "\r\n\r\n");
		fclose($file);
	}
	
	//LocalRedirect('/personal/account-history/');
	exit();
}



//POST запросы
if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	//добавление товара в корзину
	if($USER->IsAuthorized() && $_REQUEST['METHOD'] == 'add_to_cart' && !empty($_REQUEST['ID']))
	{
		//удаляем товары из корзины, т.к у нас может лежать только 1 товар
		CSaleBasket::DeleteAll(CSaleBasket::GetBasketUserID(), False);
		
		$arFilter = array(
			'IBLOCK_ID' => array(KRIBlock::$ID_CATALOG),
			'ACTIVE' => 'Y',
			'ID' => $_REQUEST['ID'],
		);
		$arSelect = array('ID', 'NAME');
		$rsElement = CIBlockElement::GetList(false, $arFilter, false, false, $arSelect);
		while($product = $rsElement->GetNext())
		{
			$product['PRICE'] = KRSubsribeCatalog::getInstance()->getProductPrice($product['ID']);
			$arProducts[] = $product;
			$arFields = array(
				'PRODUCT_ID' => $product['ID'],
				'PRICE' => $product['PRICE'],
				'CURRENCY' => 'RUB',
				'QUANTITY' => 1,
				'LID' => SITE_ID,
				'NAME' => $product['NAME'],
				'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
				'MODULE' => 'catalog',
			);
			if(!$basket_id_item = CSaleBasket::Add($arFields)) throw new Exception('Не удалось добавить товар в корзину.');
		}
		LocalRedirect("/payment/");
		exit();
	}
}



//AJAX запросы
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')
{
    $APPLICATION->RestartBuffer();
	
	$result = array(
		"status" => "error"
	);
	
	//дорабление заказа
	if($USER->IsAuthorized() && $_REQUEST['METHOD'] == 'order_paylink' && ! KROrder::isBasketEmpty())
	{
		$PAYMENT_SBER_ID = KRPayments::$PAYSYSTEM_SBERBANK_ID; // Sber
		$PERSON_TYPE_ID = KROrder::$PERSON_TYPE_FIZ_ID; // TODO: физ.лицо
		
		try {
			$arBASKET = array();
			$rsBasket = CSaleBasket::GetList(
				$arOrder = array(),
				$arFilter = array('FUSER_ID' => CSaleBasket::GetBasketUserID(), 'LID' => SITE_ID, 'ORDER_ID' => 'NULL'),
				false
			);
			while($obBasket = $rsBasket->GetNext()){
				$arBASKET[] = $obBasket;
			}

			if($arBASKET)
			{
				$arErrors = $arWarnings = array();
				$order = CSaleOrder::DoCalculateOrder(SITE_ID, $USER->GetID(), $arBASKET, 1, false, false, $PAYMENT_SBER_ID, false, $arErrors, $arWarnings);

				$arFields = array(
					'LID' => SITE_ID,
					'PERSON_TYPE_ID' => $PERSON_TYPE_ID,
					'PAYED' => 'N',
					'CANCELED' => 'N',
					"STATUS_ID" => "N",
					'PRICE' => round($order['ORDER_PRICE']),
					'CURRENCY' => 'RUB',
					'USER_ID' => $USER->GetID(),
					'PAY_SYSTEM_ID' => $PAYMENT_SBER_ID,
				);
				
				if (CModule::IncludeModule('statistic'))
					$arFields['STAT_GID'] = CStatistic::GetEventParam();

				$orderId = CSaleOrder::Add($arFields);
				if( ! $orderId) throw new Exception('Не удалось создать заказ.');

				$cSaleBakset = new CSaleBasket();
				if(!$cSaleBakset->OrderBasket($orderId, CSaleBasket::GetBasketUserID(), SITE_ID)){
					throw new Exception('Не удалось привязать товары из корзины к заказу.');
				}

				//сохранение заказа в сессию
				KROrder::setOrder($orderId);
				
				//URL для оплаты 
				$payform_url = KRPayments::payform($orderId);
				if($payform_url)
				{
					$result = array(
						"status" => "ok",
						"payform_url" => $payform_url
					);
				}
			}
		}
		catch(Exception $e){
		}
		
	}
	
	//Подписка
	else if($_REQUEST['METHOD'] == 'subscribe' )
	{
		//Проверяю, есть ли пользователь с таким email
		if(!empty($_REQUEST['EMAIL']) && filter_var($_REQUEST['EMAIL'], FILTER_VALIDATE_EMAIL))
		{
			$filter = Array("EMAIL" => $_REQUEST['EMAIL']);
			$rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
			$arUser = $rsUsers->Fetch();
			if( ! $arUser)
			{	
				//пароль
				$pass = randString(7);
				
				$user = new CUser;
				$arFields = Array(
					"NAME"              => $_REQUEST['NAME'],
					"LAST_NAME"         => "",
					"EMAIL"             => $_REQUEST['EMAIL'],
					"LOGIN"             => $_REQUEST['EMAIL'],
					"LID"               => SITE_ID,
					"LANGUAGE_ID"       => LANGUAGE_ID,
					"ACTIVE"            => "Y",
					"GROUP_ID"          => array(KRUser::$GROUP_FREE_ACCESS),
					"PASSWORD"          => $pass,
					"CONFIRM_PASSWORD"  => $pass,
					"USER_IP"    		=> $_SERVER["REMOTE_ADDR"],
					"USER_HOST"    		=> @gethostbyaddr($_SERVER["REMOTE_ADDR"]),
				);
				$USER_ID = $user->Add($arFields);
				if (intval($USER_ID) > 0){
					
					//Прописываем тестовый период для пользователя
					$free_access_period = Option::get("main", "free_access_period");
					if($free_access_period && $USER_ID)
					{
						$arGroups_db = CUser::GetUserGroupList( $USER_ID );
						while ($arGroup = $arGroups_db->Fetch()){

							if ($arGroup['GROUP_ID'] == KRUser::$GROUP_FREE_ACCESS) {
								$arGroup['DATE_ACTIVE_TO'] = date("d.m.Y H:i:s", strtotime("+{$free_access_period}days"));
							}
							$arGroups[] = $arGroup;
						}
						CUser::SetUserGroup($USER_ID, $arGroups);
					}
					
					//*** Копия кода находится также в ini.php, просьба дублировать правки и туда
					//регистрациея пользователя в бумстрим, с демо доступом
					//получаем список медиа-файлов демо доступа, берем первый попавшийся и прописываем для него доступ пользователю, эта операция позволит пользователю просматривать все видео файлы из этого демо доступа
					if(defined("BOOMSTREAM_PASS_RULE_CODE_DEMO")){
						$KRBoomstream = new KRBoomstream(BOOMSTREAM_PASS_RULE_CODE_DEMO);
						if($listmedia = $KRBoomstream->listmedia())
						{
							//Добавить покупателя в платный доступ.
							$media = $listmedia[0]['Code'];
							$email = $arFields["EMAIL"];
							$access_expire = date("Y-m-d", strtotime("+{$free_access_period}days"));
							$access = $KRBoomstream->addbuyer($media, $email, $access_expire);
							
							//добавление пароля пользователю
							if(!empty($access['Hash'])){
								$user = new CUser;
								$fields = Array( 
									"UF_BOOMSTREAM_HASH_D" => $access['Hash'], 
								); 
								$user->Update($USER_ID, $fields);
							}
						}
					}
					
					//шлем пиcьмо
					$arEventFields = array();
					$arEventFields['USER_ID'] 	= $USER_ID;
					$arEventFields['EMAIL'] 	= $arFields['EMAIL'];
					$arEventFields['LOGIN'] 	= $arFields['LOGIN'];
					$arEventFields['PASSWORD'] 	= $arFields['PASSWORD'];
					$arEventFields['NAME'] 		= $arFields['NAME'];
					$arEventFields['USER_IP'] 	= $arFields['USER_IP'];
					$arEventFields['USER_HOST'] = $arFields['USER_HOST'];
					if (!CEvent::Send('REG_NEW_USER', SITE_ID, $arEventFields)){
						throw new Exception('Произошла ошибка во время отправки заказа. Повторите попытку позже.');
						
					}
					
					$USER->Authorize($USER_ID);
					
					$result = array(
						"status" => "ok",
					);
				}
			}
		}
	}
	
	print json_encode($result);
	exit();
}



//Выводимый контент
$arResult['ITEMS'] = array();

//Страница истории покупок
if($arParams["PAGE"]=='account_history')
{
	// Выведем даты всех заказов текущего пользователя за текущий месяц, отсортированные по дате заказа
	$arFilter = Array();
	$arFilter["USER_ID"] = $USER->GetID();
	$arFilter["STATUS_ID"] = "F";
	$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter);
	while ($ar_sales = $db_sales->Fetch())
	{
		//достаем информацию о товаре
		$month_period = 0;
		$dbBasketItems = CSaleBasket::GetList(array(), array("ORDER_ID" => $ar_sales["ID"]), false, false, array());
		while ($arItems = $dbBasketItems->Fetch()){
			$arSelect = Array("ID", "NAME", "PROPERTY_MONTH");
			$arFilter = Array("IBLOCK_ID"=>KRIBlock::$ID_CATALOG, "ID"=>$arItems["PRODUCT_ID"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNextElement()){
				$arFields = $ob->GetFields();
				$month_period += (int)($arFields["PROPERTY_MONTH_VALUE"]);
			}
		}
		$ar_sales["MONTH_PERIOD"] = $month_period;
		$arResult['ITEMS'][$ar_sales["ID"]] = $ar_sales;
	}
}
//страница подписок
else
{
	
	$arFilter = Array();
	$arFilter["IBLOCK_ID"] = KRIBlock::$ID_CATALOG;
	$arFilter["ACTIVE"] = 'Y';
	$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter);
	while($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$item['PROPS'] = $ob->GetProperties();
		$item['PRICE'] = KRSubsribeCatalog::getInstance()->getProductPrice($item['ID']);
		
		$arResult['ITEMS'][$item["ID"]] = $item;
	}
}

//var_dump($arResult['ITEMS']);

$this->IncludeComponentTemplate();


