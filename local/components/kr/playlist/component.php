<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if(!CModule::IncludeModule('iblock')) {
	return;
}
global $USER;
$arResult['VIDEOS'] = array();
$arResult['PLAYLISTS'] = array();


//Если пользователь не член тестовой группы, или подписки нужно слать его на страницу subscription
$arGroups = array();
$rows = CUser::GetUserGroupList( $USER->GetID() );
while ($row = $rows->Fetch()){
	$arGroups[$row['GROUP_ID']] = $row;
}

$arResult['USER_GROUPS'] = $arGroups;

if( ! array_key_exists(KRUser::$GROUP_FREE_ACCESS, $arGroups) && ! array_key_exists(KRUser::$GROUP_SUBSCRIBE_USERS, $arGroups)){
	LocalRedirect("/subscription/");
}


//Подписан ли пользователь, если залогинен
$arParams["IS_SUBSCRIBE"] 	= (array_key_exists(KRUser::$GROUP_SUBSCRIBE_USERS, $arGroups)) ? true : false;

//пароль пользователя для boomstream
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
$arParams['BOOMSTREAM_HASH'] = (!empty($arUser["UF_BOOMSTREAM_HASH"])) ? $arUser["UF_BOOMSTREAM_HASH"] : false ;
$arParams['BOOMSTREAM_HASH_D'] = (!empty($arUser["UF_BOOMSTREAM_HASH_D"])) ? $arUser["UF_BOOMSTREAM_HASH_D"] : false ;

//var_dump($arParams);

if($_SERVER['REQUEST_METHOD'] == 'POST')
{	
	//Удаление плейлиста
	if($_REQUEST['METHOD'] == 'playlist_remove' && isset($_REQUEST['PLAYLIST_ID']))
	{
		//проверяем принадлежит ли плейлист пользователю
		$arFilter = Array();
		$arFilter["IBLOCK_ID"] = KRIBlock::$ID_PLAYLISTS;
		$arFilter["PROPERTY_USER"] = $USER->GetID();
		$arFilter["ID"] = $_REQUEST['PLAYLIST_ID'];
		$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter);
		if($ob = $res->GetNextElement())
		{
			CIBlockElement::Delete($_REQUEST['PLAYLIST_ID']);
		}
		LocalRedirect("/personal/playlist/");
	}
	//Сохранение плейлиста
	else if($_REQUEST['METHOD'] == 'playlist_create' && isset($_REQUEST['PLAYLIST']))
	{
		//Пункты списков
		$CATEGORY_ENUM = array();
		$db_enum_list = CIBlockProperty::GetPropertyEnum("CATEGORY", Array(), Array("IBLOCK_ID"=>KRIBlock::$ID_PLAYLISTS));
		while($row = $db_enum_list->GetNext()){
			$CATEGORY_ENUM[$row['XML_ID']] = $row['ID'];
		}
		$TYPE_ENUM = array();
		$db_enum_list = CIBlockProperty::GetPropertyEnum("TYPE", Array(), Array("IBLOCK_ID"=>KRIBlock::$ID_PLAYLISTS));
		while($row = $db_enum_list->GetNext()){
			$TYPE_ENUM[$row['XML_ID']] = $row['ID'];
		}
		
		
		//обработка переменных из формы
		//категории
		if(in_array("all", $_REQUEST['PLAYLIST']['CATEGORY'])){
			$_REQUEST['PLAYLIST']['CATEGORY'] = array_values($CATEGORY_ENUM);
		}else{
			foreach($_REQUEST['PLAYLIST']['CATEGORY'] as $key=>$xml_id)
				$_REQUEST['PLAYLIST']['CATEGORY'][$key] = $CATEGORY_ENUM[$xml_id];
		}
		//типы
		if(in_array("all", $_REQUEST['PLAYLIST']['TYPE'])){
			$_REQUEST['PLAYLIST']['TYPE'] = array_values($TYPE_ENUM);
		}else{
			foreach($_REQUEST['PLAYLIST']['TYPE'] as $key=>$xml_id)
				$_REQUEST['PLAYLIST']['TYPE'][$key] = $TYPE_ENUM[$xml_id];
		}
		//Длительность
		$db_enum_list = CIBlockProperty::GetPropertyEnum("INTERVAL", Array(), Array("IBLOCK_CODE"=>KRIBlock::$ID_PLAYLISTS, "XML_ID"=>$_REQUEST['PLAYLIST']['INTERVAL']));
		$ar_enum_list = $db_enum_list->GetNext();
		$_REQUEST['PLAYLIST']['INTERVAL'] = $ar_enum_list["ID"];
		
		
		//Сохранение плейлиста
		$el = new CIBlockElement;

		$PROPS = array();
		$PROPS['USER'] 		= $USER->GetID();
		$PROPS['CATEGORY'] 	= $_REQUEST['PLAYLIST']['CATEGORY'];
		$PROPS['TYPE'] 		= $_REQUEST['PLAYLIST']['TYPE'];
		$PROPS['INTERVAL'] 	= $_REQUEST['PLAYLIST']['INTERVAL'];
		$PROPS['VIDEO'] 	= $_REQUEST['PLAYLIST']['VIDEO'];

		$arLoadProductArray = Array(
			"MODIFIED_BY"    	=> $USER->GetID(), 	// элемент изменен текущим пользователем
			"IBLOCK_SECTION_ID" => false,          	// элемент лежит в корне раздела
			"IBLOCK_ID"      	=> KRIBlock::$ID_PLAYLISTS,
			"NAME"           	=> $_REQUEST['PLAYLIST']['NAME'],
			"ACTIVE"         	=> "Y",
			"PREVIEW_TEXT"   	=> $_REQUEST['PLAYLIST']['PREVIEW_TEXT'],
			"PROPERTY_VALUES"	=> $PROPS,
		);

		$PRODUCT_ID = $el->Add($arLoadProductArray);
		LocalRedirect("/personal/playlist/");
	}
}




//ВИДЕО
$arFilter = Array();
$arFilter["IBLOCK_ID"] = KRIBlock::$ID_WORKOUTS;
$arFilter["ACTIVE"] = 'Y';
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter);
while($ob = $res->GetNextElement())
{
	$item = $ob->GetFields();
	$item['PROPS'] = $ob->GetProperties();
	
	$arResult['VIDEOS'][$item["ID"]] = $item;
}


//ПЛЕЙЛИСТЫ
$arFilter = Array();
$arFilter["IBLOCK_ID"] = KRIBlock::$ID_PLAYLISTS;
$arFilter["ACTIVE"] = 'Y';
$arFilter["PROPERTY_USER"] = $USER->GetID();
if(!empty($_GET['ID'])){
	$arFilter["ID"] = $_GET['ID'];
}
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter);
while($ob = $res->GetNextElement())
{
	$item = $ob->GetFields();
	$item['PROPS'] = $ob->GetProperties();
	
	//cover
	$item['COVER'] = '';
	if(isset($item['PROPS']['VIDEO']['VALUE'][0])){
		$item['COVER'] = $arResult['VIDEOS'][$item['PROPS']['VIDEO']['VALUE'][0]]["PREVIEW_PICTURE"];
	}
	
	$arResult['PLAYLISTS'][] = $item;
}

if( ! empty($_GET['ID']) && ! $arResult['PLAYLISTS']){
	die("Playlist not found");
}



$this->IncludeComponentTemplate();

