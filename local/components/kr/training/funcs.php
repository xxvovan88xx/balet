<?

function getTrainigFilterItems(
	$arITEMS = false, 
	$arCATEGORY = array(), 
	$catsName = array(), 
	$count = 0, 
	$duration_current = 0, 
	$duration_max = 0,
	$notItemsId = false,
	$params = array()
){
	$items = array();
	$count_ = 0;
	if( ! $arITEMS || ! $arCATEGORY) return $result;
	
	//сбор всех ID по выбранным категориям
	$arCatsID = array();
	foreach($catsName as $cat){
		if(!empty($arCATEGORY[$cat])){
			foreach($arCATEGORY[$cat] as $cat_id){
				if(empty($notItemsId) || ! in_array($cat_id, $notItemsId)){
					
					//дополнительные условия
					//текст в описании
					if(!empty($params['PREVIEW_TEXT_SEARCH'])){
						$is_exts = false;
						foreach($params['PREVIEW_TEXT_SEARCH'] as $str){
							if(strpos(mb_strtolower(trim($arITEMS[$cat_id]["PREVIEW_TEXT"])), mb_strtolower(trim($str))) !== false){
								$is_exts = true;
							}
						}
						if( ! $is_exts){
							continue;
						}
					}
					//текущий день
					/*
					if(!empty($params['PROPERTY_DAY'])){
						if((int)($arITEMS[$cat_id]['PROPS']['DAY']['VALUE']) != (int)($params['PROPERTY_DAY'])){
							continue;
						}
					}
					*/
					
					$arCatsID[] = $cat_id;
				}
			}
		}
	}
	
	if($arCatsID)
	{
		shuffle($arCatsID);
		foreach($arCatsID as $item_id)
		{
			$count_++;
			if(
				($count > 0 && $duration_max ==0 && $count_ <= $count) ||
				($count == 0 && $duration_max > 0 && $duration_current + (float)($arITEMS[$item_id]['PROPS']['DURATION']['VALUE']) <= $duration_max) ||
				($count > 0 && $duration_max > 0 && $count_ <= $count && $duration_current + (float)($arITEMS[$item_id]['PROPS']['DURATION']['VALUE']) <= $duration_max)
			){
				$duration_current += (float)($arITEMS[$item_id]['PROPS']['DURATION']['VALUE']);
				$items[] = $arITEMS[$item_id];
			}
		}
	}
	
	return array(
		"duration" => $duration_current,
		"items" => $items,
	);
}


function getTrainigFilterItemsbySchema($DAY, $arParams, $arITEMS, $arCATEGORY)
{
	$arResult = array();
	$notItemsId = array();
	$duration_current_ = 0;
	$all = array('leg-chair','cardio-chair','cardio-middle','back-mat','leg-stretching','press-mat','press-chair','press-middle','leg-mat','leg-middle','hand-mat','hand-chair','hand-middle','back-middle');
	
	/*
	Схема
	Если пользователь выбрал «Всё тело». Выборка происходит из полного списка тренировок.
	1. Первое видео ему выбираем из Ноги/ягодицы (опора) или кардио (опора, на середине)
		'leg-chair','cardio-chair','cardio-middle'
	2. Последнее видео из спина (коврик) или растяжка (ноги/ягодицы, тело).
		'back-mat','leg-stretching'
	3. Между ними рандомом накидываем видео из пресс, ноги/ягодицы, руки (коврик, опора, на середине), спина (на середине). Так, чтобы не превышало общую длительность тренировки в день.
		'press-mat','press-chair','press-middle','leg-mat','leg-chair','leg-middle','hand-mat','hand-chair','hand-middle','back-middle'
	*/
	if($arParams['TRAINIG_FILTER']['AIM']["SELECTED"] == 'ALL')
	{
		//Первое видео
		$item_first = getTrainigFilterItems(
			$arITEMS, 
			$arCATEGORY, 
			$catsName 			= array('leg-chair','cardio-chair','cardio-middle'), //название 'category-type'
			$count 				= 1, //запрашиваем одно видео
			$duration_current 	= 0,
			$duration_max 		= 0, // в случае запроса по количеству, тут нужно указать 0
			$notItemsId			= array(), //Перечислены id, которые нужно исключить из выборки
			$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
		);
		if(!empty($item_first['items'])){
			$arResult[] = $item_first['items'][0];
			$notItemsId[] = $item_first['items'][0]['ID'];
			$duration_current_ = $item_first['duration'];
		}
		
		//Последнее видео
		$item_last = getTrainigFilterItems(
			$arITEMS, 
			$arCATEGORY, 
			$catsName 			= array('back-mat','leg-stretching'), //название 'category-type'
			$count 				= 1, //запрашиваем одно видео 
			$duration_current 	= $item_first['duration'],
			$duration_max 		= 0, // в случае запроса по количеству, тут нужно указать 0
			$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
			$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
		);
		if(!empty($item_last['items'])){
			$notItemsId[] = $item_last['items'][0]['ID'];
			$duration_current_ = $item_last['duration'];
		}
		
		//Заполняем середину
		$item_middle = getTrainigFilterItems(
			$arITEMS, 
			$arCATEGORY, 
			$catsName 			= array('press-mat','press-chair','press-middle','leg-mat','leg-chair','leg-middle','hand-mat','hand-chair','hand-middle','back-middle'), //название 'category-type'
			$count 				= 0, //в случае запроса по максимальному времени, тут нужно указать 0
			$duration_current 	= $item_last['duration'],
			$duration_max 		= (float)($arParams['TRAINIG_FILTER']['DURATION']["SELECTED"]), // указываем максимальное время, которое не должно превышать суммарно все выбранные видео
			$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
			$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
		);
		if(!empty($item_middle['items'])){
			foreach($item_middle['items'] as $item){
				$arResult[] = $item;
			}
			$duration_current_ = $item_middle['duration'];
		}
		//Последнее видео
		if(!empty($item_last['items'])){
			$arResult[] = $item_last['items'][0];
		}
	}
	
	/*
	Ноги, как у балерины тоже самое, только:
	1. Первое видео из Ноги/ягодицы (коврик, опора, на середине),
		'leg-mat','leg-chair','leg-middle',
	2. Последнее из Растяжка (где есть словосочетание «растяжка ног»)
		'leg-stretching','hand-stretching','back-stretching','press-stretching','cardio-stretching',
	3. Промежуточные Кардио (опора, на середине)
		'cardio-chair','cardio-middle'
	*/
	else if($arParams['TRAINIG_FILTER']['AIM']["SELECTED"] == 'LEGS')
	{
		//Первое видео
		$item_first = getTrainigFilterItems(
			$arITEMS, 
			$arCATEGORY, 
			$catsName 			= array('leg-mat','leg-chair','leg-middle',), //название 'category-type'
			$count 				= 1, //запрашиваем одно видео
			$duration_current 	= 0,
			$duration_max 		= 0, // в случае запроса по количеству, тут нужно указать 0
			$notItemsId			= array(), //Перечислены id, которые нужно исключить из выборки
			$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
		);
		if(!empty($item_first['items'])){
			$arResult[] = $item_first['items'][0];
			$notItemsId[] = $item_first['items'][0]['ID'];
			$duration_current_ = $item_first['duration'];
		}
		
		//Последнее видео
		$item_last = getTrainigFilterItems(
			$arITEMS, 
			$arCATEGORY, 
			$catsName 			= array('leg-stretching','hand-stretching','back-stretching','press-stretching','cardio-stretching'), //название 'category-type'
			$count 				= 1, //запрашиваем одно видео 
			$duration_current 	= $item_first['duration'],
			$duration_max 		= 0, // в случае запроса по количеству, тут нужно указать 0
			$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
			$params				= array("PREVIEW_TEXT_SEARCH"=>array("растяжка ног"), "PROPERTY_DAY"=>$DAY) //доп условия
		);
		if(!empty($item_last['items'])){
			$notItemsId[] = $item_last['items'][0]['ID'];
			$duration_current_ = $item_last['duration'];
		}
		
		//Заполняем середину
		$item_middle = getTrainigFilterItems(
			$arITEMS, 
			$arCATEGORY, 
			$catsName 			= array('cardio-chair','cardio-middle'), //название 'category-type'
			$count 				= 0, //в случае запроса по максимальному времени, тут нужно указать 0
			$duration_current 	= $item_last['duration'],
			$duration_max 		= (float)($arParams['TRAINIG_FILTER']['DURATION']["SELECTED"]), // указываем максимальное время, которое не должно превышать суммарно все выбранные видео
			$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
			$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
		);
		if(!empty($item_middle['items'])){
			foreach($item_middle['items'] as $item){
				$arResult[] = $item;
			}
			$duration_current_ = $item_middle['duration'];
		}
		
		//Последнее видео
		if(!empty($item_last['items'])){
			$arResult[] = $item_last['items'][0];
		}
	}
	/*
	Красивая осанка и мышцы кора
	1. Первое выборка из Пресса (коврик, опора, на середине),
		'press-mat','press-chair','press-middle',
	2. Последнее растяжка (где есть словосочетание «растяжка тела»)
		'leg-stretching','hand-stretching','back-stretching','press-stretching','cardio-stretching',
	3. Потом поочередно берем одно видео из Спина (коврик, на середине, опора), другое видео из руки (опора, коврик, на середине), пока влезаем в общую длительность в день
		'back-mat','back-middle','back-chair'
		'hand-mat','hand-middle','hand-chair'
	*/
	else if($arParams['TRAINIG_FILTER']['AIM']["SELECTED"] == 'POSE')
	{
		//Первое видео
		$item_first = getTrainigFilterItems(
			$arITEMS, 
			$arCATEGORY, 
			// $catsName 			= array('press-mat','press-chair','press-middle'), //название 'category-type'
			$catsName 			= array('back-mat','back-middle','back-chair'), //название 'category-type'
			$count 				= 1, //запрашиваем одно видео
			$duration_current 	= 0,
			$duration_max 		= 0, // в случае запроса по количеству, тут нужно указать 0
			$notItemsId			= array(), //Перечислены id, которые нужно исключить из выборки
			$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
		);
		if(!empty($item_first['items'])){
			$arResult[] = $item_first['items'][0];
			$notItemsId[] = $item_first['items'][0]['ID'];
			$duration_current_ = $item_first['duration'];
		}
		
		//Последнее видео
		$item_last = getTrainigFilterItems(
			$arITEMS, 
			$arCATEGORY, 
			// $catsName 			= array('leg-stretching','hand-stretching','back-stretching','press-stretching','cardio-stretching'), //название 'category-type'
			$catsName 			= array('press-mat','press-chair','press-middle'), //название 'category-type'
			$count 				= 1, //запрашиваем одно видео 
			$duration_current 	= $item_first['duration'],
			$duration_max 		= 0, // в случае запроса по количеству, тут нужно указать 0
			$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
			$params				= array("PREVIEW_TEXT_SEARCH"=>array("растяжка тела"), "PROPERTY_DAY"=>$DAY) //доп условия
		);
		if(!empty($item_last['items'])){
			$notItemsId[] = $item_last['items'][0]['ID'];
			$duration_current_ = $item_last['duration'];
		}
		
		//Заполняем середину
		$duration_current_ = $item_last['duration'];
		while ($duration_current_ < (float)($arParams['TRAINIG_FILTER']['DURATION']["SELECTED"]))
		{
			$item_middle1 = getTrainigFilterItems(
				$arITEMS, 
				$arCATEGORY, 
				$catsName 			= array('back-mat','back-middle','back-chair','leg-stretching','hand-stretching','back-stretching','press-stretching','cardio-stretching'), //название 'category-type'
				$count 				= 1, //запрашиваем одно видео 
				$duration_current 	= $duration_current_,
				$duration_max 		= (float)($arParams['TRAINIG_FILTER']['DURATION']["SELECTED"]), // в случае запроса по количеству и при этом учитывать время, тут нужно указать максимальное время
				$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
				$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
			);
			if(!empty($item_middle1['items'])){
				$arResult[] = $item_middle1['items'][0];
				$notItemsId[] = $item_middle1['items'][0]['ID'];
				$duration_current_ = $item_middle1['duration'];
			}
			
			$item_middle2 = getTrainigFilterItems(
				$arITEMS, 
				$arCATEGORY, 
				$catsName 			= array('hand-mat','hand-middle','hand-chair'), //название 'category-type'
				$count 				= 1, //запрашиваем одно видео 
				$duration_current 	= $duration_current_,
				$duration_max 		= (float)($arParams['TRAINIG_FILTER']['DURATION']["SELECTED"]), // в случае запроса по количеству и при этом учитывать время, тут нужно указать максимальное время
				$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
				$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
			);
			if(!empty($item_middle2['items'])){
				$arResult[] = $item_middle2['items'][0];
				$notItemsId[] = $item_middle2['items'][0]['ID'];
				$duration_current_ = $item_middle2['duration'];
			}
			
			if(empty($item_middle1['items']) && empty($item_middle2['items'])){
				break;
			}
		}
		
		//Последнее видео
		if(!empty($item_last['items'])){
			$arResult[] = $item_last['items'][0];
		}
	}
	/*
	Жиросжигающая тренировка
	1. Первое кардио (опора, на середине),
		'cardio-chair', 'cardio-middle'
	2. Остальные из Ноги/ягодицы (где есть слово «Плие» и словосочетание «Ноги, как у балерины» )
		'leg-mat','leg-chair','leg-stretching','leg-middle',
	*/
	else if($arParams['TRAINIG_FILTER']['AIM']["SELECTED"] == 'BURN')
	{
		//Заполняем середину
		$count_max = 4;
		$count_curr = 0;
		while ($duration_current_ < (float)($arParams['TRAINIG_FILTER']['DURATION']["SELECTED"]))
		{
			$item_middle1 = getTrainigFilterItems(
				$arITEMS, 
				$arCATEGORY, 
				$catsName 			= array('cardio-chair', 'cardio-middle'), //название 'category-type'
				$count 				= 1, //запрашиваем одно видео 
				$duration_current 	= $duration_current_,
				$duration_max 		= (float)($arParams['TRAINIG_FILTER']['DURATION']["SELECTED"]), // в случае запроса по количеству и при этом учитывать время, тут нужно указать максимальное время
				$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
				$params				= array("PROPERTY_DAY"=>$DAY) //доп условия
			);
			if(!empty($item_middle1['items'])){
				$arResult[] = $item_middle1['items'][0];
				$notItemsId[] = $item_middle1['items'][0]['ID'];
				$duration_current_ = $item_middle1['duration'];
				$count_curr++;
			}
			if($count_max <= $count_curr){break;}
			
			$item_middle2 = getTrainigFilterItems(
				$arITEMS, 
				$arCATEGORY, 
				$catsName 			= array('leg-mat','leg-chair','leg-stretching','leg-middle',), //название 'category-type'
				$count 				= 1, //запрашиваем одно видео 
				$duration_current 	= $duration_current_,
				$duration_max 		= (float)($arParams['TRAINIG_FILTER']['DURATION']["SELECTED"]), // в случае запроса по количеству и при этом учитывать время, тут нужно указать максимальное время
				$notItemsId			= $notItemsId, //Перечислены id, которые нужно исключить из выборки
				$params				= array("PREVIEW_TEXT_SEARCH"=>array("Плие", "Ноги, как у балерины"), "PROPERTY_DAY"=>$DAY) //доп условия
			);
			if(!empty($item_middle2['items'])){
				$arResult[] = $item_middle2['items'][0];
				$notItemsId[] = $item_middle2['items'][0]['ID'];
				$duration_current_ = $item_middle2['duration'];
				$count_curr++;
			}
			if($count_max <= $count_curr){break;}
			
			if(empty($item_middle1['items']) && empty($item_middle2['items'])){
				break;
			}
		}
	}
	
	
	
	
	//раскомментировать для проверки
	//return $arResult;
	
	$LOGS["ITEMS"] = array_map(function($item){return array("ID"=>$item["ID"], "CATEGORY"=>$item['PROPS']['CATEGORY']['VALUE_XML_ID'], "TYPE"=>$item['PROPS']['TYPE']['VALUE_XML_ID'], "DURATION"=>$item['PROPS']['DURATION']['VALUE']);}, $arResult);
	$LOGS["DURATION"] = $duration_current_;
	
	$ITEMS = array_map(function($item){return $item["ID"];}, $arResult);
	
	return array("ITEMS"=>$ITEMS, "LOGS"=>$LOGS);
}
