<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if(!CModule::IncludeModule('iblock')) {
	return;
}
global $USER;

//Если пользователь не член тестовой группы, или подписки нужно слать его на страницу subscription
$arGroups = array();
$rows = CUser::GetUserGroupList( $USER->GetID() );
while ($row = $rows->Fetch()){
	$arGroups[$row['GROUP_ID']] = $row;
}

$arResult['USER_GROUPS'] = $arGroups;

if( ! $arParams["IS_PAGE_ACCESS"] && ! array_key_exists(KRUser::$GROUP_FREE_ACCESS, $arGroups) && ! array_key_exists(KRUser::$GROUP_SUBSCRIBE_USERS, $arGroups)){
	LocalRedirect("/subscription/");
}


$allowed_days = 5;
if(array_key_exists(KRUser::$GROUP_SUBSCRIBE_USERS, $arGroups) && $arGroups[KRUser::$GROUP_SUBSCRIBE_USERS]['DATE_ACTIVE_FROM'] !== null) {
	$dStart = new DateTime($arGroups[KRUser::$GROUP_SUBSCRIBE_USERS]['DATE_ACTIVE_FROM']);
	$dEnd  = new DateTime($arGroups[KRUser::$GROUP_SUBSCRIBE_USERS]['DATE_ACTIVE_TO']);
	$dDiff = $dStart->diff($dEnd);
	if($dDiff->format('%r%a') < 15) {
	  $allowed_days = 7;
	}
}


$arParams["SORT_NAME"] 		= $_REQUEST['SORT_NAME'];
$arParams["CATEGORY"] 		= $_REQUEST['CATEGORY'];
$arParams["TYPE"] 			= $_REQUEST['TYPE'];

if(!empty($arParams['SORT_BY'])){
	$arParams["SORT_BY"] = $arParams['SORT_BY'];
}elseif(!empty($_REQUEST['SORT_BY'])){
	$arParams["SORT_BY"] = $_REQUEST['SORT_BY'];
}else{
	$arParams["SORT_BY"] = 'DATE';
}

if(!empty($arParams['SORT_ORDER'])){
	$arParams["SORT_ORDER"] = $arParams['SORT_ORDER'];
}elseif(!empty($_REQUEST['SORT_ORDER'])){
	$arParams["SORT_ORDER"] = $_REQUEST['SORT_ORDER'];
}else{
	$arParams["SORT_ORDER"] = 'DESC';
}

$arParams["PAGINATION"] 	= ((!empty($_REQUEST['PAGINATION']))?$_REQUEST['PAGINATION']:12);
$arParams["DAY"] 			= ((!empty($_REQUEST['DAY']))? (int)($_REQUEST['DAY']):1);
$arParams["FAVORITES"] 		= ((!empty($_REQUEST['FAVORITES']))?$_REQUEST['FAVORITES']:0);
$arParams["PAGEN_1"] 		= ((!empty($_REQUEST['PAGEN_1']))?$_REQUEST['PAGEN_1']:1);
$arParams["IS_AJAX"] 		= false;

//Подписан ли пользователь, если залогинен
$arParams["IS_SUBSCRIBE"] 	= (array_key_exists(KRUser::$GROUP_SUBSCRIBE_USERS, $arGroups)) ? true : false;

//пароль пользователя для boomstream
$rsUser = CUser::GetByID($USER->GetID());
$arUser = $rsUser->Fetch();
$arParams['BOOMSTREAM_HASH'] = (!empty($arUser["UF_BOOMSTREAM_HASH"])) ? $arUser["UF_BOOMSTREAM_HASH"] : false ;
$arParams['BOOMSTREAM_HASH_D'] = (!empty($arUser["UF_BOOMSTREAM_HASH_D"])) ? $arUser["UF_BOOMSTREAM_HASH_D"] : false ;


//для страницы Мои треноровки, значения фильтра: цель тренировки, продолжительность
$arParams['TRAINIG_FILTER'] = array(
	"AIM" => array(
		"ITEMS" => array(
			"ALL" => "Всё тело",
			"LEGS" => "Ноги, как у балерины",
			"POSE" => "Красивая осанка и мышцы кора",
			"BURN" => "Жиросжигающая тренировка",
		),
		"SELECTED" => ($allowed_days == 5 ? ((isset($_GET['filter_aim'])) ? $_GET['filter_aim'] : "ALL") : "ALL"),
	),
	"DURATION" => array(
		"ITEMS" => array(
			"30" => "До 30 мин",
			"60" => "До 60 мин",
		),
		"SELECTED" => ($allowed_days == 5 ? ((isset($_GET['filter_duration'])) ? $_GET['filter_duration'] : "30") : "60"),
	),
);


//var_dump($arParams);
$arResult['ITEMS'] = array();
//особый вид фильтрации видео, по схемам
// debug($arParams);
if(
	$arParams["FILTER_TYPE"]=='DAYS_FILTER' &&
	(isset($_GET['filter_aim']) && isset($_GET['filter_duration'])) &&
	$USER->GetID()
)
{
	require (dirname(__FILE__)."/funcs.php");
	
	$arITEMS = array();
	$arCATEGORY = array();
	
	$arFilter = Array();
	$arFilter["IBLOCK_ID"] = KRIBlock::$ID_WORKOUTS;
	$arFilter["ACTIVE"] = 'Y';
	//$arFilter["PROPERTY_DAY"] = $arParams["DAY"];
	$res = CIBlockElement::GetList(Array("RAND" => "ASC"), $arFilter);
	while($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$item['PROPS'] = $ob->GetProperties();
		$arITEMS[$item['ID']] = $item;
		
		//Распределение по категориям и типам
		$cat_id = $item['PROPS']['CATEGORY']['VALUE_XML_ID'];
		$type_id = $item['PROPS']['TYPE']['VALUE_XML_ID'];
		if(!empty($cat_id) && !empty($type_id)){
			$c_t_name = "{$cat_id}-{$type_id}";
			if(!isset($arCATEGORY[$c_t_name])){
				$arCATEGORY[$c_t_name] = array();
			}
			$arCATEGORY[$c_t_name][] = $item['ID'];
		}
	}
	
	
	//раскомментировать для проверки
	//$arResult['ITEMS'] = getTrainigFilterItemsbySchema($DAY=1, $arParams, $arITEMS, $arCATEGORY);
	
	$arResultFilter = array();
	$arResultLogs = array();
	$arResultFilter["FILTER"] = $arResultLogs["FILTER"] = array(
		"AIM" => $arParams['TRAINIG_FILTER']['AIM']["SELECTED"],
		"DURATION" => $arParams['TRAINIG_FILTER']['DURATION']["SELECTED"],
	);
	for($DAY=1; $DAY <= 7; $DAY++){
		$TRAINING = getTrainigFilterItemsbySchema($DAY, $arParams, $arITEMS, $arCATEGORY);
		$arResultFilter["ITEMS"][$DAY] = $TRAINING['ITEMS'];
		$arResultLogs[$DAY] = $TRAINING['LOGS'];
	}
	
	//логи
	\KrasivoeReshenie\Logger::add(\KrasivoeReshenie\Logger::LEVEL_NOTE, 'MyTrainingFilterNote', 'Результат: '.json_encode($arResultLogs));
	\KrasivoeReshenie\Logger::add(\KrasivoeReshenie\Logger::LEVEL_DEBUG, 'MyTrainingFilterDebug', 'Результат: '.json_encode($arResultLogs));
	
	//Сохраняем в базу
	$user = new CUser;
	$fields = Array( 
		"UF_MY_TRAINING" => json_encode($arResultFilter, JSON_HEX_APOS | JSON_HEX_QUOT ), 
	); 
	$user->Update($USER->GetID(), $fields);
	
	LocalRedirect($APPLICATION->GetCurPageParam("", array("filter_aim", "filter_duration")));
	//var_dump($APPLICATION->GetCurPageParam("", array("filter_aim", "filter_duration")));
	//var_dump($arResultFilter);
	//die;
}
//фильтры
else
{
	
	$arResult['ITEMS'] = Array();
	$arSort = Array();
	$arFilter = Array();
	$arFilter["IBLOCK_ID"] = KRIBlock::$ID_WORKOUTS;
	$arFilter["ACTIVE"] = 'Y';

	if($allowed_days == 7 && $arParams["FILTER_TYPE"]=='DAYS_FILTER') {
		// debug(isset($_REQUEST['DAY']) ? $_REQUEST['DAY'] : 1);
		$arFilter["PROPERTY_DAY_SUBSCRIBE_WEEK"] = isset($_REQUEST['DAY']) ? $_REQUEST['DAY'] : 1;

	} else {
		//Смотрим есть ли у пользователя сохраненные упражнения, елси есть добавляем в фильтры
		$arResultFilter = Array();
		if($arParams["FILTER_TYPE"]=='DAYS_FILTER'){
			if($user_id = $USER->GetID())
			{
				$arRes = CUser::GetList(($by = "NAME"), ($order = "desc"), array("ID" => $user_id), array("SELECT" => array("UF_MY_TRAINING")));
				if ($arUser = $arRes->Fetch()) {
					// die();
					$arResultFilter = json_decode($arUser['UF_MY_TRAINING'], true);
					if(!empty($arResultFilter['ITEMS'][(int)($arParams["DAY"])])){
						$arFilter["ID"] = $arResultFilter['ITEMS'][(int)($arParams["DAY"])];
						$arResult['ITEMS'] = array_flip($arResultFilter['ITEMS'][(int)($arParams["DAY"])]);
					}else{
						$arFilter["ID"] = false; //не выводим ничего 
					}
					if(!empty($arResultFilter['FILTER'])){
						$arParams['TRAINIG_FILTER']['AIM']["SELECTED"] = $arResultFilter['FILTER']['AIM'];
						$arParams['TRAINIG_FILTER']['DURATION']["SELECTED"] = $arResultFilter['FILTER']['DURATION'];
					}
				}
			}
		}
	}
	

	
	//обычные фильтры и сортировка
	if( ! $arResultFilter)
	{
		unset($arFilter["ID"]); //Удаляем ID из тренировок
		if(!empty($arParams["CATEGORY"])){
			$db_enum_list = CIBlockProperty::GetPropertyEnum("CATEGORY", Array(), Array("IBLOCK_ID"=>KRIBlock::$ID_WORKOUTS, "XML_ID"=>$arParams["CATEGORY"]));
			$ar_enum_list = $db_enum_list->GetNext();
			$arFilter["PROPERTY_CATEGORY"] = $ar_enum_list["ID"];
		}
		if(!empty($arParams["TYPE"])){
			$db_enum_list = CIBlockProperty::GetPropertyEnum("TYPE", Array(), Array("IBLOCK_ID"=>KRIBlock::$ID_WORKOUTS, "XML_ID"=>$arParams["TYPE"]));
			$ar_enum_list = $db_enum_list->GetNext();
			$arFilter["PROPERTY_TYPE"] = $ar_enum_list["ID"];
		}
		if(!empty($arParams["FAVORITES"])){
			$arFilter["ID"] = KRFavorites::get();
		}
		if($arParams["FILTER_TYPE"]=='DAYS_FILTER' && $allowed_days == 5){
			$arFilter["PROPERTY_DAY"] = (int)($arParams["DAY"]);
		}

		//сортировка
		//для страницы моя тренировка
		if($arParams["FILTER_TYPE"]=='DAYS_FILTER'){
			$arSort['SORT'] = 'ASC';
		//для страницы все тренировки
		}else{
			//Если демо доступ, то сначала все доступные видео
			if( ! $arParams["IS_SUBSCRIBE"]){
				$arSort = Array('propertysort_SUBSCRIBE_ONLY' => 'ASC');
			}
			$SORT_ORDER = $arParams["SORT_ORDER"];
			switch($arParams["SORT_BY"]){
				case "SORT" : $SORT_BY = "SORT"; break;
				case "NAME" : $SORT_BY = "NAME"; break;
				case "DATE" : $SORT_BY = "PROPERTY_DATE"; break;
				case "DURATION" : $SORT_BY = "PROPERTY_DURATION"; break;
				default : $SORT_BY = "PROPERTY_DATE"; break;
			}
			$arSort[$SORT_BY] = $SORT_ORDER;
		}
	}
	
	//var_dump($arResult['ITEMS']);
	// debug($arSort);
	//var_dump($arParams);
	// debug($arFilter);
	//die;
	
	$res = CIBlockElement::GetList($arSort, $arFilter, false, array(
		'checkOutOfRange' => true,
		'nTopCount' => false,
		'nPageSize' => $arParams["PAGINATION"],
		'iNumPage' => $arParams['PAGEN_1']
	)/*, $arSelect*/);

	$navComponentObject = null;
	$arResult["NAV_STRING"] = $res->GetPageNavStringEx($navComponentObject, 'Страница', 'training', 'Y');
	while($ob = $res->GetNextElement())
	{
		$item = $ob->GetFields();
		$item['PROPS'] = $ob->GetProperties();
		
		$arResult['ITEMS'][$item['ID']] = $item;
	}
}


//AJAX запросы
if ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') {
    $APPLICATION->RestartBuffer();
	
	
	//избранное
	if($_REQUEST['METHOD'] == 'favorites'){
		
		$result = array(
			"status" => "error"
		);
		
		$id = (int)($_POST["id"]);
		if($id)
		{
			$type = KRFavorites::set($id);
			
			$result = array(
				"status" => "ok",
				"type" => $type,
				"count" => KRFavorites::getCount()
			);
		}
		
		print json_encode($result);
		exit();
	}
	
	
	
	//фильры
	if($_REQUEST['METHOD'] == 'filter'){
		
		$arParams["IS_AJAX"] = true;
		
		ob_start();
		$this->IncludeComponentTemplate();
		$html = ob_get_contents(); 
		ob_end_clean();
		
		print $html;
		exit();	
	}
}



//var_dump($arResult['ITEMS']);

$this->IncludeComponentTemplate();


