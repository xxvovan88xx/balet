<?php
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
	die();
}

if(!CModule::IncludeModule('iblock')) {
	return;
}

global $USER;
use Bitrix\Main\Config\Option;


//ошибки валидации
$arParams["validate_error"] = array();


//Добавление комментария
if(
	$_SERVER["REQUEST_METHOD"]=='POST' && 
	!empty($_POST["method"]) && 
	$_POST["method"]=='comment_add' && 
	!empty($arParams["BLOG_ID"]) &&
	$USER->IsAuthorized()
){
	$arParams['form']["name"] = $name = trim($_POST["name"]);
	$arParams['form']["comment"] = $comment = trim($_POST["comment"]);
	$arParams['form']["parent_id"] = $parent_id = trim($_POST["parent_id"]);
	
	//валидация имени
	if(validateCommentName($name))
	{	
		$PROP = array();
		$PROP['USER'] = $USER->GetID();
		$PROP['BLOG'] = $arParams['BLOG_ID'];
		$PROP['AUTHOR'] = '';
		$PROP['PARENT_ID'] = $parent_id;

		$arLoadProductArray = Array(
			"MODIFIED_BY"    => $USER->GetID(), // элемент изменен текущим пользователем
			"IBLOCK_SECTION_ID" => false,		// элемент лежит в корне раздела
			"IBLOCK_ID"      => KRIBlock::$ID_COMMENTS,
			"PROPERTY_VALUES"=> $PROP,
			"NAME"           => $name,
			"ACTIVE"         => "Y",			// активен
			"PREVIEW_TEXT"   => $comment,
			"DETAIL_TEXT"    => "",
			//"DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/image.gif")
		);
		//var_dump($arLoadProductArray);die;

		$el = new CIBlockElement;
		if($PRODUCT_ID = $el->Add($arLoadProductArray)){
			
			//шлем пиcьмо
			$arEventFields = array();
			$arEventFields['USER_ID'] 	= $USER_ID;
			$arEventFields['EMAIL'] 	= $arFields['EMAIL'];
			$arEventFields['LOGIN'] 	= $arFields['LOGIN'];
			$arEventFields['PASSWORD'] 	= $arFields['PASSWORD'];
			$arEventFields['NAME'] 		= $arFields['NAME'];
			$arEventFields['USER_IP'] 	= $arFields['USER_IP'];
			$arEventFields['USER_HOST'] = $arFields['USER_HOST'];
			//if (!CEvent::Send('REG_NEW_USER', SITE_ID, $arEventFields)){
			//	throw new Exception('Произошла ошибка во время отправки заказа. Повторите попытку позже.');
			//}
		}
		
		LocalRedirect($_SERVER["REDIRECT_URL"]."#comment_block");
		exit();
	}else{
		$arParams["validate_error"][] = "name";
	}
}


$arResult['ITEMS'] = array();
$arSelect = Array("ID", "NAME", "PREVIEW_TEXT", "DATE_CREATE", "PROPERTY_AUTHOR", "PROPERTY_PARENT_ID");
$arFilter = Array("IBLOCK_ID"=>KRIBlock::$ID_COMMENTS, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "PROPERTY_BLOG"=>$arParams["BLOG_ID"]);
$res = CIBlockElement::GetList(Array("TIMESTAMP_X" => "ASC"), $arFilter, false, Array(), $arSelect);
while($ob = $res->GetNextElement())
{
	$arFields = $ob->GetFields();
	$arResult['ITEMS'][] = $arFields;
	
}

//сортировка комментариев
$arResult['ITEMS'] = sortComments($arResult['ITEMS'], array(), $parent_id = 0);


$this->IncludeComponentTemplate();


