<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "О проекте Баббалет");
$APPLICATION->SetPageProperty("description", "Сочетание интенсивных силовых тренировок Barre с элементами балетных па, кардио нагрузки и растяжки. Все вместе это даёт поразительные результаты уже после нескольких занятий.");
$APPLICATION->SetTitle("О проекте Баббалет");
?><main class="main" role="main">
  <div class="wrap">
    <div class="wrap__block">
      <div class="wrap__heading wrap__heading--left">
        <h1 class="wrap__title">О проекте</h1>
      </div>
      <div class="wrap__item container">
        <div class="project">
          <div class="project__content">
            <div class="project__item">
              <div class="content">
                <div class="content__block">
                  <p><b>Я вывела идеальную формулу здорового и подтянутого тела. Мои тренировки нельзя отнести ни к одному виду фитнеса: ни к боди-балету, ни конкретно к Barre. Это новый уровень!</b></p>
                  <p>Мои тренировки — это сочетание интенсивных силовых тренировок Barre с элементами балетных па, кардио нагрузки и растяжки. Все вместе это даёт поразительные результаты уже после нескольких занятий.</p>
                </div>
              </div>
            </div>
            <div class="project__background">
              <div class="project__image"
                   style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/assets/images/about_01.jpg);"></div>
            </div>
            <div class="project__item">
              <div class="content">
                <div class="content__block">
                  <h4>Как это работает?</h4>
                  <ul>
                    <li>Смесь танцевальных элементов с кардио нагрузкой направлены на сжигание жира. Благодаря им вы
                      похудеете, увеличите выносливость и улучшите работу сердечно-сосудистой системы;
                    </li>
                    <li>Силовые упражнения из направления Barre укрепят ваши мышцы, сделают тело рельефным и подтянутым,
                      но не перекаченным;
                    </li>
                    <li>За счет растяжки вы приобретете гармоничное телосложение, гибкость и плавность движений. Пройдут
                      боли в спине и ногах, осанка станет ровной, а походка — легкой и изящной.
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="project__foreground">
              <div class="project__image">
                <div class="project__image"
                     style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/assets/images/about_02.jpg);"></div>
              </div>
            </div>
            <div class="project__info">
              <p>Уже через пару недель вы будете чувствовать себя собранной и подтянутой, ваше
                тело станет более изящным, стройным и упругим.</p>
            </div>
            <div class="project__item">
              <div class="content">
                <div class="content__block">
                  <div type="button" class="watch-video" data-modal="#Instruction" data-new-modal="#Instruction" data-url="https://www.youtube.com/embed/Gxk-ba3Lxjw">Смотреть видео о проекте</div>
                </div>
              </div>
            </div>
          </div>
          <div class="project__bottom">
            <div class="content ">
              <p><b>Такой комплексный подход позволяет получить первые результаты уже после третьей тренировки. Через месяц вы&nbsp;заметите, как укрепится ваш корпус, талия, бёдра и&nbsp;ягодицы, т.&nbsp;е. все зоны, которые не&nbsp;удавалось проработать с&nbsp;помощью традиционных программ тренировок.</b></p>
            </div>
          </div>
        </div>
      </div>
      <div class="wrap__all">
        <a href="/subscription/" class="button">
          <span class="button__text">Начать заниматься</span>
        </a>
      </div>
    </div>
  </div>
</main><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>