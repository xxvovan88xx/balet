<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Авторская программа по созданию красивого тела с помощью балетных элементов, силовых упражнений, кардионагрузки и растяжки");
$APPLICATION->SetPageProperty("keywords", "Баббалет");
$APPLICATION->SetPageProperty("title", "Онлайн курсы по улучшению фигуры - Баббалет");
//$APPLICATION->SetPageProperty("NOT_SHOW_NAV_CHAIN", "Y");
$APPLICATION->SetTitle("Онлайн курсы по улучшению фигуры - Баббалет");
?><main class="main" role="main">
<div class="intro__buttons mobile_fixed">
      <a href="#trial_subscription" class="button intro__button" id="">
        <span class="button__text">Попробовать бесплатно</span>
      </a>
      <a  href="/subscription/" class="button intro__button button--stroke button--white-stroke" id="" data-tab="aasd">
        <span class="button__text">Начать заниматься</span>
      </a>
    </div>
  <div class="intro">
    <video class="intro__video" loop="loop" preload="auto" muted="muted" autoplay="autoplay" playsinline>
      <source src="<?= SITE_TEMPLATE_PATH ?>/assets/media/intro-video.mp4" type="video/mp4">
      Your browser does not support the video tag.
    </video>
    <div class="intro__inner">
      <div class="intro__info">
        <h2 class="intro__title">Откройте в себе новые возможности</h2>
        <p class="intro__text">Авторская программа по&nbsp;созданию красивого тела с&nbsp;помощью балетных элементов, силовых
          упражнений, кардионагрузки и&nbsp;растяжки</p>
      </div>
      <div class="intro__buttons desktop">
        <a href="#trial_subscription" class="button intro__button" id="try_free_btn">
          <span class="button__text">Попробовать бесплатно</span>
        </a>
        <a href="/subscription/" class="button intro__button button--stroke button--white-stroke" id="start__btn">
          <span class="button__text">Начать заниматься</span>
        </a>
      </div>
    </div>
    <a href="#wrap" class="intro__down" data-smooth>
      <svg xmlns="http://www.w3.org/2000/svg">
        <use xlink:href="#arrow"></use>
      </svg>
      <svg xmlns="http://www.w3.org/2000/svg">
        <use xlink:href="#arrow"></use>
      </svg>
    </a>
  </div>
  <div class="wrap" id="wrap">
    <div class="wrap__block">
      <div class="wrap__heading">
        <h2 class="wrap__title">Что вы получите от&nbsp;курса?</h2>
      </div>
      <div class="wrap__item container">
        <div class="course">
          <div class="course__row">
            <ul class="course__list">
              <li class="course__item">Элегантную осанку балерины и&nbsp;здоровую спину</li>
              <li class="course__item">Сильное и&nbsp;подтянутое тело</li>
              <li class="course__item">Гибкие мышцы и&nbsp;связки</li>
              <li class="course__item">Легкую и&nbsp;красивую походку</li>
              <li class="course__item">Уверенность в&nbsp;себе</li>
            </ul>
            <div class="course__background">
              <div class="course__image"
                   style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/assets/images/course__image_01.jpg);"></div>
            </div>
          </div>
          <div class="course__row">
            <div class="course__desc">
              <p class="course__text">Неважно, тянули вы когда-нибудь носочки, занимались балетом или нет. Здесь мы не&nbsp;танцуем! Мои тренировки подойдут всем женщинам с&nbsp;любой фигурой и&nbsp;уровнем подготовки. Вас ждут упражнения без тяжелых нагрузок, основанные на&nbsp;элементах балета и&nbsp;растяжки. Они сделают ваше тело изящным и&nbsp;сильным.</p>
            </div>
            <div class="course__background">
              <div class="course__image"
                   style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/assets/images/course__image_02.jpg);"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrap__block wrap__block--gray">
      <div class="wrap__heading">
        <h2 class="wrap__title">Что из&nbsp;себя представляет курс?</h2>
        <p class="wrap__text">Это авторская программа тренировок, в&nbsp;основу которых входят интенсивные силовые упражнения с&nbsp;элементами балета, кардионагрузка и&nbsp;растяжка. Все упражнения отличаются эффективностью и&nbsp;рассчитаны на&nbsp;выносливость, силу, гибкость и&nbsp;сжигание калорий.</p>
      </div>
      <div class="wrap__item container">
        <div class="features">
          <div class="features__item">
            <div class="features__content">
              <div class="features__icon">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xlink:href="#ballet"></use>
                </svg>
              </div>
              <div class="features__info">
                <p class="features__title">Полноценные часовые тренировки</p>
                <p class="features__text">Заниматься можете в&nbsp;любое удобное для вас время и&nbsp;любом месте, где есть доступ к&nbsp;wi-fi</p>
              </div>
            </div>
          </div>
          <div class="features__item">
            <div class="features__content">
              <div class="features__icon">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xlink:href="#album"></use>
                </svg>
              </div>
              <div class="features__info">
                <p class="features__title">Богатая база</p>
                <p class="features__text">Вы получите более 100&nbsp;видео с&nbsp;тренировками на&nbsp;все группы мышц</p>
              </div>
            </div>
          </div>
          <div class="features__item">
            <div class="features__content">
              <div class="features__icon">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xlink:href="#review"></use>
                </svg>
              </div>
              <div class="features__info">
                <p class="features__title">Собственный плейлист</p>
                <p class="features__text">Сможете сконструировать тренировку по&nbsp;интенсивности, времени и&nbsp;типу
                  занятия</p>
              </div>
            </div>
          </div>
          <div class="features__item">
            <div class="features__content">
              <div class="features__icon">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xlink:href="#computer"></use>
                </svg>
              </div>
              <div class="features__info">
                <p class="features__title">Онлайн-тренировки</p>
                <p class="features__text">Занятия можно смотреть с&nbsp;компьютера или телефона</p>
              </div>
            </div>
          </div>
          <div class="features__item">
            <div class="features__content">
              <div class="features__icon">
                <svg xmlns="http://www.w3.org/2000/svg">
                  <use xlink:href="#play"></use>
                </svg>
              </div>
              <div class="features__info">
                <p class="features__title">Новые видео каждый месяц</p>
                <p class="features__text">Вы будете получать по&nbsp;3–4 новых видео в&nbsp;месяц</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrap__block" data-stick="all">
      <div class="wrap__item">
        <div class="promo" data-new-modal="#modalPreview" data-url="https://www.youtube.com/embed/c2mV-q0vr4A?enablejsapi=1">
          <div class="promo__info">
            <h2 class="promo__title">Пример тренировки</h2>
            <p class="promo__text">Комплекс упражнений на&nbsp;ягодичные мышцы и&nbsp;внешнюю поверхность бедра</p>
          </div>
          <div class="promo__player">
             <div class="promo__frame" id="promo__frame">
              
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="wrap__block wrap__block--overflow">
      <div class="wrap__heading wrap__heading--left">
        <h2 class="wrap__title">От автора</h2>
      </div>
      <div class="wrap__item container">
        <div class="author">
          <div class="author__content">
            <div class="content">
              <p><em>Я</em> искренне желаю вам изменений и&nbsp;развития! Я&nbsp;не&nbsp;устану повторять, что каждый человек, независимо от&nbsp;возраста и&nbsp;природных данных, может иметь здоровую спину, правильную осанку и&nbsp;подтянутую фигуру. А&nbsp;через развитие гибкости может поднять самооценку, убрать скованность и&nbsp;зажатость.</p>
              <blockquote>
                <p>Я&nbsp;точно знаю, что многие проблемы можно решить через работу с&nbsp;телом</p>
              </blockquote>
              <p>Я&nbsp;желаю вам перемен, перемен к&nbsp;лучшей версии себя! Уверена, что это получится сделать с&nbsp;моими тренировками.</p>
            </div>
          </div>
          <div class="author__character">
            <div class="character">
              <div class="character__background"
                   style="background-image: url(<?= SITE_TEMPLATE_PATH ?>/assets/images/character-31.png);">
                <div class="character__info">
                  <p class="character__name">Анастасия Катаева</p>
                  <p class="character__state">автор курса и&nbsp;основатель проекта «Баббалет»</p>
                </div>
              </div>
            </div>
          </div>
          <div class="author__bottom">
            <a href="/author/" class="button button--stroke button--black-stroke">
              <span class="button__text">Об авторе</span>
            </a>
            <a href="/about/" class="button">
              <span class="button__text">О проекте</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  
  <div class="wrap__block wrap__block--overflow wrap__block--gray">
  
<?$APPLICATION->IncludeComponent(
  "bitrix:news.list", 
  "main_reviews", 
  array(
    "IBLOCK_TYPE" => "-",
    "IBLOCK_ID" => KRIBlock::$ID_REVIEWS,
    "NEWS_COUNT" => "20",
    "SORT_BY1" => "SORT",
    "SORT_ORDER1" => "ASC",
    "SORT_BY2" => "ID",
    "SORT_ORDER2" => "ASC",
    "FILTER_NAME" => "",
    "PROPERTY_CODE" => array(
      0 => "",
      1 => "CITY",
      2 => "VIDEO",
      3 => "",
    ),
    "CHECK_DATES" => "Y",
    "DETAIL_URL" => "",
    "AJAX_MODE" => "N",
    "AJAX_OPTION_JUMP" => "N",
    "AJAX_OPTION_STYLE" => "N",
    "AJAX_OPTION_HISTORY" => "N",
    "AJAX_OPTION_ADDITIONAL" => "",
    "CACHE_TYPE" => "A",
    "CACHE_TIME" => "3600",
    "CACHE_FILTER" => "N",
    "CACHE_GROUPS" => "N",
    "PREVIEW_TRUNCATE_LEN" => "200",
    "ACTIVE_DATE_FORMAT" => "d.m.Y",
    "SET_TITLE" => "N",
    "SET_BROWSER_TITLE" => "N",
    "SET_META_KEYWORDS" => "N",
    "SET_META_DESCRIPTION" => "N",
    "SET_STATUS_404" => "N",
    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
    "ADD_SECTIONS_CHAIN" => "N",
    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
    "PARENT_SECTION" => "",
    "PARENT_SECTION_CODE" => "",
    "INCLUDE_SUBSECTIONS" => "Y",
    "DISPLAY_DATE" => "N",
    "DISPLAY_NAME" => "N",
    "DISPLAY_PICTURE" => "N",
    "DISPLAY_PREVIEW_TEXT" => "Y",
    "PAGER_TEMPLATE" => ".default",
    "DISPLAY_TOP_PAGER" => "N",
    "DISPLAY_BOTTOM_PAGER" => "N",
    "PAGER_TITLE" => "Новости",
    "PAGER_SHOW_ALWAYS" => "N",
    "PAGER_DESC_NUMBERING" => "N",
    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
    "PAGER_SHOW_ALL" => "N",
    "COMPONENT_TEMPLATE" => "main_reviews",
    "FIELD_CODE" => array(
      0 => "",
      1 => "",
    ),
    "SET_LAST_MODIFIED" => "N",
    "STRICT_SECTION_CHECK" => "N",
    "PAGER_BASE_LINK_ENABLE" => "N",
    "SHOW_404" => "N",
    "MESSAGE_404" => ""
  ),
  false
);
?>

    <?php if(!$USER->IsAuthorized()): ?>
        <?php
        $APPLICATION->IncludeComponent(
          'kr:subscribe',
          '.default',
          array(
          ),
          false
        );
        ?>
    <?php endif; ?>
    
  </div>

  
  
    
</main>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>