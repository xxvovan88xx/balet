<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");

if (is_string($_REQUEST["backurl"]) && strpos($_REQUEST["backurl"], "/") === 0)
{
	LocalRedirect($_REQUEST["backurl"]);
}

$APPLICATION->SetTitle("Авторизация");
?>

<main class="main" role="main">
	<div class="wrap">
		<div class="wrap__block wrap__block--gray">
			<div class="wrap__heading">
				<h1 class="wrap__title">Вы зарегистрированы и успешно авторизовались.</h1>
			</div>
			<div class="wrap__item container">
 
<p class="wrap__text">Используйте административную панель в верхней части экрана для быстрого доступа к функциям управления структурой и информационным наполнением сайта. Набор кнопок верхней панели отличается для различных разделов сайта. Так отдельные наборы действий предусмотрены для управления статическим содержимым страниц, динамическими публикациями (новостями, каталогом, фотогалереей) и т.п.</p>
 <br>
<p class="wrap__text"><a class="header__link " href="<?=SITE_DIR?>">Вернуться на главную страницу</a></p>

			</div>
		</div>
	</div>
</main>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>